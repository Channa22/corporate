<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package StartBiz
 */

get_header(); ?>
<div class="container">

<header class="page-header not-found-title">
	<div class="inner">
		<h1 class="page-title"><?php esc_html_e( '404 Error! That page can&rsquo;t be found', 'startbiz' ); ?></h1>
	</div>
</header><!-- page-header -->

<div class="inner">

	<div id="none-sidebar" class="none-sidebar-area">
		<main id="main" class="site-main" role="main">

			<section id="tc-not-found" class="error-404 not-found">

				<div class="page-content">

					<h2><?php _e('404 Error', 'startbiz'); ?></h2>
					<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'startbiz' ); ?></p>

				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->
</div>

</div>
<?php get_footer(); ?>
