<?php
/**
 * @package StartBiz
 */

$position = ot_get_option('social-sharing-pos');
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php if ( tc_get_option( 'breadcrumb' ) != 'off') : ?>
			<div class="breadcrumb">
				 <?php tcc_breadcrumb(); ?>
			</div>
		<?php endif; ?>
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		

		<?php if (tc_get_option('blog_enable_post_meta_info') != 'off' ) : ?>
			<div class="entry-meta">
				<?php startbiz_posted_on(); ?>
			</div><!-- .entry-meta -->
		<?php endif; ?>
		

		<?php if ( isset($position[0]) ) { tc_social_icons('social-sharing-top'); } ?>

		<?php if ( isset($position[2]) ) : ?>
			<div class="floating-to-right sharing-top-float">
			<?php tc_social_icons('social-sharing-left');?>
			</div>
	<?php  endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'startbiz' ),
				'after'  => '</div>',
			) );
		?>
		<?php if ( isset($position[1]) ) { tc_social_icons('social-sharing-bottom'); } ?>
	</div><!-- .entry-content -->

</article><!-- #post-## -->
<?php if (tc_get_option('post-related') != 'off'): ?>
	<div class="related-posts clear">
		<?php tcc_related_posts() ?>
	</div>
<?php endif; ?>
