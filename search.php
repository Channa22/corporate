<?php
/**
 * The template for displaying search results pages.
 *
 * @package StartBiz
 */

get_header(); ?>
<div class="container">
	<header class="page-header">
			<div class="inner">
				<?php if ( have_posts() ) :  ?>	
					<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'startbiz' ), '"' . get_search_query() . '"' ); ?></h1>
				<?php else : ?>
					<h1 class="page-title">Nothing Found</h1>
				<?php endif; ?>				
			</div>
		</header><!-- .page-header -->
	<div class="row">

		<div class="col-lg-8 col-md-8 col-sm-12 content-left">
			<section id="primary" class="content-area">
				<main id="main" class="site-main" role="main">

					<?php if ( have_posts() ) : ?>

						<!-- <header class="page-header">
							<h1 class="page-title"><?php //printf( __( 'Search Results for: %s', 'startbiz' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
						</header> --><!-- .page-header -->

					<?php /* Start the Loop */ ?>
					<?php while ( have_posts() ) : the_post(); ?>

					<?php
					/**
				 	* Run the loop for the search to output the results.
				 	* If you want to overload this in a child theme then include a file
				 	* called content-search.php and that will be used instead.
				 	*/
					get_template_part( 'content', 'search' );
					?>

					<?php endwhile; ?>

					<?php the_posts_navigation(); ?>

					<?php else : ?>

					<?php  get_template_part( 'content', 'none' ); ?>

					<?php endif; ?>

				</main><!-- #main -->
			</section><!-- #primary -->
		</div>
		<!-- #col-lg-8 -->

		<?php get_sidebar(); ?>
	<?php get_footer(); ?>
