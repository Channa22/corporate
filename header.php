<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package StartBiz
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>


</head>

<body <?php body_class(); ?>>	
<div id="page" class="hfeed site ">	
	<a class="skip-link screen-reader-text" href="#content" style="display: none;"><?php _e( 'Skip to content', 'startbiz' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<div id="sticky" class="navbar navbar-default">			
				<div class="main-menu-wrapper">				
					<div class="container">
						<div class="row">
							<div class="custom-mobile-menu col-xs-12 col-sm-9 col-md-9">
								<div class="navbar-header">
									<button type="button" class="navbar-toggle collapsed toggle-mobile-menu" data-toggle="collapse" data-target="#bs-example-navbar-collapse-11">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
								</div>

								<nav id="site-navigation" class="main-navigation primary-navigation" role="navigation">
									<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
										<span class="mobile-only toggle-mobile-menu" title="Menu"><i class="fa fa-bars"></i> Main Navigation<i class="fa fa-close"></i></span>
										<?php if ( has_nav_menu( 'primary' ) ) : ?>
											<?php wp_nav_menu( array('theme_location' => 'primary', 'container' => false, 'menu_class' => 'nav navbar-nav navbar-right') ); ?> 
										<?php else : ?>
											<ul id="primary-menu" class="menu nav-menu" aria-expanded="false">
												<?php wp_list_pages('title_li=&sort_column=menu_order'); ?>
											</ul>
										<?php endif; ?>
									</div>
								</nav><!-- #site-navigation -->
							</div>

							<?php if (tc_get_option('btn_sign_up_on_off') != 'off') : ?>
								<div class="custom-subscribe-box">
									<a href="<?php echo tc_get_option('url_link_sign_up'); ?>" class="sign_up_btn">
										<i class="fa fa-envelope"></i>
										<?php echo tc_get_option('btn_sign_up_title'); ?>
									</a>
								</div>
							<?php endif; ?>

						</div>
					</div>
				</div>
				<!-- .main-menu-wrapper -->
		</div>

		<div id="catcher"></div>
		<div class="wrapper-branding">
			<div class="container">
				<div class="row">
					<?php if (tc_get_option('btn_sign_up_on_off') != 'off') : ?>
						<div class="site-branding">
					<?php else : ?>
						<div class="site-branding no-sign">
					<?php endif; ?>
						<?php tcc_header_title() ?>																	
					</div><!-- .site-branding -->

					<div class="pull-right wrap-contact-header">
						<a href="#" class="call-contact">
							<i class="fa fa-mobile"></i>

							<span class="call-us-content">
								<span>Call us today</span>
								<?php echo tc_get_option('phone_number_contact'); ?>
							</span>
						</a>

						<a href="#" class="mail-contact">
							<i class="fa fa-envelope-o"></i>

							<span class="mail-to-content">
								<span>Mail us today</span>
								<?php echo tc_get_option('email_address_contact'); ?>								
							</span>
						</a>
					</div>

				</div>
			</div>
			<!-- .container -->

		</div>	
		
	</header><!-- #masthead -->

	<?php if (is_page_template('page-contact.php')) : ?>
		<div id="wrapper" class="no-space wrapper">
	<?php else : ?>
		<div class="wrapper" style="display: inline;">
	<?php endif; ?>
		<?php
			$sidebar_position = ot_get_option('layout_width_sidebar') ;
			if ( $sidebar_position == 'left' || is_page_template( 'page-sidebar-left.php' )) {
				$class = 'sidebar-left';
			} else if ( $sidebar_position == 'no-sidebar' || is_page_template( 'page-no-sidebar.php' )) {
				$class = 'no-sidebar';
			} else {
				$class = 'sidebar-right';
			}

		?>
		<div id="content" class="site-content clearfix <?php echo $class; ?>">
		
