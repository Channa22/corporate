<?php
/**
 * Template Name: Contact Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package StartBiz
 */
get_header(); ?>

    <?php if ( tc_get_option('show_hide_map') == 'on' && tc_get_option('contact_lat') && tc_get_option('contact_lng') ) : ?>
        <section id="map" class="map-section section-map">
            <div class="tc-grid-1">
                <div class="tc-grid-item">          
                    <div id="contact-map" style="width:100%; height:450px;">&nbsp;</div>
                </div>
            </div>
        </section>
    <?php endif ?>

    <section id="contact-detail" class="contact-detail section-content">    
        <div class="container contact-form-area">       
		<main id="main" class="site-main clearfix" role="main">
            <?php while ( have_posts() ) : the_post(); ?>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="map-title"><?php the_title() ?></h2>
                </div>                
            </div>
		    <div class="row">
                <div class="col-lg-offset-2 col-lg-4 clearfix">
                    <div class="map-data-inner">
                        <form class="form-left" method="post" action="<?php the_permalink() ?>" id="tc_cotact_form">
                            <input type="hidden" name="action" value="tc_contact_send">
                            <div class="form-group">
                                <input type="text" class="form-control" value="" name="name"  required = "required"  id="tctxt_name" placeholder="<?php _e('Name *', 'bizcorp') ?>">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" value="" name="email"  required = "required"  id="tctxt_email" placeholder="<?php _e('E-Mail Address *', 'bizcorp') ?>">
                            </div>
                            <textarea class="form-control" rows="7" id="tctxt_message" name="message"  required = "required"  placeholder="<?php _e('Type your message *', 'bizcorp') ?>"></textarea>
                            <input  class="btn capitalize" type="submit" value="<?php _e('Send Message', 'bizcorp') ?>" id="tctxt_submit">
                        </form>
                    </div>
                </div>
                <div class="col-lg-6 clearfix">
                    <div id="contactDetail" class="tc-contact-fo map-data-inner-right">
                        <?php the_content() ?>
                        <ul class="contact-info">
                            <?php if (tc_get_option('contact_address')) : ?>
                            <li class="address"><i class="fa fa-map-marker"></i><?php echo tc_get_option('contact_address') ?></li>
                            <?php endif; ?>
                            <?php if (tc_get_option('contact_phone')) : ?>
                            <li class="address"><i class="fa fa-phone-square"></i><?php echo tc_get_option('contact_phone') ?></li>
                            <?php endif; ?>
                            <?php if (tc_get_option('contact_fax')) : ?>
                            <li class="address"><i class="fa fa-fax"></i><?php echo tc_get_option('contact_fax') ?></li>
                            <?php endif; ?>
                            <?php if (tc_get_option('contact_email')) : ?>
                            <li class="address"><i class="fa fa-envelope-o"></i><?php echo tc_get_option('contact_email') ?></li>
                            <?php endif; ?>
                            <?php if (tc_get_option('contact_website')) : ?>
                            <li class="address"><i class="fa fa-link"></i><a href="<?php echo tc_get_option('contact_website') ?>"><?php echo tc_get_option('contact_website') ?></a></li>
                            <?php endif; ?>
                        </ul>
                    </div>                        
                </div>                
            </div>

			<?php
				// If comments are open or we have at least one comment, load up the comment template
				if ( (comments_open() || get_comments_number()) && tc_get_option('page_comments') != 'off') :
					comments_template();
				endif;
			?>

		    <?php endwhile; // end of the loop. ?>
		</main><!-- #main -->      
    </section><!-- #section -->

    <?php if (tc_get_option('contact_lat') && tc_get_option('contact_lng')) : ?>
    <script>
    function map_init(){
        
            var latlng = new google.maps.LatLng(<?php echo tc_get_option('contact_lat'); ?>, <?php echo tc_get_option('contact_lng'); ?>);
            var contentString = '<div class="map-infobox tc-contact-fo">' + jQuery('#contactDetail').html() + '</div>';
            var infowindow = new google.maps.InfoWindow({
                  content: contentString
            });

            var opt = {
                zoom: <?php echo tc_get_option('contact_zoom_level'); ?>,
                center: latlng,
                zoomControl: true,
                mapTypeControl: false,
                streetViewControl: false,
                scrollwheel: false,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var map = new google.maps.Map(document.getElementById("contact-map"),opt);
            var marker = new google.maps.Marker({position: latlng,map: map,});

            google.maps.event.addListener(marker, 'click', function() {
                infowindow.open(map,marker);
            });
        }

        jQuery(document).ready(function($){
        	map_init();
        });
        </script>
<?php endif; ?>
<?php get_footer(); ?>
