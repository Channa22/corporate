<?php if ( is_active_sidebar( 'footer-sidebar-1' )
|| is_active_sidebar( 'footer-sidebar-2' )
|| is_active_sidebar( 'footer-sidebar-3' )
|| is_active_sidebar( 'footer-sidebar-4' ) ) : ?>
    <div class="site-footer-widget">
        <div class="container">
            <div class="row">
                <?php if ( is_active_sidebar( 'footer-sidebar-1' )) : ?>
                    <div class="col-md-3 col-sm-6">
                        <?php dynamic_sidebar( 'footer-sidebar-1' ); ?>
                    </div>
                <?php endif; ?>
                
                <?php if ( is_active_sidebar( 'footer-sidebar-2' )) : ?>
                    <div class="col-md-3 col-sm-6">
                    <?php dynamic_sidebar( 'footer-sidebar-2' ); ?>
                    </div>
                <?php endif; ?>

                <?php if ( is_active_sidebar( 'footer-sidebar-3' )) : ?>
                    <div class="col-md-3 col-sm-6">
                        <?php dynamic_sidebar( 'footer-sidebar-3' ); ?>
                    </div>
                <?php endif; ?>

                <?php if ( is_active_sidebar( 'footer-sidebar-4' )) : ?>
                    <div class="col-md-3 col-sm-6">
                    <?php dynamic_sidebar( 'footer-sidebar-4' ); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <!-- end site footer widget -->
<?php endif; ?>