<?php
/**
 * Default post render
 *
 * @package StartBiz
 */
?>
<div class="col-xs-6 col-sm-4 col-md-3 col-portfolio">		
		<div class="overlay-portfo">
		<?php 
			if ( has_post_thumbnail() ) : 
				$img_src = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );
		?>		
			<div class="post-thumb">
	        	<a href="#">	        		
	        		<?php the_post_thumbnail('post-thumbnails-blog'); ?>	        		       		
	        	</a>
	        </div>
			<?php endif; ?> 
			<div class="post-data">
				<div class="post-data-in">
					<h3 class="post-data-title">
			        	<a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
			    	</h3>
				</div>
				    <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post) ); ?>
				<div class="overlay-link image-overlay">
			    		<a class="prettyPhoto" href="<?php echo $url; ?>" rel="prettyPhoto" title="<?php the_title(); ?>"><i class="fa fa-search-plus"></i></a>
			    		<a href="<?php the_permalink(); ?>" title="view this portfolio"><i class="fa fa-link"></i></a>
			    </div>			  
			</div>
			
		</div>
		<!-- .overlay-portfo -->	    
</div>
