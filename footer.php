<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package StartBiz
 */
?>

    	</div><!-- #content -->
    </div><!--wrapper-->
	<footer id="colophon" class="site-footer" role="contentinfo">
         <?php if(tc_get_option('section_contact_on_off') != 'off') : ?>
            <section id="feature-contact" class="feature-contact site-contact">
                <?php tcc_feature_contact(); ?>
            </section>
        <?php endif; ?>
        <?php get_sidebar('footer'); ?>
		<div class="container">
            <nav class="pull-right nav-footer">
                <div class="menu-footer">
                    <?php wp_nav_menu( array('theme_location' => 'secondary', 'container' => false, 'menu_class' => '') ); ?> 
                </div>
            </nav>

            <?php if (tc_get_option('copyright')) : ?>
                <div class="copyright pull-left">
                     <p><?php echo tc_get_option('copyright'); ?></p>
                </div>
            <?php endif; ?>
        </div>
		<!-- .container -->
	</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>

<?php if(ot_get_option('back_to_top') == 'on') { ?><span class="back-to-top" style="display: none;"><i class="fa fa-hand-o-up"></i></span><?php } ?>

</body>
</html>
