<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package StartBiz
 */


if ( ! function_exists('tcc_header_title') ) :
	/**
	|------------------------------------------------------------------------------
	| Header Title
	|------------------------------------------------------------------------------
	|
	*/
	function tcc_header_title() {
		?>
			<?php if (tc_get_option( 'logo' )) : ?>
				<meta itemprop="logo" content="<?php echo tc_get_option( 'logo' ); ?>">
				<?php if( is_front_page() || is_home() || is_404() ) : ?>
				<h1 class="site-title logo" itemprop="headline">
					<a itemprop="url" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="<?php bloginfo( 'description' ); ?>">
						<img src="<?php echo esc_url(tc_get_option( 'logo' )); ?>" alt="<?php bloginfo( 'description' ); ?>" />
					</a>
				</h1>
				<?php else : ?>
					<h2 class="site-title logo" itemprop="headline">
						<a itemprop="url" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="<?php bloginfo( 'description' ); ?>">
							<img src="<?php echo esc_url(tc_get_option( 'logo' )); ?>" alt="<?php bloginfo( 'description' ); ?>" />
						</a>
					</h2>
				<?php endif ?>
			<?php else : ?>

				<?php if( is_front_page() || is_home() || is_404() ) : ?>
					<h1 itemprop="headline" class="site-title">
						<a itemprop="url" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="<?php bloginfo( 'description' ); ?>">
							<?php bloginfo( 'name' ); ?>
						</a>
					</h1>
					<h2 class="site-description hidden-sm"><?php bloginfo( 'description' ); ?></h2>
					<?php else : ?>
						<h2 class="site-title">
						<a itemprop="url" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="<?php bloginfo( 'description' ); ?>">
							<?php bloginfo( 'name' ); ?>
						</a>
						</h2>
						<h3 class="site-description hidden-sm"><?php bloginfo( 'description' ); ?></h3>
					<?php endif ?>
			<?php endif ?>
		<?php
	}
endif;

if ( ! function_exists( 'tcc_posts_navigation' ) ) :
/**
 |------------------------------------------------------------------------------
 | Display navigation to next/previous set of posts when applicable.
 |------------------------------------------------------------------------------
 | @todo Remove this function when WordPress 4.3 is released.
 |
 */
function tcc_posts_navigation() {
	// Don't print empty markup if there's only one page.
	if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
		return;
	}

	$nav_style = tc_get_option ('pagination');


	if ( $nav_style == 'infinite') :
		tc_infinite_loading('infinite');
	
	elseif ( $nav_style == 'numberal') :
		// Previous/next page navigation.
			the_posts_pagination( array(
				'prev_text'          => __( '<i class="fa fa-angle-left"></i> Previous', 'startbiz' ),
				'next_text'          => __( 'Next <i class="fa fa-angle-right"></i>', 'startbiz' ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'startbiz' ) . ' </span>',
			) );
	elseif ($nav_style == 'loading') :
		tc_infinite_loading();
	else :
		
	?>
	<nav class="paging-navigation clearfix" role="navigation">
		<span class="screen-reader-text"><?php _e( 'Posts navigation', 'startbiz' ); ?></span>
		<div class="nav-links">

			<?php if ( get_next_posts_link() ) : ?>
			<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'startbiz' ) ); ?></div>
			<?php endif; ?>

			<?php if ( get_previous_posts_link() ) : ?>
			<div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'startbiz' ) ); ?></div>
			<?php endif; ?>

		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
	
	
	endif;
}
endif;
/**
|------------------------------------------------------------------------------
| Infinite loading and more button
|------------------------------------------------------------------------------
| 
| @return void
|
*/

function tc_infinite_loading($load_style = 'loadding') {
	global $wp_query;
	$totalPages = $wp_query->max_num_pages;

	if ($totalPages > 1) :

		if ($load_style != 'infinite'):
		?>
			<div id="load-more-wrap">
				<a id="load-more-post" href="#" data-loading="<?php _e('Loading...', 'startbiz') ?>" data-more="Load More..."><?php _e('Load More...', 'startbiz') ?></a>
			</div>
			
		<?php

		endif;
		?>

		<script type="text/javascript">
			var totalPages = <?php echo $totalPages ?>;
			var loadStyle = '<?php echo $load_style; ?>';
		</script>

		<?php

	endif;
}

if (! function_exists('tc_infinitepaginate')):

	/**
	|------------------------------------------------------------------------------
	| Ajax Infinite Scroll
	|------------------------------------------------------------------------------
	| 
	| @return void
	|
	*/

	function tc_infinitepaginate(){ 
		

	    $loopFile        = $_POST['loop_file'];
	    $paged           = $_POST['page_no'];
	    $posts_per_page  = get_option('posts_per_page');
	    

	    $post_format = tc_get_option('layout-view-sidebar') == 'grid' ? 'grid' : get_post_format();
	 
	    # Load the posts
	    query_posts(array('paged' => $paged ));


		while ( have_posts() ) {
				the_post();
		   		get_template_part( $loopFile, $post_format );
		}

	    exit;
	}

	add_action('wp_ajax_infinite_scroll', 'tc_infinitepaginate');        // for logged in user
	add_action('wp_ajax_nopriv_infinite_scroll', 'tc_infinitepaginate');

endif;
if ( ! function_exists( 'tcc_the_post_navigation' ) ) :
/**
 |------------------------------------------------------------------------------
 | Display navigation to next/previous post when applicable.
 |------------------------------------------------------------------------------
 | @todo Remove this function when WordPress 4.3 is released.
 |
 */
function tcc_the_post_navigation() {

	// Don't print empty markup if there's nowhere to navigate.
	$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );

	if ( ! $next && ! $previous ) {
		return;
	}
	?>
	<nav class="post-navigation clearfix" role="navigation">
		<h2 class="screen-reader-text"><?php _e( 'Post navigation', 'startbiz' ); ?></h2>
		<div class="nav-links next-prev-post">
			<?php
				previous_post_link( '<div class="nav-previous">%link</div>', '<span>« Previous</span>' );
				next_post_link( '<div class="nav-next">%link</div>', '<span>Next »</span>' );
			?>
		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}
endif;

if ( ! function_exists( 'startbiz_posted_on' ) ) :
/**
|------------------------------------------------------------------------------
| Prints HTML with meta information for the current post-date/time and author.
|------------------------------------------------------------------------------
|
 */
function startbiz_posted_on() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);
	
	$posted_on = sprintf(
		_x( '<i class="fa fa-calendar"></i> %s&nbsp;', 'post date', 'themecountry' ),
		'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
	);

	$byline = sprintf(
		_x( '&nbsp;<i class="fa fa-user"></i> %s&nbsp;', 'post author', 'themecountry' ),
		'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
	);

	$cat_list = '';
	$tg_list = '';
	// Hide category and tag text for pages.
	if ( 'post' == get_post_type() ) {
		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_category_list( __( ', ', 'startbiz' ) );

		if ( ($categories_list && startbiz_categorized_blog())) {
			$cat_list  = sprintf( '&nbsp;<i class="fa fa-external-link"></i> <span class="cat-links info-posted">' . __( '<span class="info-set">In</span> %1$s&nbsp;', 'startbiz' ) . '</span>', $categories_list );
		}

		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list( '', __( ', ', 'startbiz' ) );
		if ( $tags_list ) {
			$tg_list = sprintf( '&nbsp;<i class="fa fa-tags"></i> <span class="tags-links info-posted">' . __( '<span class="info-set">Tagged</span> %1$s&nbsp;', 'startbiz' ) . '</span>', $tags_list );
		}
	}

	if ( is_single() ) {
		
		$meta_options = is_array(tc_get_option('post-meta-info')) ? tc_get_option('post-meta-info') : array();


	} else {

		$meta_options = is_array(tc_get_option('blog_post_meta_info')) ? tc_get_option('blog_post_meta_info') : array();

	}

	if (is_array($meta_options)) {
		$meta_options = array_flip($meta_options);
	}

	if (!isset($meta_options['autho_name'])) {
		echo '<span class="byline info-posted">' . $byline . '</span>';
	}

	if (!isset($meta_options['date'])) {
		echo '<span class="posted-on info-posted"> ' . $posted_on . '</span>';
	}

	if (!isset($meta_options['category'])) {
		echo $cat_list;
	}

	if (!isset($meta_options['tag'])) {
		echo $tg_list;
	}

	if ( ! post_password_required() && ! isset($meta_options['comment_count']) && ( comments_open() || get_comments_number() ) ) {
		echo '&nbsp;<i class="fa fa-comments"></i> <span class="comments-link info-posted">';
			comments_popup_link( __( 'Leave a comment', 'startbiz' ), __( '1 Comment', 'startbiz' ), __( '% Comments', 'startbiz' ) );
		echo '</span>';

	}

	edit_post_link( __( '&nbsp;Edit', 'startbiz' ), '<span class="edit-link info-posted">', '</span>' );
	
}
endif;







if ( ! function_exists( 'startbiz_related_posts_meta_info' ) ) :
/**
|------------------------------------------------------------------------------
| Prints HTML with meta information for the current post-date/time and author.
|------------------------------------------------------------------------------
|
 */
function startbiz_related_posts_meta_info() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);
	
	$posted_on = sprintf(
		_x( '<i class="fa fa-calendar"></i> %s&nbsp;', 'post date', 'themecountry' ),
		'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
	);

	$cat_list = '';
	$tg_list = '';
	// Hide category and tag text for pages.
	if ( 'post' == get_post_type() ) {
		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_category_list( __( ', ', 'startbiz' ) );

		if ( ($categories_list && startbiz_categorized_blog())) {
			$cat_list  = sprintf( '&nbsp;<i class="fa fa-external-link"></i> <span class="cat-links info-posted">' . __( '<span class="info-set">In</span> %1$s', 'startbiz' ) . '</span>', $categories_list );
		}
	}

	if ( is_single() ) {
		
		$meta_options = is_array(tc_get_option('post-meta-info')) ? tc_get_option('post-meta-info') : array();

	} else {

		$meta_options = is_array(tc_get_option('blog_post_meta_info')) ? tc_get_option('blog_post_meta_info') : array();

	}

	if (is_array($meta_options)) {
		$meta_options = array_flip($meta_options);
	}

	if (!isset($meta_options['date'])) {
		echo '<span class="posted-on info-posted"> ' . $posted_on . '</span>';
	}

	if (!isset($meta_options['category'])) {
		echo $cat_list;
	}
}
endif;











if ( ! function_exists( 'the_archive_title' ) ) :
/**
 |------------------------------------------------------------------------------
 | Shim for `the_archive_title()`.
 |------------------------------------------------------------------------------
 | Display the archive title based on the queried object.
 |
 | @todo Remove this function when WordPress 4.3 is released.
 |
 | @param string $before Optional. Content to prepend to the title. Default empty.
 | @param string $after  Optional. Content to append to the title. Default empty.
 |
 */
function the_archive_title( $before = '', $after = '' ) {
	if ( is_category() ) {
		$title = sprintf( __( 'Category: %s', 'startbiz' ), single_cat_title( '', false ) );
	} elseif ( is_tag() ) {
		$title = sprintf( __( 'Tag: %s', 'startbiz' ), single_tag_title( '', false ) );
	} elseif ( is_author() ) {
		$title = sprintf( __( 'Author: %s', 'startbiz' ), '<span class="vcard">' . get_the_author() . '</span>' );
	} elseif ( is_year() ) {
		$title = sprintf( __( 'Year: %s', 'startbiz' ), get_the_date( _x( 'Y', 'yearly archives date format', 'startbiz' ) ) );
	} elseif ( is_month() ) {
		$title = sprintf( __( 'Month: %s', 'startbiz' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'startbiz' ) ) );
	} elseif ( is_day() ) {
		$title = sprintf( __( 'Day: %s', 'startbiz' ), get_the_date( _x( 'F j, Y', 'daily archives date format', 'startbiz' ) ) );
	} elseif ( is_tax( 'post_format' ) ) {
		if ( is_tax( 'post_format', 'post-format-aside' ) ) {
			$title = _x( 'Asides', 'post format archive title', 'startbiz' );
		} elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) {
			$title = _x( 'Galleries', 'post format archive title', 'startbiz' );
		} elseif ( is_tax( 'post_format', 'post-format-image' ) ) {
			$title = _x( 'Images', 'post format archive title', 'startbiz' );
		} elseif ( is_tax( 'post_format', 'post-format-video' ) ) {
			$title = _x( 'Videos', 'post format archive title', 'startbiz' );
		} elseif ( is_tax( 'post_format', 'post-format-quote' ) ) {
			$title = _x( 'Quotes', 'post format archive title', 'startbiz' );
		} elseif ( is_tax( 'post_format', 'post-format-link' ) ) {
			$title = _x( 'Links', 'post format archive title', 'startbiz' );
		} elseif ( is_tax( 'post_format', 'post-format-status' ) ) {
			$title = _x( 'Statuses', 'post format archive title', 'startbiz' );
		} elseif ( is_tax( 'post_format', 'post-format-audio' ) ) {
			$title = _x( 'Audio', 'post format archive title', 'startbiz' );
		} elseif ( is_tax( 'post_format', 'post-format-chat' ) ) {
			$title = _x( 'Chats', 'post format archive title', 'startbiz' );
		}
	} elseif ( is_post_type_archive() ) {
		$title = sprintf( __( 'Archives: %s', 'startbiz' ), post_type_archive_title( '', false ) );
	} elseif ( is_tax() ) {
		$tax = get_taxonomy( get_queried_object()->taxonomy );
		/* translators: 1: Taxonomy singular name, 2: Current taxonomy term */
		$title = sprintf( __( '%1$s: %2$s', 'startbiz' ), $tax->labels->singular_name, single_term_title( '', false ) );
	} else {
		$title = __( 'Archives', 'startbiz' );
	}

	/**
	 * Filter the archive title.
	 *
	 * @param string $title Archive title to be displayed.
	 */
	$title = apply_filters( 'get_the_archive_title', $title );

	if ( ! empty( $title ) ) {
		echo $before . $title . $after;
	}
}
endif;

if ( ! function_exists( 'the_archive_description' ) ) :
/**
 |------------------------------------------------------------------------------
 | Shim for `the_archive_description()`.
 |------------------------------------------------------------------------------
 | Display category, tag, or term description.
 |
 | @todo Remove this function when WordPress 4.3 is released.
 |
 | @param string $before Optional. Content to prepend to the description. Default empty.
 | @param string $after  Optional. Content to append to the description. Default empty.
 */
function the_archive_description( $before = '', $after = '' ) {
	$description = apply_filters( 'get_the_archive_description', term_description() );

	if ( ! empty( $description ) ) {
		/**
		 * Filter the archive description.
		 *
		 * @see term_description()
		 *
		 * @param string $description Archive description to be displayed.
		 */
		echo $before . $description . $after;
	}
}
endif;

/**
 |------------------------------------------------------------------------------
 | Returns true if a blog has more than 1 category.
 |------------------------------------------------------------------------------
 |
 | @return bool
 |
 */
function startbiz_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'startbiz_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,

			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'startbiz_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so startbiz_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so startbiz_categorized_blog should return false.
		return false;
	}
}

/**
 |------------------------------------------------------------------------------
 | Flush out the transients used in startbiz_categorized_blog.
 |------------------------------------------------------------------------------
 |
 */
function startbiz_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'startbiz_categories' );
}
add_action( 'edit_category', 'startbiz_category_transient_flusher' );
add_action( 'save_post',     'startbiz_category_transient_flusher' );

if ( ! function_exists( 'tcc_excerpt_more' ) ) {
	/**
	|------------------------------------------------------------------------------
	| Excerpt ending
	|------------------------------------------------------------------------------
	| 
	| @return string
	|
	*/

	function tc_excerpt_more( $more ) {

		return tc_get_option('excerpt_more');
	}
	
}

add_filter( 'excerpt_more', 'tc_excerpt_more' );

if ( ! function_exists( 'tc_excerpt_length' ) ) {

	/**
	|------------------------------------------------------------------------------
	| Excerpt length
	|------------------------------------------------------------------------------
	| 
	| @return integer
	|
	*/

	function tc_excerpt_length() {

		if (function_exists('tc_get_option')) {
			return tc_get_option('excerpt_length');
		} 

		return 26;
	}
	
}

add_filter( 'excerpt_length', 'tc_excerpt_length', 999 );

if (!function_exists('tc_social_icons')) :
	/**
	|------------------------------------------------------------------------------
	| Social Sharing Buttons
	|------------------------------------------------------------------------------
	|
	*/
	function tc_social_icons($position) {
		global $post;
		if (tc_get_option('social-button-enable') != 'off') :
	?>


		<div class="tc-social-sharing <?php echo $position ?>">
			<ul class="tc-social-icons">
				<?php if (tc_get_option('social-facebook') != 'off') : ?>
				<li class="facebook">
					<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_the_permalink()) ?>" class="social-popup" target="_blank">
						<i class="fa fa-facebook"></i>
				        <span class="text">facebook</span>
					</a>
				</li>
				<?php  endif; ?>
				<?php if (tc_get_option('social-twitter') != 'off') : ?>
				<li class="twitter">
					<a href="http://twitter.com/share?url=<?php echo urlencode(get_the_permalink()) ?>&via=<?php echo tc_get_option('twitter-username') ?>&text=<?php the_title() ?>" class="social-popup" target="_blank">
						<i class="fa fa-twitter"></i>
				        <span class="text">tweet</span>
					</a>
				</li> 
				<?php endif; ?>
				<?php if (tc_get_option('social-google-plus') != 'off') : ?>
				<li class="googleplus">
					<a href="https://plus.google.com/share?url=<?php echo urlencode(get_the_permalink()) ?>" class="social-popup" target="_blank">
						<i class="fa fa-google-plus"></i>
				        <span class="text">google+</span>
					</a>
				</li>
				<?php endif; ?>
				<?php if (tc_get_option('social-pinterest') != 'off') : ?>
				<li class="pinterest">
					<a href="#" class="social-pinterst" target="_blank">
						<i class="fa fa-pinterest"></i>
				        <span class="text">pinterest</span>
					</a>
				</li>
				<?php endif; ?>
				<?php if (tc_get_option('social-linkedin') != 'off') : ?>
				<li class="linkedin">
					<a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode(get_the_permalink()) ?>&title=<?php the_title() ?>&summary=<?php echo get_the_excerpt() ?>" class="social-popup" target="_blank">
						<i class="fa fa-linkedin"></i>
				        <span class="text">linkedin</span>
					</a>
				</li>
				<?php endif; ?>
			</ul>
		</div>
		<?php
		endif;
	}
endif;
