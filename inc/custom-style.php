<?php 

if ( ! function_exists( 'startbiz_header_style' ) ) :

/**
|------------------------------------------------------------------------------
| Hook theme option style to head
|------------------------------------------------------------------------------
 */

function tcc_header_style() {
	//$skin_name = tc_get_option('header_style') ? tc_get_option('header_style') : 'default';
	tcc_custom_style();
	
}
add_action('wp_head','tcc_header_style');
endif;

/**
|------------------------------------------------------------------------------
| Generate custom style from theme option
|------------------------------------------------------------------------------
 */

function tcc_custom_style() {
	?>
	<style type="text/css">
		<?php
			if (tc_get_option('default_primary_color') != '#42a5f6') :
		?>

			.post-excerpt .more-link,
			.nav-previous a,
			.nav-next a,
			input[type="submit"],
			.nav-links .page-numbers {
				background-color: <?php echo tc_get_option('default_primary_color'); ?>;
				border: none;
			}

		<?php
			endif;
		?>
		<?php
			if (tc_get_option('author-highlight') != 'off') :
		?>
			.bypostauthor article {				
				padding: 10px;
				margin-bottom: 10px;
			}
			.bypostauthor p {
				margin-bottom: 10px;
			}
		<?php
			endif;
		?>
		
		hr {
			border-color: <?php echo tc_get_option('default_primary_color'); ?>;
		}

		.feature-service-box h3 a,
		.rm-slide-services,
		.feature-service-box:hover .service-box-shape a .fa,
		.feature-service-box .service-box-shape a,
		#features .customNavigation .owl-prev i,
		#features .customNavigation .owl-next i,
		.member-social-icon ul li a,
		.wrap-contact-header .call-contact i,
		.wrap-contact-header .mail-contact i {
			color: <?php echo tc_get_option('default_primary_color') ?>;
		}
		#features .customNavigation .owl-next, #features .customNavigation .owl-prev {
			border: 2px solid <?php echo tc_get_option('default_primary_color') ?>;
		}
		.btn,
		.back-to-top,
		caption,
		.custom-subscribe-box,
		.navbar-toggle,
		.featured-apps,
		.bottom-border:after,
		.member-detail .member-title:before,
		.feature-service-box .service-box-shape a,
		.feature-service-box:hover .service-box-shape:before {
			background-color: <?php echo tc_get_option('default_primary_color'); ?>;
		}
		.main-navigation li.current-menu-item > a,
		.main-navigation li:hover > a,
		.main-navigation li li:hover > a {
			color: <?php echo tc_get_option('default_active_hover_color'); ?>;
		}
		.bottom-border:after {
			background-color: <?php echo tc_get_option('default_active_hover_color'); ?>;
		}

		.intro-banner .header-content .header-content-inner h2 {
			color: <?php echo tc_get_option('home_first_banner_color'); ?>;
		}

		.intro-banner .header-content .header-content-inner p {
			color: <?php echo tc_get_option('home_first_desc_color_banner'); ?>;
		}
		
		<?php echo ot_get_option('custom_style') ?>;

	</style>
	<?php
}