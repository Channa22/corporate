<?php

/*
|
| Plugin Name: ThemeCountry 125x125 Banner Widget
| Description: A widget for 125 x 125 banner unit (maximum 6 banners)
| Version: 1.0
|
*/

/*
|------------------------------------------------------------------------------
| Banner Widget Class
|------------------------------------------------------------------------------
*/
class tc_banner_widget extends WP_Widget {


	/*
	|------------------------------------------------------------------------------
	| Widget Setup
	|------------------------------------------------------------------------------
	|
	| @return void
	|
	*/
	public function tc_Banner_widget() {

		// Widget settings
		$widget_ops = array (
			'classname' => 'tc_banner_widget',
			'description' => __('A widget for 125 x 125 banner unit (max 6 ads)', 'startbiz')
		);

		// Widget control settings
		$control_ops = array (
			'width' => 300,
			'height' => 350,
			'id_base' => 'tc_banner_widget'
		);

		// Create the widget
		$this->WP_Widget( 'tc_banner_widget', __('ThemeCountry: 125x125 Banner', 'startbiz'), $widget_ops, $control_ops );
		
	}

	/*
	|------------------------------------------------------------------------------
	|	Display Widget
	|------------------------------------------------------------------------------
	|
	| @return void
	|
	*/
	public function widget( $args, $instance ) {
		extract( $args );

		// variables from the widget settings
		$title = apply_filters('widget_title', $instance['title'] );
		$banner1 = $instance['banner1'];
		$banner2 = $instance['banner2'];
		$banner3 = $instance['banner3'];
		$banner4 = $instance['banner4'];
		$banner5 = $instance['banner5'];
		$banner6 = $instance['banner6'];
		$link1 = $instance['link1'];
		$link2 = $instance['link2'];
		$link3 = $instance['link3'];
		$link4 = $instance['link4'];
		$link5 = $instance['link5'];
		$link6 = $instance['link6'];
		$target1 = $instance['target1'] ? 'target="_blank"' : '' ;
		$target2 = $instance['target2'] ? 'target="_blank"' : '' ;
		$target3 = $instance['target3'] ? 'target="_blank"' : '' ;
		$target4 = $instance['target4'] ? 'target="_blank"' : '' ;
		$target5 = $instance['target5'] ? 'target="_blank"' : '' ;
		$target6 = $instance['target6'] ? 'target="_blank"' : '' ;
		$randomize = $instance['random'];



		// Before widget (defined by theme functions file)
		echo $before_widget;

		// Display the widget title if one was input
		if ( $title )
			echo $before_title . $title . $after_title;

		// Randomize banners order in a new array
		$banners = array();
			
		// Display a containing div
		echo '<div class="banner-125">';
		
		echo '<ul>';

		// Display banner 1
		if ( $link1 )
			$banners[] = '<li class="odd-banner"><a href="' . $link1 . '" '. $target1 .'><img src="' . $banner1 . '" width="125" height="125" alt="" /></a></li>';
			
		elseif ( $banner1 )
		 	$banners[] = '<li class="odd-banner"><img src="' . $banner1 . '" width="125" height="125" alt="" /></li>';
		
		// Display banner 2
		if ( $link2 )
			$banners[] = '<li class="even-banner"><a href="' . $link2 . '" '. $target2 .'><img src="' . $banner2 . '" width="125" height="125" alt="" /></a></li>';
			
		elseif ( $banner2 )
		 	$banners[] = '<li class="even-banner"><img src="' . $banner2 . '" width="125" height="125" alt="" /></li>';
			
		// Display banner 3
		if ( $link3 )
			$banners[] = '<li class="odd-banner"><a href="' . $link3 . '" '. $target3 .'><img src="' . $banner3 . '" width="125" height="125" alt="" /></a></li>';
			
		elseif ( $banner3 )
		 	$banners[] = '<li class="odd-banner"><img src="' . $banner3 . '" width="125" height="125" alt="" /></li>';
			
		// Display banner 4
		if ( $link4 )
			$banners[] = '<li class="even-banner"><a href="' . $link4 . '" '. $target4 .'><img src="' . $banner4 . '" width="125" height="125" alt="" /></a></li>';
			
		elseif ( $banner4 )
		 	$banners[] = '<li class="even-banner"><img src="' . $banner4 . '" width="125" height="125" alt="" /></li>';
			
		// Display banner 5
		if ( $link5 )
			$banners[] = '<li class="odd-banner"><a href="' . $link5 . '" '. $target5 .'><img src="' . $banner5 . '" width="125" height="125" alt="" /></a></li>';
			
		elseif ( $banner5 )
		 	$banners[] = '<li class="odd-banner"><img src="' . $banner5 . '" width="125" height="125" alt="" /></li>';
			
		// Display banner 6
		if ( $link6 )
			$banners[] = '<li class="even-banner"><a href="' . $link6 . '" '. $target6 .'><img src="' . $banner6 . '" width="125" height="125" alt="" /></a></li>';
			
		elseif ( $banner6 )
		 	$banners[] = '<li class="even-banner"><img src="' . $banner6 . '" width="125" height="125" alt="" /></li>';
		
		// Randomize order if selected
		if ($randomize){
			shuffle($banners);
		}
		
		//Display banners
		foreach($banners as $banner){
			echo $banner;
		}
		
		echo '</ul>';
			
		echo '</div>';

		// After widget (defined by theme functions file)
		echo $after_widget;
	}

	/*
	|------------------------------------------------------------------------------
	| Update Widget
	|------------------------------------------------------------------------------
	| 
	| @return void
	|
	*/
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		// Strip tags to remove HTML (important for text inputs)
		$instance['title'] = strip_tags( $new_instance['title'] );

		// No need to strip tags
		$instance['banner1'] = $new_instance['banner1'];
		$instance['banner2'] = $new_instance['banner2'];
		$instance['banner3'] = $new_instance['banner3'];
		$instance['banner4'] = $new_instance['banner4'];
		$instance['banner5'] = $new_instance['banner5'];
		$instance['banner6'] = $new_instance['banner6'];
		$instance['link1'] = $new_instance['link1'];
		$instance['link2'] = $new_instance['link2'];
		$instance['link3'] = $new_instance['link3'];
		$instance['link4'] = $new_instance['link4'];
		$instance['link5'] = $new_instance['link5'];
		$instance['link6'] = $new_instance['link6'];
		$instance['target1'] = $new_instance['target1'];
		$instance['target2'] = $new_instance['target2'];
		$instance['target3'] = $new_instance['target3'];
		$instance['target4'] = $new_instance['target4'];
		$instance['target5'] = $new_instance['target5'];
		$instance['target6'] = $new_instance['target6'];
		$instance['random'] = $new_instance['random'];
		
		return $instance;
	}

	/*
	|------------------------------------------------------------------------------
	| Widget Settings 
	|------------------------------------------------------------------------------
	|
	| Displays the widget settings controls on the widget panel
	|
	| @return void
	|
	*/
		
	public function form( $instance ) {

		// Set up some default widget settings
		$defaults = array(
			'title' => __('Our Sponsors','startbiz'),
			'banner1' => get_template_directory_uri()."/images/125x125.png",
			'link1' => 'http://themecountry.com/',
			'target1' => false,
			'banner2' => get_template_directory_uri()."/images/125x125.png",
			'link2' => 'http://themecountry.com/',
			'target2' => false,
			'banner3' => '',
			'link3' => '',
			'target3' => false,
			'banner4' => '',
			'link4' => '',
			'target4' => false,
			'banner5' => '',
			'link5' => '',
			'target5' => false,
			'banner6' => '',
			'link6' => '',
			'target1' => false,
			'random' => false
		);
			
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'themecountry') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" />
		</p>

		<!-- Banner 1 image url: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'banner1' ); ?>"><?php _e('Banner 1 image url:', 'themecountry') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'banner1' ); ?>" name="<?php echo $this->get_field_name( 'banner1' ); ?>" value="<?php echo $instance['banner1']; ?>" />
		</p>
		
		<!-- Banner 1 link url: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'link1' ); ?>"><?php _e('Banner 1 link url:', 'themecountry') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'link1' ); ?>" name="<?php echo $this->get_field_name( 'link1' ); ?>" value="<?php echo $instance['link1']; ?>" />
		</p>
		<!-- Target 1 : Checkbox -->
		<p>
			<label for="<?php echo $this->get_field_id( 'target1' ); ?>"><?php _e('Open New Window:', 'themecountry') ?></label>
			<?php if ($instance['target1']){ ?>
				<input type="checkbox" id="<?php echo $this->get_field_id( 'target1' ); ?>" name="<?php echo $this->get_field_name( 'target1' ); ?>" checked="checked" />
			<?php } else { ?>
				<input type="checkbox" id="<?php echo $this->get_field_id( 'target1' ); ?>" name="<?php echo $this->get_field_name( 'target1' ); ?>"  />
			<?php } ?>
		</p>
		
		<!-- Banner 2 image url: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'banner2' ); ?>"><?php _e('banner 2 image url:', 'themecountry') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'banner2' ); ?>" name="<?php echo $this->get_field_name( 'banner2' ); ?>" value="<?php echo $instance['banner2']; ?>" />
		</p>
		
		<!-- Banner 2 link url: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'link2' ); ?>"><?php _e('banner 2 link url:', 'themecountry') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'link2' ); ?>" name="<?php echo $this->get_field_name( 'link2' ); ?>" value="<?php echo $instance['link2']; ?>" />
		</p>
		<!-- Target 2 : Checkbox -->
		<p>
			<label for="<?php echo $this->get_field_id( 'target2' ); ?>"><?php _e('Open New Window:', 'themecountry') ?></label>
			<?php if ($instance['target2']){ ?>
				<input type="checkbox" id="<?php echo $this->get_field_id( 'target2' ); ?>" name="<?php echo $this->get_field_name( 'target2' ); ?>" checked="checked" />
			<?php } else { ?>
				<input type="checkbox" id="<?php echo $this->get_field_id( 'target2' ); ?>" name="<?php echo $this->get_field_name( 'target2' ); ?>"  />
			<?php } ?>
		</p>
		
		<!-- Banner 3 image url: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'banner3' ); ?>"><?php _e('banner 3 image url:', 'themecountry') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'banner3' ); ?>" name="<?php echo $this->get_field_name( 'banner3' ); ?>" value="<?php echo $instance['banner3']; ?>" />
		</p>
		
		<!-- Banner 3 link url: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'link3' ); ?>"><?php _e('banner 3 link url:', 'themecountry') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'link3' ); ?>" name="<?php echo $this->get_field_name( 'link3' ); ?>" value="<?php echo $instance['link3']; ?>" />
		</p>
		<!-- Target 3 : Checkbox -->
		<p>
			<label for="<?php echo $this->get_field_id( 'target3' ); ?>"><?php _e('Open New Window:', 'themecountry') ?></label>
			<?php if ($instance['target3']){ ?>
				<input type="checkbox" id="<?php echo $this->get_field_id( 'target3' ); ?>" name="<?php echo $this->get_field_name( 'target3' ); ?>" checked="checked" />
			<?php } else { ?>
				<input type="checkbox" id="<?php echo $this->get_field_id( 'target3' ); ?>" name="<?php echo $this->get_field_name( 'target3' ); ?>"  />
			<?php } ?>
		</p>
		
		<!-- Banner 4 image url: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'banner4' ); ?>"><?php _e('banner 4 image url:', 'themecountry') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'banner4' ); ?>" name="<?php echo $this->get_field_name( 'banner4' ); ?>" value="<?php echo $instance['banner4']; ?>" />
		</p>
		
		<!-- Banner 4 link url: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'link4' ); ?>"><?php _e('banner 4 link url:', 'themecountry') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'link4' ); ?>" name="<?php echo $this->get_field_name( 'link4' ); ?>" value="<?php echo $instance['link4']; ?>" />
		</p>
		<!-- Target 4 : Checkbox -->
		<p>
			<label for="<?php echo $this->get_field_id( 'target5' ); ?>"><?php _e('Open New Window:', 'themecountry') ?></label>
			<?php if ($instance['target3']){ ?>
				<input type="checkbox" id="<?php echo $this->get_field_id( 'target5' ); ?>" name="<?php echo $this->get_field_name( 'target5' ); ?>" checked="checked" />
			<?php } else { ?>
				<input type="checkbox" id="<?php echo $this->get_field_id( 'target5' ); ?>" name="<?php echo $this->get_field_name( 'target5' ); ?>"  />
			<?php } ?>
		</p>
		
		<!-- Banner 5 image url: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'banner5' ); ?>"><?php _e('banner 5 image url:', 'themecountry') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'banner5' ); ?>" name="<?php echo $this->get_field_name( 'banner5' ); ?>" value="<?php echo $instance['banner5']; ?>" />
		</p>
		
		<!-- Banner 5 link url: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'link5' ); ?>"><?php _e('banner 5 link url:', 'themecountry') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'link5' ); ?>" name="<?php echo $this->get_field_name( 'link5' ); ?>" value="<?php echo $instance['link5']; ?>" />
		</p>
		<!-- Target 5 : Checkbox -->
		<p>
			<label for="<?php echo $this->get_field_id( 'target1' ); ?>"><?php _e('Open New Window:', 'themecountry') ?></label>
			<?php if ($instance['target1']){ ?>
				<input type="checkbox" id="<?php echo $this->get_field_id( 'target5' ); ?>" name="<?php echo $this->get_field_name( 'target5' ); ?>" checked="checked" />
			<?php } else { ?>
				<input type="checkbox" id="<?php echo $this->get_field_id( 'target5' ); ?>" name="<?php echo $this->get_field_name( 'target5' ); ?>"  />
			<?php } ?>
		</p>
		<!-- Banner 6 image url: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'banner6' ); ?>"><?php _e('banner 6 image url:', 'themecountry') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'banner6' ); ?>" name="<?php echo $this->get_field_name( 'banner6' ); ?>" value="<?php echo $instance['banner6']; ?>" />
		</p>
		
		<!-- banner 6 link url: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'link6' ); ?>"><?php _e('banner 6 link url:', 'themecountry') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'link6' ); ?>" name="<?php echo $this->get_field_name( 'link6' ); ?>" value="<?php echo $instance['link6']; ?>" />
		</p>
		<!-- Target 6 : Checkbox -->
		<p>
			<label for="<?php echo $this->get_field_id( 'target6' ); ?>"><?php _e('Open New Window:', 'themecountry') ?></label>
			<?php if ($instance['target6']){ ?>
				<input type="checkbox" id="<?php echo $this->get_field_id( 'target6' ); ?>" name="<?php echo $this->get_field_name( 'target6' ); ?>" checked="checked" />
			<?php } else { ?>
				<input type="checkbox" id="<?php echo $this->get_field_id( 'target6' ); ?>" name="<?php echo $this->get_field_name( 'target6' ); ?>"  />
			<?php } ?>
		</p>
		<!-- Randomize? -->
		<p>
			<label for="<?php echo $this->get_field_id( 'random' ); ?>"><?php _e('Randomize banners order?', 'themecountry') ?></label>
			<?php if ($instance['random']){ ?>
				<input type="checkbox" id="<?php echo $this->get_field_id( 'random' ); ?>" name="<?php echo $this->get_field_name( 'random' ); ?>" checked="checked" />
			<?php } else { ?>
				<input type="checkbox" id="<?php echo $this->get_field_id( 'random' ); ?>" name="<?php echo $this->get_field_name( 'random' ); ?>"  />
			<?php } ?>
		</p>
		
	<?php
	}
}
/*
|------------------------------------------------------------------------------
| Load Widgets
|------------------------------------------------------------------------------
*/
add_action( 'widgets_init', 'tc_banner_widgets' );

/*
 |------------------------------------------------------------------------------
 | Register widget
 |------------------------------------------------------------------------------
 |
 | @return void
 |
 */
function tc_banner_widgets() {
	register_widget( 'tc_banner_widget' );
}
?>