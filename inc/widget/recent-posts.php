<?php
/*
|
|	Plugin Name: ThemeCountry Recent Posts
|	Description: A widget to display Recent Posts.
|	Version: 1.0
|
*/

/*
|------------------------------------------------------------------------------
| Recent Posts Widget Class
|------------------------------------------------------------------------------
*/

class tc_Recent_Posts_Widget extends WP_Widget {


	/*
	|------------------------------------------------------------------------------
	| Widget Setup
	|------------------------------------------------------------------------------
	|
	| @return void
	|
	*/
	public function tc_Recent_Posts_Widget() {
		$widget_ops = array(
			'classname' => 'tc-recent-posts-widget', 
			'description' => __('ThemeCountry Recent Posts.','startbiz')
		);

		$control_ops = array(
			'id_base' => 'tc-recent-posts'
			);

		$this->WP_Widget('tc-recent-posts', __('ThemeCountry: Recent Posts','startbiz'), $widget_ops, $control_ops);
	}

	/*
	|------------------------------------------------------------------------------
	| Display Widget
	|------------------------------------------------------------------------------
	|
	| @return void
	|
	*/
	public function widget( $args, $instance ) {
		extract( $args );

		$title = apply_filters( 'widget_title', $instance['title'] );
		$comment_num = $instance['comment_num'];
		$date = $instance['date'];
		$qty = (int) $instance['qty'];
		$show_thumbnail_1 = (int) $instance['show_thumbnail_1'];
		$show_excerpt = $instance['show_excerpt'];
		$excerpt_length = $instance['excerpt_length'];

		echo $before_widget;
		if ( ! empty( $title ) ) {
			echo $before_title . $title . $after_title;
		}

		echo self::tc_get_recent_posts( $qty, $comment_num, $date, $show_thumbnail_1, $show_excerpt, $excerpt_length );
		echo $after_widget;

	}
    /*
	|------------------------------------------------------------------------------
	| Update Widget
	|------------------------------------------------------------------------------
	|
	| @return void
	|
	*/
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['qty'] = intval( $new_instance['qty'] );
		$instance['comment_num'] = intval( $new_instance['comment_num'] );
		$instance['date'] = intval( $new_instance['date'] );
		$instance['show_thumbnail_1'] = intval( $new_instance['show_thumbnail_1'] );
		$instance['show_excerpt'] = intval( $new_instance['show_excerpt'] );
		$instance['excerpt_length'] = intval( $new_instance['excerpt_length'] );
		return $instance;
	}

	/*
	|------------------------------------------------------------------------------
	| Widget Settings 
	|------------------------------------------------------------------------------
	|
	| Displays the widget settings controls on the widget panel
	| 
	| @return void
	|
	*/
 	public function form( $instance ) {
		$defaults = array(
			'comment_num' => 1,
			'date' => 1,
			'show_thumbnail_1' => 1,
			'show_excerpt' => 0,
			'excerpt_length' => 10
		);

		$instance = wp_parse_args((array) $instance, $defaults);
		$title = isset( $instance[ 'title' ] ) ? $instance[ 'title' ] : __( 'Recent Posts','startbiz' );
		$qty = isset( $instance[ 'qty' ] ) ? esc_attr( $instance[ 'qty' ] ) : 5;
		$comment_num = isset( $instance[ 'comment_num' ] ) ? esc_attr( $instance[ 'comment_num' ] ) : 1;
		$show_excerpt = isset( $instance[ 'show_excerpt' ] ) ? esc_attr( $instance[ 'show_excerpt' ] ) : 1;
		$date = isset( $instance[ 'date' ] ) ? esc_attr( $instance[ 'date' ] ) : 1;
		$excerpt_length = isset( $instance[ 'excerpt_length' ] ) ? intval( $instance[ 'excerpt_length' ] ) : 10;
		$show_thumbnail_1 = isset( $instance[ 'show_thumbnail_1' ] ) ? esc_attr( $instance[ 'show_thumbnail_1' ] ) : 1;
		$show_excerpt = isset( $instance[ 'show_excerpt' ] ) ? esc_attr( $instance[ 'show_excerpt' ] ) : 1;
		$excerpt_length = isset( $instance[ 'excerpt_length' ] ) ? intval( $instance[ 'excerpt_length' ] ) : 10;
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:','startbiz' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'qty' ); ?>"><?php _e( 'Number of Posts to show','startbiz' ); ?></label> 
			<input id="<?php echo $this->get_field_id( 'qty' ); ?>" name="<?php echo $this->get_field_name( 'qty' ); ?>" type="number" min="1" step="1" value="<?php echo $qty; ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id("show_thumbnail_1"); ?>">
				<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id("show_thumbnail_1"); ?>" name="<?php echo $this->get_field_name("show_thumbnail_1"); ?>" value="1" <?php if (isset($instance['show_thumbnail_1'])) { checked( 1, $instance['show_thumbnail_1'], true ); } ?> />
				<?php _e( 'Show Thumbnails', 'startbiz'); ?>
			</label>
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id("date"); ?>">
				<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id("date"); ?>" name="<?php echo $this->get_field_name("date"); ?>" value="1" <?php checked( 1, $instance['date'], true ); ?> />
				<?php _e( 'Show post date', 'startbiz'); ?>
			</label>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id("comment_num"); ?>">
				<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id("comment_num"); ?>" name="<?php echo $this->get_field_name("comment_num"); ?>" value="1" <?php checked( 1, $instance['comment_num'], true ); ?> />
				<?php _e( 'Show number of comments', 'startbiz'); ?>
			</label>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id("show_excerpt"); ?>">
				<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id("show_excerpt"); ?>" name="<?php echo $this->get_field_name("show_excerpt"); ?>" value="1" <?php checked( 1, $instance['show_excerpt'], true ); ?> />
				<?php _e( 'Show excerpt', 'startbiz'); ?>
			</label>
		</p>
		
		<p>
	       <label for="<?php echo $this->get_field_id( 'excerpt_length' ); ?>"><?php _e( 'Excerpt Length:', 'startbiz' ); ?>
	       <input id="<?php echo $this->get_field_id( 'excerpt_length' ); ?>" name="<?php echo $this->get_field_name( 'excerpt_length' ); ?>" type="number" min="1" step="1" value="<?php echo $excerpt_length; ?>" />
	       </label>
       </p>
	   
		<?php 
	}

	/*
	|------------------------------------------------------------------------------
	| Get Recent Posts
	|------------------------------------------------------------------------------
	|
	| To display recent posts by user filter
	| 
	| @return void
	|
	*/
	public function tc_get_recent_posts( $qty, $comment_num, $date, $show_thumbnail_1, $show_excerpt, $excerpt_length ) {
		$posts = new WP_Query(
			"orderby=date&order=DESC&posts_per_page=".$qty
		);

		echo '<ul class="tc-recent-posts tc-sidebar-widget">';
		
		while ( $posts->have_posts() ) : 
		$posts->the_post(); ?>
			<li <?php if ($show_thumbnail_1 == 1) : ?>class="thumbnail-widget" <?php endif; ?>>
					<?php if ( $show_thumbnail_1 == 1 ) : ?>
						<div class="post-img">
							<a href="<?php the_permalink(); ?>">							
								<?php the_post_thumbnail('widget-thumbnail',array('title' => '')); ?>							
							</a>
						</div>
					<?php endif; ?>
					<div class="post-data">
						<a href="<?php the_permalink(); ?>" alt="<?php the_title(); ?>"><?php the_title(); ?></a>
						<div class="recent-post-meta">
							<?php if ( $date == 1 ) : ?>
								<?php the_time('F j, Y'); ?>,
							<?php endif; ?>
							<?php if ( $date == 1 && $comment_num == 1) : ?>
								
							<?php endif; ?>
							<?php if ( $comment_num == 1 ) : ?>
								<?php echo comments_number(__('No Comment','startbiz'), __('One Comment','startbiz'), '<span class="comm">%</span> '.__('Comments','startbiz'));?>
							<?php endif; ?>
						</div> <!--end .entry-meta-->
						<?php if ( $show_excerpt == 1 ) : ?>
							<p>
								<?php echo tc_excerpt($excerpt_length); ?>
							</p>
						<?php endif; ?>
					</div>
				<span class="clear"></span>
			</li>	
		<?php 
		endwhile;		
		echo '</ul>'."\r\n";
	}

}

/*
|------------------------------------------------------------------------------
| Load Widgets
|------------------------------------------------------------------------------
*/
add_action('widgets_init', 'tc_recent_posts_load_widgets');

/*
 |------------------------------------------------------------------------------
 | Register widget
 |------------------------------------------------------------------------------
 |
 | @return void
 |
 */
function tc_recent_posts_load_widgets()
{
	register_widget('tc_Recent_Posts_Widget');
}