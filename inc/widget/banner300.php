<?php

/*
|
|	Plugin Name: ThemeCountry 300x250 Banner Widget
|	Description: A widget for 300 x 250 banner (Single banner)
|	Version: 1.0
|
*/


/*
|------------------------------------------------------------------------------
| Load Widgets
|------------------------------------------------------------------------------
*/
add_action( 'widgets_init', 'tc_banner_300_widgets' );


/*
 |------------------------------------------------------------------------------
 | Register widget
 |------------------------------------------------------------------------------
 |
 | @return void
 |
 */
function tc_banner_300_widgets() {
	register_widget( 'tc_banner_300_widget' );
}

/*
|------------------------------------------------------------------------------
| Banner Widget Class
|------------------------------------------------------------------------------
*/
class tc_banner_300_widget extends WP_Widget {

	/*
	|------------------------------------------------------------------------------
	| Widget Setup
	|------------------------------------------------------------------------------
	*/
	public function tc_banner_300_Widget() {

		// Widget settings
		$widget_ops = array(
			'classname' => 'tc_banner_300_widget',
			'description' => __('A widget for 300 x 250 banner (Single banner)', 'startbiz')
		);

		// Widget control settings
		$control_ops = array(
			'width' => 300,
			'height' => 350,
			'id_base' => 'tc_banner_300_widget'
		);

		// Create the widget
		$this->WP_Widget( 'tc_banner_300_widget', __('ThemeCountry: 300x250 Banner', 'startbiz'), $widget_ops, $control_ops );
		
	}

	/*
	|------------------------------------------------------------------------------
	|	Display Widget
	|------------------------------------------------------------------------------
	*/
	public function widget( $args, $instance ) {
		extract( $args );

		// Variables from the widget settings
		$title = apply_filters('widget_title', $instance['title'] );
		$banner = $instance['banner'];
		$link = $instance['link'];
		$target = $instance['target'] ? 'target="_blank"' : '';

		// Before widget (defined by theme functions file)
		echo $before_widget;

		// Display the widget title if one was input
		if ( $title )
			echo $before_title . $title . $after_title;
			
		// Display a containing div
		echo '<div class="banner-300">';

		// Display Banner
		if ( $link )
			echo '<a href="' . $link . '" '. $target .'><img src="' . $banner . '" width="300" height="250" alt="" /></a>';
			
		elseif ( $banner )
		 	echo '<img src="' . $banner . '" width="300" height="250" alt="" />';
			
		echo '</div>';

		// After widget (defined by theme functions file)
		echo $after_widget;
	}


	/*
	|------------------------------------------------------------------------------
	| Update Widget
	|------------------------------------------------------------------------------
	*/
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		// Strip tags to remove HTML (important for text inputs)
		$instance['title'] = strip_tags( $new_instance['title'] );

		// No need to strip tags
		$instance['banner'] = $new_instance['banner'];
		$instance['link'] = $new_instance['link'];
		$instance['target'] = $new_instance['target'];

		return $instance;
	}

	/*
	|------------------------------------------------------------------------------
	| Widget Settings 
	|------------------------------------------------------------------------------
	|
	| Displays the widget settings controls on the widget panel
	|
	*/
	public function form( $instance ) {

		// Set up some default widget settings
		$defaults = array(
			'title' => '',
			'banner' => get_template_directory_uri()."/images/300x250.png",
			'link' => 'http://themecountry.com/',
			'target' => false
		);
		
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'startbiz') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" />
		</p>

		<!-- Banner image url: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'banner' ); ?>"><?php _e('Banner image url:', 'startbiz') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'banner' ); ?>" name="<?php echo $this->get_field_name( 'banner' ); ?>" value="<?php echo $instance['banner']; ?>" />
		</p>
		<!-- Target  : Checkbox -->
		<p>
			<label for="<?php echo $this->get_field_id( 'target' ); ?>"><?php _e('Open New Window:', 'startbiz') ?></label>
			<?php if ($instance['target']){ ?>
				<input type="checkbox" id="<?php echo $this->get_field_id( 'target' ); ?>" name="<?php echo $this->get_field_name( 'target' ); ?>" checked="checked" />
			<?php } else { ?>
				<input type="checkbox" id="<?php echo $this->get_field_id( 'target' ); ?>" name="<?php echo $this->get_field_name( 'target' ); ?>"  />
			<?php } ?>
		</p>
		
		<!-- Banner link url: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'link' ); ?>"><?php _e('Banner link url:', 'themecountry') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'link' ); ?>" name="<?php echo $this->get_field_name( 'link' ); ?>" value="<?php echo $instance['link']; ?>" />
		</p>
		
		<?php
		}
	}
?>