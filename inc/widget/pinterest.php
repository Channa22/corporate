<?php
/*
|
|	Plugin Name: ThemeCountry Pinterest Profile Widget Box
|	Description: A widget to display Pinterest Profile Widget Box.
|	Version: 1.0
|
*/


/*
|------------------------------------------------------------------------------
| Pinterest Profile Widget Class
|------------------------------------------------------------------------------
*/
class tc_Pinterest_Profile_Widget extends WP_Widget {
	
	/*
	|------------------------------------------------------------------------------
	| Widget Setup
	|------------------------------------------------------------------------------
	|
	| @return void
	|
	*/
	public function tc_Pinterest_Profile_Widget()
	{
		$widget_ops = array(
			'classname' => 'tc_pinterest_profile_widget', 
			'description' => __('ThemeCountry Pinterest Profile Widget Box.','startbiz')
		);

		$control_ops = array(
			'id_base' => 'tc-pinterest-profile-box'
			);

		$this->WP_Widget('tc-pinterest-profile-box', __('ThemeCountry: Pinterest Profile Widget Box','startbiz'), $widget_ops, $control_ops);
	}
	
	/*
	|------------------------------------------------------------------------------
	| Display Widget
	|------------------------------------------------------------------------------
	|
	| @return void
	|
	*/
	public function widget($args, $instance)
	{
		extract($args);

		$title = apply_filters('widget_title', $instance['title']);		
		$page_url = $instance['page_url'];
		$img_width = $instance['img_width'];
		$board_width = $instance['board_width'];
		$board_height = $instance['board_height'];
		
		echo $before_widget;

		if($title) {
			echo $before_title.$title.$after_title;
		}
		
		if($page_url): ?>	
		
		<a class="tc-pinterest-profile" data-pin-do="embedUser" href="<?php echo esc_url($page_url) ?>" data-pin-scale-width="<?php echo $img_width; ?>" data-pin-scale-height="<?php echo $board_height; ?>" data-pin-board-width="<?php echo $board_width; ?>"><?php echo $title; ?></a>
		<!-- Please call pinit.js only once per page -->

		<?php endif;
		
		echo $after_widget;
	}
	
	/*
	|------------------------------------------------------------------------------
	| Update Widget
	|------------------------------------------------------------------------------
	|
	| @return void
	|
	*/
	public function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$instance['page_url'] = $new_instance['page_url'];
		$instance['img_width'] = $new_instance['img_width'];
		$instance['board_width'] = $new_instance['board_width'];
		$instance['board_height'] = $new_instance['board_height'];
		
		return $instance;
	}

	/*
	|------------------------------------------------------------------------------
	| Widget Settings 
	|------------------------------------------------------------------------------
	|
	| Displays the widget settings controls on the widget panel
	| 
	| @return void
	|
	*/
	public function form($instance)
	{
		$defaults = array(
			'title' => __('Find us on Pinterest','startbiz'), 
			'page_url' => '', 
			'img_width' => '80', 
			'board_width' => '300', 
			'board_height' => '320'
		);
		$instance = wp_parse_args((array) $instance, $defaults); ?>
		
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title','startbiz'); ?>:</label>
			<input type="text" class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id('page_url'); ?>"><?php _e('Pinterest User URL:','startbiz'); ?>:</label>
			<input type="text" class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('page_url'); ?>" name="<?php echo $this->get_field_name('page_url'); ?>" value="<?php echo $instance['page_url']; ?>" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id('img_width'); ?>"><?php _e('Image Width','startbiz'); ?>:</label>
			<input type="text" class="widefat" style="width: 40px;" id="<?php echo $this->get_field_id('img_width'); ?>" name="<?php echo $this->get_field_name('img_width'); ?>" value="<?php echo $instance['img_width']; ?>" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id('board_width'); ?>"><?php _e('Board Width','startbiz'); ?>:</label>
			<input type="text" class="widefat" style="width: 40px;" id="<?php echo $this->get_field_id('board_width'); ?>" name="<?php echo $this->get_field_name('board_width'); ?>" value="<?php echo $instance['board_width']; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('board_height'); ?>"><?php _e('Board Height','startbiz'); ?>:</label>
			<input type="text" class="widefat" style="width: 40px;" id="<?php echo $this->get_field_id('board_height'); ?>" name="<?php echo $this->get_field_name('board_height'); ?>" value="<?php echo $instance['board_height']; ?>" />
		</p>
	<?php
	}
}

/*
|------------------------------------------------------------------------------
| Load Widgets
|------------------------------------------------------------------------------
*/
add_action('widgets_init', 'tc_pinterest_profile_load_widgets');

/*
 |------------------------------------------------------------------------------
 | Register widget
 |------------------------------------------------------------------------------
 |
 | @return void
 |
 */
function tc_pinterest_profile_load_widgets()
{
	register_widget('tc_Pinterest_Profile_Widget');
}