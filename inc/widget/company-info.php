<?php

/*
|
|	Plugin Name: Static Company Information
|	Description: A widget to display static company information
|	Version: 1.0
|
*/


/*
|------------------------------------------------------------------------------
| Load Widgets
|------------------------------------------------------------------------------
*/
add_action( 'widgets_init', 'tc_static_company_info_widgets' );


/*
 |------------------------------------------------------------------------------
 | Register widget
 |------------------------------------------------------------------------------
 |
 | @return void
 |
 */
function tc_static_company_info_widgets() {
	register_widget( 'tc_static_company_info_widget' );
}

/*
|------------------------------------------------------------------------------
| Banner Widget Class
|------------------------------------------------------------------------------
*/
class tc_static_company_info_widget extends WP_Widget {

	/*
	|------------------------------------------------------------------------------
	| Widget Setup
	|------------------------------------------------------------------------------
	*/
	public function tc_static_company_info_widget() {

		// Widget settings
		$widget_ops = array(
			'classname' => 'tc-static-company-info-widget',
			'description' => __('A widget to display static company information', 'startbiz')
		);

		// Widget control settings
		$control_ops = array(
			'id_base' => 'tc_static_company_info_widget'
		);

		// Create the widget
		$this->WP_Widget( 'tc_static_company_info_widget', __('ThemeCountry: Company Info', 'startbiz'), $widget_ops, $control_ops );
		
	}

	/*
	|------------------------------------------------------------------------------
	|	Display Widget
	|------------------------------------------------------------------------------
	*/
	public function widget( $args, $instance ) {
		extract( $args );

		// Variables from the widget settings
		$title = apply_filters('widget_title', $instance['title'] );
		$address = $instance['address'];
		$phone = $instance['phone'];
		$fax = $instance['fax'];
		$email = $instance['email'];
		$website = $instance['website'];
		$desc = $instance['desc'];

		// Before widget (defined by theme functions file)
		echo $before_widget;

		// Display the widget title if one was input
		if ( $title )
			echo $before_title . $title . $after_title;
			
		// Display a containing div
		echo '<div class="contact-infomation">';


		// Display info

		if ($desc) {
			echo '<p>' . $desc . '</p>';
		}

		echo '<div class="tc-contact-fo"><ul class="contact-info">';
			if ( $address ) {
				echo '<li><i class="fa fa-map-marker"></i> '. $address .'</li>';
			}
			if ( $phone ) {
				echo '<li><i class="fa fa-phone-square"></i> '. $phone .'</li>';
			}
			if ( $fax ) {
				echo '<li><i class="fa fa-fax"></i> '. $fax .'</li>';
			}
			if ( $email ) {
				echo '<li><i class="fa fa-envelope-o"></i> '. $email .'</li>';
			}
			if ( $website ) {
				echo '<li><i class="fa fa-globe"></i> '. $website .'</li>';
			}

		echo '</ul></div>';
			
		echo '</div>';

		// After widget (defined by theme functions file)
		echo $after_widget;
	}


	/*
	|------------------------------------------------------------------------------
	| Update Widget
	|------------------------------------------------------------------------------
	*/
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		// Strip tags to remove HTML (important for text inputs)
		$instance['title'] = strip_tags( $new_instance['title'] );

		// No need to strip tags
		$instance['address'] = $new_instance['address'];
		$instance['phone'] = $new_instance['phone'];
		$instance['fax'] = $new_instance['fax'];
		$instance['email'] = $new_instance['email'];
		$instance['website'] = $new_instance['website'];
		$instance['desc'] = $new_instance['desc'];

		return $instance;
	}

	/*
	|------------------------------------------------------------------------------
	| Widget Settings 
	|------------------------------------------------------------------------------
	|
	| Displays the widget settings controls on the widget panel
	|
	*/
	public function form( $instance ) {

		// Set up some default widget settings
		$defaults = array(
			'title' 	=> '',
			'address' 	=> '',
			'phone' 	=> '',
			'fax'		=> '',
			'email'		=> '',
			'website'	=> home_url( '/' ),
			'desc'		=> ''
		);
		
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'startbiz') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" />
		</p>

		<!-- Address -->
		<p>
			<label for="<?php echo $this->get_field_id( 'address' ); ?>"><?php _e('Address:', 'startbiz') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'address' ); ?>" name="<?php echo $this->get_field_name( 'address' ); ?>" value="<?php echo $instance['address']; ?>" />
		</p>
		<!-- Phone -->
		<p>
			<label for="<?php echo $this->get_field_id( 'phone' ); ?>"><?php _e('Phone:', 'startbiz') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'phone' ); ?>" name="<?php echo $this->get_field_name( 'phone' ); ?>" value="<?php echo $instance['phone']; ?>" />
		</p>
		
		<!-- Fax -->
		<p>
			<label for="<?php echo $this->get_field_id( 'fax' ); ?>"><?php _e('Fax:', 'startbiz') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'fax' ); ?>" name="<?php echo $this->get_field_name( 'fax' ); ?>" value="<?php echo $instance['fax']; ?>" />
		</p>
		<!-- Email -->
		<p>
			<label for="<?php echo $this->get_field_id( 'email' ); ?>"><?php _e('Email:', 'startbiz') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'email' ); ?>" name="<?php echo $this->get_field_name( 'email' ); ?>" value="<?php echo $instance['email']; ?>" />
		</p>
		<!-- Website -->
		<p>
			<label for="<?php echo $this->get_field_id( 'website' ); ?>"><?php _e('Website:', 'startbiz') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'website' ); ?>" name="<?php echo $this->get_field_name( 'website' ); ?>" value="<?php echo $instance['website']; ?>" />
		</p>
		<!-- Description -->
		<p>
			<label for="<?php echo $this->get_field_id( 'desc' ); ?>"><?php _e('Description:', 'startbiz') ?></label>
			<textarea rows="3" id="<?php echo $this->get_field_id( 'desc' ); ?>" name="<?php echo $this->get_field_name( 'desc' ); ?>"><?php echo $instance['desc']; ?></textarea>
		</p>
		<?php
		}
	}
?>