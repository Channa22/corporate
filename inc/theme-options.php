<?php
/**
 * Initialize the custom Theme Options.
 */
add_action( 'admin_init', 'custom_theme_options' );

/**
* Include all custom theme options
*/
require get_template_directory() . '/inc/options/custom-options.php';

/**
 * Build the custom settings & update OptionTree.
 *
 * @return    void
 * @since     2.3.0
 */
function custom_theme_options() {
  
  /* OptionTree is not loaded yet */
  if ( ! function_exists( 'ot_settings_id' ) )
    return false;
  
  /**
   * Get a copy of the saved settings array. 
   */
  $saved_settings = get_option( ot_settings_id(), array() );
  
  /**
   * Custom settings array that will eventually be 
   * passes to the OptionTree Settings API Class.
   */
  $custom_settings = array( 
        'contextual_help' => array( 
          'content'       => array( 
            array(
              'id'        => 'option_types_help',
              'title'     => __( 'Option Types', 'startbiz' ),
              'content'   => '<p>' . __( 'Help content goes here!', 'startbiz' ) . '</p>'
            )
          ),
          'sidebar'       => '<p>' . __( 'Sidebar content goes here!', 'startbiz' ) . '</p>'
        ),
        'sections'        => array( 
            array(
                'id'          => 'option_general',
                'title'       => __( 'General', 'startbiz' )
            ),
            array(
                'id'          => 'option_banner',
                'title'       => __( 'Home Banner', 'startbiz' )
            ),
            array(
                'id'          => 'option_home',
                'title'       => __( 'Home', 'startbiz' )
            ),
            array(
                'id'          => 'option_contact',
                'title'       => __( 'Contact', 'startbiz' )
            ),
            array(
                'id'          => 'option_style',
                'title'       => __( 'Styles', 'startbiz' )
            ),
            array(
                'id'          => 'option_header',
                'title'       => __( 'Header', 'startbiz' )
            ),
            array(
                'id'          => 'option_footer',
                'title'       => __( 'Footer', 'startbiz' )
            ),
            array(
                'id'          => 'option_blog',
                'title'       => __( 'Blog', 'startbiz' )
            ),
            array(
                'id'          => 'option_single',
                'title'       => __( 'Single Post', 'startbiz' )
            ),
            array(
                'id'          => 'option_sidebar',
                'title'       => __( 'Sidebar', 'startbiz' )
            ),
            array(
                'id'          => 'option_social_button',
                'title'       => __( 'Social Buttons', 'startbiz' )
            ),
             array(
                'id'          => 'option_social_link',
                'title'       => __( 'Social Footer', 'startbiz' )
            ),
            array(
                'id'          => 'option_typography',
                'title'       => __( 'Typography', 'startbiz' )
            ),
            array(
                'id'      => 'option_ad_mangment',
                'title'   => __( 'Ad Managment', 'startbiz' )
            ),
            array(
                'id'          => 'option_import_export',
                'title'       => __( 'Import/Export', 'startbiz') 
            ),
        ),

        'settings'        => array_merge( 
                   
            /* General */
            tc_general_theme_option(),

            /* Feature Banner */
            tc_feature_banner_option(),

            /* Home */
            tc_home_theme_option(),

            /* Contact */
            tc_contact_theme_option(),

            /* Style */
            tc_style_theme_option(),
                    
            /* Header */        
            tc_header_theme_option(),

            /* Footer */
            tc_footer_theme_option(),

            /* Blog */
            tc_blog_theme_option(),
                    
            /* Single */
            tc_single_theme_option(),

            /* Social Buttons */
            tc_social_buttons_theme_options(),

            /* Social Links */
            tc_social_links_theme_options(),

            /* Sidebar */
            tc_sidebar_theme_option(),
                
            /* Typography */
            tc_typography_theme_option(),

            /* Ad Managment */
          tc_ads_theme_option(),
            
            /* Import & Export */
            tc_import_export_theme_option()
        )     
    );
  
  /* allow settings to be filtered before saving */
  $custom_settings = apply_filters( ot_settings_id() . '_args', $custom_settings );
  
  /* settings are not the same update the DB */
  if ( $saved_settings !== $custom_settings ) {
    update_option( ot_settings_id(), $custom_settings ); 
  }
  
  /* Lets OptionTree know the UI Builder is being overridden */
  global $ot_has_custom_theme_options;
  $ot_has_custom_theme_options = true;
  
}