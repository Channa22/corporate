<?php
	function tc_feature_banner_option() {
		return array(
			//Enable, Disable and Edite Banner Slide			
            array(
                'id'        =>    'first_banner',
                'label'     =>    __( '1st Section - Business Introduction', 'themecountry' ),
                'desc'      =>    __( 'Enable or Disable', 'themecountry' ),
                'std'       =>    'on',
                'type'      =>   'on-off',
                'section'   =>    'option_banner'
            ),
            array(
                'id'        =>    'home_first_banner_title',
                'label'     =>    '',
                'desc'      =>    __( 'Title', 'themecountry' ),
                'std'       =>    'This is an Awesome Feathure Banner',
                'class'     =>    'first-section',
                'type'      =>    'text',
                'section'   =>    'option_banner',
            ),
            array(
                'id'        =>    'home_first_banner_short_desc',
                'label'     =>    '',
                'desc'      =>    __( 'Description', 'themecountry' ),
                'std'       =>    'Corpprate is the Most Simple Way to Launch Your Startup. Perfect Landing Page for Startup. Everything Your Bussiness Need in One Place.',
                'type'      =>    'textarea-simple',
                'class'     =>    'first-section',
                'section'   =>    'option_banner',
                'rows'      =>    '3'
            ),
            array(
                'id'        =>    'home_first_banner_img',
                'label'     =>    '',
                'desc'      =>    __( 'Background Image<br/>(Recommend: width: 1600px, height: 800px)', 'themecountry' ),
                'type'      =>    'upload',
                'class'     =>    'first-section',
                'section'   =>    'option_banner'
            ),
            array(
                'id'        =>    'home_first_banner_img_mobile',
                'label'     =>    '',
                'desc'      =>    __( 'Background Image for mobile display<br/>(Recommend width: 640px, height: 420px)', 'themecountry' ),
                'type'      =>    'upload',
                'class'     =>    'first-section',
                'section'   =>    'option_banner'
            ),
            array(
                'id'        =>    'home_first_banner_color',
                'label'     =>    'Option Color',
                'desc'      =>    __( 'Title', 'themecountry' ),
                'type'      =>    'colorpicker',
                'std'       =>    '#fff',
                'class'     =>    'first-section',
                'section'   =>    'option_banner'           
            ),
            array(
                'id'        =>    'home_first_desc_color_banner',
                'label'     =>    '',
                'desc'      =>    __( 'Description','themecountry'),
                'type'      =>    'colorpicker',
                'std'       =>    '#fff',
                'class'     =>    'first-section',
                'section'   =>    'option_banner'
            ),
        );

	}
