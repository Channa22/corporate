<?php
function tc_single_theme_option() {
	return array (
        array(
            'id'        => 'author-bio',
            'label'     => __( 'Author Bio', 'themecountry' ),
            'desc'      => __( 'Enable or Disable', 'themecountry' ),
            'std'       => 'on',
            'type'      => 'on-off',
            'section'   => 'option_single'
        ),
        array(
            'id'        => 'author-highlight',
            'label'     => __( 'Highlight Author Comment', 'themecountry' ),
            'desc'      => __( 'Enable or Disable', 'themecountry' ),
            'std'       => 'on',
            'type'      => 'on-off',
            'section'   => 'option_single'
    	),
        array(
            'id'        => 'enable-post-meta-info',
            'label'     => __( 'Enable Post Meta Info', 'themecountry' ),
            'desc'      => __( 'Enable or Disable Meta Info in Single Pages<br/>(Author name, Date etc.)', 'themecountry' ),
            'std'       => 'on',
            'type'      => 'on-off',
            'section'   => 'option_single'
        ),
        array(
            'id'          => 'post-meta-info',
            'label'       => __( 'What To Hide in Meta Info', 'themecountry' ),
            'desc'        => __( 'Choose What Meta Info to Hide.', 'themecountry' ),
            'type'        => 'checkbox',
            'section'     => 'option_single',        
            'choices'     => array( 
                array(
                	'value'       => 'autho_name',
                	'label'       => __( 'Author Name', 'themecountry' ),
                ),
                array(
                	'value'       => 'date',
                	'label'       => __( 'Date', 'themecountry' ),
                ),
                array(
                	'value'       => 'category',
                	'label'       => __( 'Category', 'themecountry' ),
                ),
                array(
                	'value'       => 'tag',
                	'label'       => __( 'Tag', 'themecountry' ),
                ),
                array(
                	'value'       => 'comment_count',
                	'label'       => __( 'Comment Count', 'themecountry' ),
                )
            )
        ),
        array(
            'id'        => 'breadcrumb',
            'label'     => __( 'Breadcrumbs', 'themecountry' ),
            'desc'      => __( 'Enable or Disable', 'themecountry' ),
            'std'       => 'off',
            'type'      => 'on-off',
            'section'   => 'option_single'
        ),
        array(
            'id'        => 'yoast_breadcrumb',
            'desc'      => __( 'Check it if you install WordPress SEO by Yoast', 'themecountry' ),
            'type'      => 'checkbox',
            'section'   => 'option_single',
            'choices'   => array( 
	        	array(
	            	'value'       =>	'yoast_breadcrumb_yes',
	            	'label'       =>	__( 'Use Yoast Breadcrumb', 'option-tree-theme' ),
	        	)
	        )
        ),
        array(
            'id'        => 'post-nav',
            'label'     => __( 'Post Navigation', 'themecountry' ),
            'desc'      => __( 'Shows Links Next and Previous Posts', 'themecountry' ),
            'std'       => 'on',
            'type'      => 'on-off',
            'section'   => 'option_single'
        ),
        array(
            'id'        => 'post-related',
            'label'     => __( 'Related Posts', 'themecountry' ),
            'desc'      => __( 'Use This Option to Enable or Disable Related Posts Below Your Post.', 'themecountry' ),
            'std'       => 'on',
            'type'      => 'on-off',
            'section'   => 'option_single'
        ),
        array(
            'id'        => 'related-posts-taxonomy',
            'label'     => __( 'Related Posts Taxonomy', 'themecountry' ),
            'desc'      => __( 'Choose the Way Related Post Work.', 'themecountry' ),
            'std'       => 'tag',
            'type'      => 'radio',
            'section'   => 'option_single',
            'choices'   => array(
                array( 
                    'value' => 'tag',
                    'label' => 'Tags'
                ),
                array( 
                    'value' => 'category',
                    'label' => 'Categories'
                )
            )
        ),
        array(
            'id'        => 'number-related',
            'std'       => '3',
            'label'     => __( 'Number Related Posts', 'themecountry' ),
            'desc'      => __( 'Enter The Number of Posts to Show in the Related Posts.', 'themecountry' ),
            'type'      => 'text',
            'section'   => 'option_single'
        ),
        array(
            'id'        => 'display-related-posts',
            'label'     => __( 'Display Related Posts', 'themecountry' ),
            'desc'      => __('Related Posts type to display.' ),
            'std'       => 'thumbnail',
            'type'      => 'radio',
            'section'   => 'option_single',
            'choices'   => array(
                array( 
                    'value' => 'thumbnail',
                    'label' =>  __('Thumbnail and Title', 'themecountry')
                ),
                array( 
                    'value' => 'list',
                    'label' => __('Only Tittle, In List style.', 'themecountry')
                )
            )
        ),
	);
}