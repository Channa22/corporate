<?php
function tc_social_links_theme_options () {
	return array (
		array(
	            'id'        => 'social_link_enable',
	            'label'     => __( 'Enable Social Links Footer', 'themecountry' ),
	            'desc'      => __( 'Use this option to show social links.', 'themecountry' ),
	            'std'       => 'on',
	            'type'      => 'on-off',
	            'section'   => 'option_social_link'
	        ),
			array(
                'id'          => 'social_links',
                'label'        => __( 'Add Social Links', 'themecountry' ),
                'std'         => '',
                'type'        => 'list-item',
                'section'     => 'option_social_link',
                'settings'	=> array(
				array(
					'id'		=> 'social-icon',
					'label'		=> 'Icon Name',
					'desc'		=> 'Font Awesome icon names [<a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank"><strong>View all</strong>]</a>  ',
					'type'		=> 'select',
					'choices'     => array( 
						array(
							'value'       => '',
							'label'       => __( '-- Choose One --', 'themecountry' ),
						),
						array(
							'value'       => 'fa fa-facebook',
							'label'       => __( 'Facebook', 'themecountry' ),
						),
						array(
							'value'       => 'fa fa-twitter',
							'label'       => __( 'Twitter', 'themecountry' ),
						),
						array(
							'value'       => 'fa fa-google-plus',
							'label'       => __( 'Google+', 'themecountry' ),
						),
						array(
							'value'       => 'fa fa-linkedin',
							'label'       => __( 'Linkedin', 'themecountry' ),
						),
						 array(
							'value'       => 'fa fa-pinterest-p',
							'label'       => __( 'Pinterest', 'themecountry' ),
						),
						array(
							'value'       => 'fa fa-youtube',
							'label'       => __( 'Youtube', 'themecountry' ),
						),
						array(
							'value'       => 'fa fa-github',
							'label'       => __( 'Github', 'themecountry' ),
						),
						array(
							'value'       => 'fa fa-dribbble',
							'label'       => __( 'Dribbble', 'themecountry' ),
						),
						array(
							'value'       => 'fa fa-reddit',
							'label'       => __( 'Reddit', 'themecountry' ),
						),
						array(
							'value'       => 'fa fa-vimeo-square',
							'label'       => __( 'Vimeo', 'themecountry' ),
						),
						array(
							'value'       => 'fa fa-wordpress',
							'label'       => __( 'WordPress', 'themecountry' ),
						),
						 array(
							'value'       => 'fa fa-tumblr',
							'label'       => __( 'Tumblr', 'themecountry' ),
						),
						array(
							'value'       => 'fa fa-vine',
							'label'       => __( 'Vine', 'themecountry' ),
						),
						array(
							'value'       => 'fa fa-slideshare',
							'label'       => __( 'Slideshare', 'themecountry' ),
						)
			        )
				),
				array(
					'id'		=> 'social-link',
					'label'		=> 'Link',
					'desc'		=> 'Enter the full url for your social link',
					'std'		=> 'http://',
					'type'		=> 'text',
				),
				array(
					'id'		=> 'social-color',
					'label'		=> 'Icon Color',
					'desc'		=> 'Set a unique color for your icon (optional)',
					'std'		=> '#c1c1c1',
					'type'		=> 'colorpicker',
				),
				array(
					'id'		=> 'social-target',
					'label'		=> 'Link Options',
					'desc'		=> '',
					'std'		=> '',
					'type'		=> 'checkbox',
					'choices'	=> array(
						array( 
							'value' => '_blank',
							'label' => 'Open in new window'
						)
					)
				)
			)
        ),
	);
}