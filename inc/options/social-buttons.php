<?php
function tc_social_buttons_theme_options () {
	return array (
		array(
            'id'        => 'social-button-enable',
            'label'     => __( 'Social Sharing Button', 'themecountry' ),
            'desc'      => __( 'Use This Option to Show Social Sharing Button.', 'themecountry' ),
            'std'       => 'on',
            'type'      => 'on-off',
            'section'   => 'option_social_button'
        ),
        array(
            'id'        => 'twitter-username',
            'label'     => __( 'Twitter Username', 'themecountry' ),
            'desc'      => __( 'Your@username will be added to share-tweets of your posts (optional)', 'themecountry' ),
            'type'      => 'text',
            'section'   => 'option_social_button'
        ),
        array(
            'id'        => 'social-sharing-pos',
            'label'     => __( 'Social Sharing Buttons Position', 'themecountry' ),
            'desc'      => __( 'Choose Position of Social Sharing Buttons.', 'themecountry' ),
            'type'      => 'checkbox',
            'section'   => 'option_social_button',
            'std'       => 'top',
            'choices'   => array(
                array(
                    'value'       => 'top',
                    'label'       => __( 'After Title', 'themecountry' ),
                ),
                array(
                    'value'       => 'bottom',
                    'label'       => __( 'After Post', 'themecountry' ),
                ),
                array(
                    'value'       => 'float',
                    'label'       => __( 'Floating Social To Left', 'themecountry' ),
                ),
            )
        ),
        array(
            'id'        => 'social-facebook',
            'label'     => __( 'Facebook', 'themecountry' ),
            'std'       => 'on',
            'type'      => 'on-off',
            'section'   => 'option_social_button'
        ),
        array(
            'id'        => 'social-twitter',
            'label'     => __( 'Twitter', 'themecountry' ),
            'std'       => 'on',
            'type'      => 'on-off',
            'section'   => 'option_social_button'
        ),
        array(
            'id'        => 'social-google-plus',
            'label'     => __( 'Google Plus', 'themecountry' ),
            'std'       => 'on',
            'type'      => 'on-off',
            'section'   => 'option_social_button'
        ),
        array(
            'id'        => 'social-linkedin',
            'label'     => __( 'Linkedin', 'themecountry' ),
            'std'       => 'off',
            'type'      => 'on-off',
            'section'   => 'option_social_button'
        ),
        array(
            'id'        => 'social-pinterest',
            'label'     => __( 'Pinterest', 'themecountry' ),
            'std'       => 'off',
            'type'      => 'on-off',
            'section'   => 'option_social_button'
        ),
	);
}