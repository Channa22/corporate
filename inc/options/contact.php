<?php

function tc_contact_theme_option () {

	return array(

		array(
	        'id'        => 'contact_address',
	        'label'     => __( 'Contact Information', 'themecountry' ),
	        'desc'      => __( 'Address', 'themecountry' ),
	        'type'      => 'text',
	        'section'   => 'option_contact'
	    ),
	    array(
	        'id'        => 'contact_phone',
	        'label'     =>  '',
	        'desc'      => __( 'Phone', 'themecountry' ),
	        'type'      => 'text',
	        'section'   => 'option_contact'
	    ),
	    array(
	        'id'        => 'contact_fax',
	        'label'     => '',
	        'desc'      => __( 'Fax', 'themecountry' ),
	        'type'      => 'text',
	        'section'   => 'option_contact'
	    ),
	    array(
	        'id'        => 'contact_email',
	        'label'     => '',
	        'desc'      => __( 'Email', 'themecountry' ),
	        'type'      => 'text',
	        'section'   => 'option_contact'
	    ),
	    array(
	        'id'        => 'contact_website',
	        'label'     => '',
	        'desc'      => __( 'Website', 'themecountry' ),
	        'type'      => 'text',
	        'section'   => 'option_contact'
	    ),

	    // Show Hide Map
        array(
            'id'        => 'show_hide_map',
            'label'     => __( 'Show Hide Google Map', 'themecountry' ),
            'desc'      => __( 'Disable or Enable Section Map<br/>(Google Map Latitude and Longitude)', 'themecountry' ),
            'std'       => 'on',
            'type'      => 'on-off',
            'section'   => 'option_contact'
        ),		   
	    array(
	        'id'        => 'contact_lat',
	        'label'     => __( 'Google Map Latitude and Longitude', 'themecountry' ),
	        'desc'      => __( 'Latitude', 'themecountry' ),
	        'type'      => 'text',
	        'std'		=> '11.5350757',
	        'section'   => 'option_contact'
	    ),
	     array(
	        'id'        => 'contact_lng',
	        'std'     => '104.9121448',
	        'desc'      => __( 'Longitude', 'themecountry' ),
	        'type'      => 'text',
	        'section'   => 'option_contact'
	    ),
	    array(
            'id'          => 'contact_zoom_level',
            'desc'       => __( 'Zoom level', 'themecountry' ),
            'std'         => 16,
            'type'          => 'numeric-slider',
            'min_max_step'  => '1,18,1',
            'section'     => 'option_contact'
        ),
	);
}



