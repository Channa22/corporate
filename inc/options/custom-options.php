<?php
require get_template_directory() . '/inc/options/general.php';
require get_template_directory() . '/inc/options/home.php';
require get_template_directory() . '/inc/options/header.php';
require get_template_directory() . '/inc/options/style.php';
require get_template_directory() . '/inc/options/footer.php';
require get_template_directory() . '/inc/options/blog.php';
require get_template_directory() . '/inc/options/single.php';
require get_template_directory() . '/inc/options/sidebar.php';
require get_template_directory() . '/inc/options/ads.php';
require get_template_directory() . '/inc/options/typography.php';
require get_template_directory() . '/inc/options/import-export.php';
require get_template_directory() . '/inc/options/social-buttons.php';
require get_template_directory() . '/inc/options/social-links.php';
require get_template_directory() . '/inc/options/contact.php';
require get_template_directory() . '/inc/options/feature_banner.php';
