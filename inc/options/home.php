<?php
function tc_home_theme_option() {
    return array (          
        //2nd Section - Business Information
        array(
            'id'        => 'featured_services',
            'label'     => __( '2nd Section - Company Projects', 'themecountry' ),
            'desc'      => __( 'Enable or Disable', 'themecountry' ),
            'std'       => 'on',
            'type'      => 'on-off',
            'section'   => 'option_home'
        ),
        array(
            'id'        => 'featured_services_section_title',
            'label'     => '',
            'desc'      => __( 'Title', 'themecountry' ),
            'std'       => 'Projects',
            'class'     => 'home-second-section',
            'type'      => 'text',
            'section'   => 'option_home'
        ),
        array(
            'id'        => 'home_modules_section_short_desc',
            'label'     => '',
            'desc'      => __( 'Description', 'themecountry' ),
            'class'     => 'home-second-section',
            'type'      => 'textarea-simple',
            'section'   => 'option_home',
            'rows'      => '3'
        ),
        array(
            'id'          => 'feather_projects',
            'label'       =>  '',
            'desc'        => __( 'Featured Projects<br/>(Recomend: 7 Items)', 'themecountry' ),
            'type'        => 'list-item',
            'section'     => 'option_home',
            'settings'  => array(
                array(
                    'id'        => 'feather_projects_url',
                    'label'     => '',
                    'desc'      => __( 'Choose a Category to Show', 'themecountry' ),
                    'std'   =>  'http://',
                    'type'  =>  'text',
                ),
                array(
                    'id'        => 'feather_projects_img',
                    'label'     => '',
                    'desc'      => __( 'Upload your custom logo image here.<br/>Recommended<br/>(width: 260px, height: 70px)', 'themecountry' ),
                    'type'      => 'upload',
                ),
            )
        ),

        //3rd Section - Featured Apps
        array(
            'id'        => 'featured_apps',
            'label'     => __( '3rd Section - Featured Apps', 'themecountry'),
            'desc'      => __( 'Enable or Disable', 'themecountry'),
            'std'       => 'on',
            'type'      => 'on-off',
            'section'   => 'option_home'
        ),
        array(
            'id'        => 'featured_apps_section_bg',
            'desc'      => 'Background Color',
            'type'      => 'colorpicker',
            'sid'       => '#42a5f6',
            'section'   => 'option_home'
        ),
        array(
            'id'        => 'featured_apps_title',
            'desc'      =>  __( 'Title', 'themecountry' ),
            'std'       => 'What We Provide to Our Customer.',
            'class'     => 'featured-apps',
            'type'      => 'text',
            'section'   => 'option_home'
        ),           
        array(
            'id'        => 'featured_apps_description',
            'label'     => '',
            'desc'      => __( 'Descrition', 'themecountry' ),
            'std'       => 'Get Our Apps Free From Play Store & AppStore!',
            'class'     => 'featured-apps',
            'type'      => 'textarea-simple',
            'section'   => 'option_home',
            'rows'      => '3'
        ),
        array(
            'id'        =>   'img_btn_download_app_list',
            'label'     =>   '',
            'desc'      =>   __( 'Add Button`s Images<br/>(Link to Download Or View Apps)','themecountry'),
            'std'       =>   '',
            'type'      =>   'list-item',
            'section'   =>      'option_home',
            'settings'  =>     array(
                array(
                    'id'    => 'img_button_download',
                    'label' =>  __( 'Image Button','themecountry'),
                    'std'   =>  '',
                    'type'  =>  'upload',  
                ),
                array(
                    'id'    =>  'url_img_button_download',
                    'label' =>  __( 'Url Link of Image Button','themecountry'),
                    'std'   =>  'http://',
                    'type'  =>  'text'
                ),
            )
        ),

        //4th Section - Our Services
        array(
            'id'        => 'featured_works',
            'label'     => __( '4th Section - Our Services', 'themecountry' ),
            'desc'      => __( 'Enable or Disable', 'themecountry' ),
            'std'       => 'on',
            'type'      => 'on-off',
            'section'   => 'option_home'
        ),
        array(
            'id'        => 'featured_works_title',
            'label'     => '',
            'desc'      => __( 'Title', 'themecountry' ),
            'std'       => 'Services',
            'class'     => 'home-second-section',
            'type'      => 'text',
            'section'   => 'option_home',
        ),
        array(
            'id'        => 'home_featured_works_short_desc',
            'label'     => '',
            'desc'      => __( 'Description', 'themecountry' ),
            'std'       => 'I Know What Are You Waithing for, Everything That Matters to Your Success.',
            'class'     => 'home-second-section',
            'type'      => 'textarea-simple',
            'section'   => 'option_home',
            'rows'      => '3'
        ),
        array(
        'id'          => 'feature_action_4_col_list',
        'label'       =>  '',
        'desc'        => __( 'Featured Works', 'themecountry' ),
        'std'         => '',
        'type'        => 'list-item',
        'section'     => 'option_home',
        'settings'    => array(
            array (
                'id'        => 'feature_action_awesome_font_icons',
                'label'     => 'Icon Name',
                'desc'      => 'More Font Awesome [<a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank"><strong>View all</strong>]</a>  ',
                'type'      => 'select',
                'choices'     => array ( 
                    array(
                        'value'       => '',
                        'label'       => __( '-- Choose One --', 'themecountry' ),
                    ),
                    array(
                        'value'       => 'fa fa-cogs',
                        'label'       => __( 'fa-cogs', 'themecountry' ),
                    ),
                    array(
                        'value'       => 'fa fa-check',
                        'label'       => __( 'fa-check', 'themecountry' ),
                    ),
                    array(
                        'value'       => 'fa fa-user',
                        'label'       => __( 'fa-user', 'themecountry' ),
                    ),
                    array(
                        'value'       => 'fa fa-flag',
                        'label'       => __( 'fa-flag', 'themecountry' ),
                    ),
                    array(
                        'value'       => 'fa fa-desktop',
                        'label'       => __( 'fa-desktop', 'themecountry' ),
                    ),
                    array(
                        'value'       => 'fa fa-question-circle',
                        'label'       => __( 'fa-question-circle', 'themecountry' ),
                    ),
                    array(
                        'value'       => 'fa fa-wrench',
                        'label'       => __( 'fa-wrench', 'themecountry' ),
                    ),
                    array(
                        'value'       => 'fa fa-video-camera',
                        'label'       => __( 'fa-video-camera', 'themecountry' ),
                    ),
                    array(
                        'value'       => 'fa fa-shopping-basket',
                        'label'       => __( 'fa-shopping-basket', 'themecountry' ),
                    ),
                    array(
                        'value'       => 'fa fa-key',
                        'label'       => __( 'fa-key', 'themecountry' ),
                    ),
                    array(
                        'value'       => 'fa fa-heart',
                        'label'       => __( 'fa-heart', 'themecountry' ),
                    ),
                    array(
                        'value'       => 'fa fa-users',
                        'label'       => __( 'fa-users', 'themecountry' ),
                    ),
                    array(
                        'value'       => 'fa fa-gavel',
                        'label'       => __( 'fa-gavel', 'themecountry' ),
                    ),
                    array(
                        'value'       => 'fa fa-envelope-o',
                        'label'       => __( 'fa-envelope-o', 'themecountry' ),
                    ),
                    array(
                        'value'       => 'fa fa-diamond',
                        'label'       => __( 'fa-diamond', 'themecountry' ),
                    ),
                    array(
                        'value'       => 'fa fa-university',
                        'label'       => __( 'fa-university', 'themecountry' ),
                    ),
                    array(
                        'value'       => 'fa fa-truck',
                        'label'       => __( 'fa-truck', 'themecountry' ),
                    ),
                    array(
                        'value'       => 'fa fa-anchor',
                        'label'       => __( 'fa-anchor', 'themecountry' ),
                    ),
                    array(
                        'value'       => 'fa fa-percent',
                        'label'       => __( 'fa-percent', 'themecountry' ),
                    ),
                    array(
                        'value'       => 'fa fa-shopping-basket',
                        'label'       => __( 'fa-shopping-basket', 'themecountry' ),
                    ),
                    array(
                        'value'       => 'fa fa-folder',
                        'label'       => __( 'fa-folder', 'themecountry' ),
                    )
                )
            ),
            array(
                'id'          => 'block_link',
                'label'       => __( 'Link URL', 'themecountry' ),
                'std'         => '',
                'type'        => 'text',
            ),
            array(
                'id'        => 'block_short_desc',
                'label'     => __( 'Description', 'themecountry' ),
                'type'      => 'textarea-simple',
                'rows'      => '3'
            ),
        )
    ),

    //5th Section - Blog Section
    array(
        'id'        => 'Our_blog_section',
        'label'     => __( '5th Section - Blog Section', 'themecountry' ),
        'desc'      => __( 'Enable this Section', 'themecountry' ),
        'std'       => 'on',
        'type'      => 'on-off',
        'section'   => 'option_home'
    ),
    array(
        'id'            => 'home_blog_show_limit',
        'desc'          => __( 'Number of Items to Show', 'themecountry' ),
        'std'           => '12',
        'type'          => 'numeric-slider',
        'min_max_step'  => '2,24,1',
        'section'       => 'option_home',
    ), 
    array(
        'id'        =>      'title_btn_view_more_post',
        'desc'      =>      __( 'Text View More','themecountry'),
        'std'       =>      'View More',
        'type'      =>      'text',
        'section'   =>      'option_home'      
    ),
    array( 
        'id'        =>      'url_view_more_post',
        'desc'      =>      __( 'Url View More'),
        'std'       =>      'http://',
        'type'      =>      'text',
        'section'   =>      'option_home'
    ),          

    //6th Section - Team Member
    array(
        'id'        =>      'section_team_member',
        'label'     =>      __( '6th Section - Team Member','themecountry'),
        'desc'      =>      __( 'Disable or Enable','themecountry'),
        'std'       =>      'on',
        'type'      =>      'on-off',
        'section'   =>      'option_home'
    ),
    array(
        'id'        =>      'title_team_member',
        'label'     =>      '',
        'desc'      =>      __( 'Title of Section Member '),
        'std'       =>      'Meet Our Team',
        'type'      =>      'text',
        'section'   =>      'option_home'
    ),
    array(
        'id'        =>      'desc_team_member',
        'desc'      =>      __( 'Descrition','themecountry'),
        'std'       =>      'Discover More About Our Team',
        'type'      =>      'textarea-simple',
        'section'   =>      'option_home',
        'rows'      =>      '3'
    ),
    array(
        'id'        =>      'enable_mem_social_share',
        'desc'      =>      __( 'Disable or Enable Member Social','themecountry'),
        'std'       =>      'on',
        'type'      =>      'on-off',
        'section'   =>      'option_home'
    ),
    array(
        'id'        =>       'add_member_list',
        'label'     =>      '',
        'desc'      =>      __( 'Create Your Profile','themecountry'),
        'type'      =>      'list-item',
        'section'   =>      'option_home',
        'settings'  =>      array( 
            array(
                'id'        =>      'list_url_member_detail',
                'desc'      =>      'Link Url',
                'std'       =>      'http://',
                'type'      =>      'text',
                'section'   =>      'option_home'
            ),
            array(
                'id'        =>      'img_profile_member',
                'desc'      =>      __( 'Image Profile<br/>Size Recommend<br/>(width: 175px, height: 175px)','themecountry'),
                'std'       =>      '',
                'type'      =>      'upload',
                'section'   =>      'option_home'
            ),
            array(
                'id'        =>      'member_description',
                'desc'      =>      'Description About Team',
                'type'      =>      'textarea-simple',
                'rows'      =>      '3',
                'section'   =>      'option_home'
            ),
            array(
                'id'        =>      'link_acc_fb',
                'desc'      =>      __( 'Faccebook'),
                'type'      =>      'text',
                'std'       =>      'http://',
            ),
            array(
                'id'        =>      'link_acc_tw',
                'desc'      =>      __( 'Twitter'),
                'type'      =>      'text',
                'std'       =>      'http://',
            ),
            array(
                'id'        =>      'link_acc_youtube',
                'desc'      =>      __( 'Youtube'),
                'type'      =>      'text',
                'std'       =>      'http://',
            ),
            array(
                'id'        =>      'link_acc_mail',
                'desc'      =>      __( 'Mail'),
                'type'      =>      'text',
                'std'       =>      'http://',
            ),
        )
    ),

    //7th Section - Sponsor Section
    array(
        'id'        =>  'enable_sponsor',
        'label'     =>  __( '7th Section - Sponsor Section', 'themecountry'),
        'desc'      =>  __( 'Disable or Enable', 'themecountry'),
        'std'       =>  'on',
        'type'      =>  'on-off',
        'section'   =>  'option_home'
    ),
    array(
        'id'        =>      'sponsor_logo_list',
        'desc'      =>      'Add Sponsor Logos<br/>Size Recommend (width 124px)',
        'std'       =>      '',
        'type'      =>      'list-item',
        'section'   =>      'option_home',
        'settings'  =>       array(
            array(
                'id'        =>      'sponsor_link',
                'desc'      =>      __( 'Link url', 'themecountry'),
                'type'      =>      'text',
                'std'       =>      'http://',
            ),
            array(
                'id'    =>      'sponsor_img',
                'label' =>      __( 'Upload logo sponsor', 'themecountry'),
                'std'   =>      '',
                'type'  =>      'upload',
                ),
            )
        ),
    );
}