<?php
function tc_typography_theme_option() {
	
	return array (
		array(
            'id'        => 'tc-google-typography',
            'label'     => '',
            'desc'      => '',
            'type'      => 'typography-google',
            'section'   => 'option_typography'
        ),
	);
}

/**
|--------------------------------------------------------------------
| Default Font Settings
|--------------------------------------------------------------------
*/

if(function_exists('register_typography')) { 
    register_typography(array(
        'Logo Font' => array(
            'preview_text' => 'Logo',
            'preview_color' => 'light',
            'font_family' => 'Open Sans',
            'font_variant' => '700',
            'font_size' => '32px',
            'font_color' => '#000',        
            'css_selectors' => '.site-header .site-branding a, .site-title a',
        ),
        'primary_navigation_font' => array(
            'preview_text' => 'Primary Navigation',
            'preview_color' => 'light',
            'font_family' => 'Open Sans',
            'font_variant' => '600',
            'font_size' => '14px',
            'css_selectors' => '.main-navigation .navbar-nav li a'
        ),
        'home_title_font' => array(
            'preview_text' => 'Home Section Title',
            'preview_color' => 'light',
            'font_family' => 'Open Sans',
            'font_variant' => '700',
            'css_selectors' => '.tc-title, .features-title, .portfolio-title, .grid-2-data-inner h3, .our-blog-section .head-section h2'
        ),
        'home_title_font' => array(
            'preview_text' => 'Blog Post Title',
            'preview_color' => 'light',
            'font_family' => 'Open Sans',
            'font_size' => '30px',
            'font_variant' => '700',
            'font_color' => '#333333',
            'css_selectors' => '.entry-title a'
        ),
        'single_title_font' => array(
            'preview_text' => 'Single Post Title',
            'preview_color' => 'light',
            'font_family' => 'Open Sans',
            'font_size' => '30px',
            'font_variant' => '700',
            'font_color' => '#333333',
            'css_selectors' => '.single-post .entry-title'
        ),
        'content_font' => array(
            'preview_text' => 'Body Text',
            'preview_color' => 'light',
            'font_family' => 'Open Sans',
            'font_size' => '16px',
            'font_variant' => 'normal',
            'css_selectors' => 'body, .f-widget ul li a .site-footer .textwidget'
        ),
        'sidebar_title_font' => array(
            'preview_text' => 'Sidebar Title',
            'preview_color' => 'light',
            'font_family' => 'Open Sans',
            'font_variant' => '700',
            'font_size' => '18px',
            'css_selectors' => '.widget-title, .site-footer-widget .widget-title, .widget h2',
            'additional_css' => 'text-transform: uppercase;',
        ),
        'footer_font' => array(
            'preview_text' => 'h1, h2, h3',
            'preview_color' => 'light',
            'font_family' => 'Open Sans',
            'font_size' => '14px',
            'font_color' => '#ffffff',
            'css_selectors' => '.copyright p, .legal a, .menu-footer ul#menu-footer-menu li a',
            'additional_css' => '',
        )
    ));
}