<?php
function tc_style_theme_option() {

	return array (
	        array(
		        'id'          => 'style_textblock_titled',
		        'label'       => __( 'Default Color', 'themecountry' ),
		        'desc'        => '',
		        'std'         => '',
		        'type'        => 'textblock-titled',
		        'section'     => 'option_style',
		    ),
		    array(
				'id'        => 'default_primary_color',
				'label'     => '',
				'desc'      => __( 'Primary Color', 'themecountry' ),
				'std'		=> '#36a8ff',
				'type'      => 'colorpicker',
				'section'   => 'option_style',
				'rows'      => '3'
			),
			array(
				'id'        => 'default_active_hover_color',
				'label'     => '',
				'desc'      => __( 'Hover and Active Color', 'themecountry' ),
				'std'		=> '#3368c4',
				'type'      => 'colorpicker',
				'section'   => 'option_style',
				'rows'      => '3'
			),
		  	// Custom Code
			array(
				'id'        => 'custom_style',
				'label'     => __( 'Custom CSS', 'themecountry' ),
				'desc'      => __( 'Add you CSS Custom Code here. (Only For Advance User)', 'themecountry' ),
				'type'      => 'css',
				'section'   => 'option_style',
				'rows'      => '3'
			),
		);
}