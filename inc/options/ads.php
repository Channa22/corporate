<?php
function tc_ads_theme_option () {
	return array (			
			// General: After Post Title
	        array(
	            'id'        => 'ad_after_post_title',
	            'label'     => __( 'Below Post Title', 'themecountry' ),
	            'desc'      => __( 'Paste Your Ads Code Here ( Adsense, Buy Sell Ads ). It will be shown Below Each Post title.', 'themecountry' ),
	            'type'      => 'textarea-simple',
	            'section'   => 'option_ad_mangment',
	            'rows'      => '3'
	        ),

			array(				
				'id'        => 'top_ads_position',
				'label'     => '',
				'desc'      => __( 'Choose Alignment Of your Ads.', 'themecountry' ),
				'type'      => 'select',
				'section'   => 'option_ad_mangment',
				'choices'     => array(
					array(
						'value'       => 'ad-none',
						'label'       => __( 'None',  'themecountry' ),
					),
					array(
						'value'       => 'ad-left',
						'label'       => __( 'Left', 'themecountry' ),
					),
					array(
						'value'       => 'ad-center',
						'label'       => __( 'Center',  'themecountry' ),
					),
					array(
						'value'       => 'ad-right',
						'label'       => __( 'Right', 'themecountry' ),
					)
				)
			),

			// General: Middle Post
	        array(
	            'id'        => 'ad_middle_post',
	            'label'     => __( 'Middle Post Content', 'themecountry' ),
	            'desc'      => __( 'Paste Your Ads Code Here ( Adsense, Buy Sell Ads ). It will be shown right middle of your post.', 'themecountry' ),
	            'type'      => 'textarea-simple',
	            'section'   => 'option_ad_mangment',
	            'rows'      => '3'
	        ),

	        array(
	            'id'        => 'middle_ads_position',
	            'label'     => '',
	            'desc'      => __( 'Choose Alignment Of your Ads.', 'themecountry' ),
	            'type'      => 'select',
	            'section'   => 'option_ad_mangment',
	            'choices'     => array(
	                array(
	                    'value'       => 'ad-none',
	                    'label'       => __( 'None', 'themecountry' ),
	                ),
	                array(
	                    'value'       => 'ad-left',
	                    'label'       => __( 'Left', 'themecountry' ),
	                ),
	                array(
	                    'value'       => 'ad-center',
	                    'label'       => __( 'Center',  'themecountry' ),
	                ),
	                array(
	                    'value'       => 'ad-right',
	                    'label'       => __( 'Right',  'themecountry' ),
	                )
	            )
	        ),
			
			// General: Bottom content 
	        array(
	            'id'        => 'ad_below_post',
	            'label'     => __( 'Below Post content', 'themecountry' ),
	            'desc'      => __( 'Paste Your Ads Code Here ( Adsense, Buy Sell Ads ). It will be shown Below Each Post.', 'themecountry' ),
	            'type'      => 'textarea-simple',
	            'section'   => 'option_ad_mangment',
	            'rows'      => '3'
	        ),

	        array(
	            'id'        => 'below_ads_position',
	            'label'     => '',
	            'desc'      => __( 'Choose Alignment Of your Ads.', 'themecountry' ),
	            'type'      => 'select',
	            'section'   => 'option_ad_mangment',
	            'choices'     => array(
	                array(
	                    'value'       => 'ad-none',
	                    'label'       => __( 'None', 'themecountry' ),
	                ),
	                array(
	                    'value'       => 'ad-left',
	                    'label'       => __( 'Left', 'themecountry' ),
	                ),
	                array(
	                    'value'       => 'ad-center',
	                    'label'       => __( 'Center', 'themecountry' ),
	                ),
	                array(
	                    'value'       => 'ad-right',
	                    'label'       => __( 'Right', 'themecountry' ),
	                )
	            )
	        ),
			
			// duration of ads
	        array(
	            'id'        => 'duration_ads_top',
	            'label'     => __( 'Ads and Post Age', 'themecountry' ),
	            'desc'      => __( 'Only Show Ads on Post Which is Older than number of days you choose.', 'themecountry' ),
				'type'      => 'numeric-slider',
				'std'         => '100',
	            'section'   => 'option_ad_mangment'
	        )
			
		);
}