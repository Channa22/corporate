<?php
function tc_sidebar_theme_option() {

	return array (
        array(
            'id'        => 'sidebar-areas',
            'label'     => __( 'Create Sidebars', 'themecountry' ),
            'desc'      => __( 'Create Custom Sidebars Here if you want to Have Different Sidebar for Homepage and Single Post and So on.', 'themecountry' ),
            'type'      => 'list-item',
            'section'   => 'option_sidebar',
            'choices'   => array(),
            'settings'  => array(
                array(
                    'id'        => 'id',
                    'label'     => __( 'Sidebar ID', 'themecountry' ),
                    'desc'      => __( 'This ID must be unique<br/>Ex: (sidebar-about)', 'themecountry' ),
                    'std'       => 'sidebar-select',
                    'type'      => 'text',
                    'choices'   => array()
                )
            )
        ),
        array(
            'id'        => 's1-home',
            'label'     => __( 'Home', 'themecountry' ),
            'desc'      => __( 'Choose Sidebar to Show On Hompage.', 'themecountry' ),
            'type'      => 'sidebar-select',
            'section'   => 'option_sidebar'
        ),
        array(
            'id'        => 's1-single',
            'label'     => __( 'Single', 'themecountry' ),
            'desc'      => __( 'Choose Sidebar to Show On Single Posts.', 'themecountry' ),
            'type'      => 'sidebar-select',
            'section'   => 'option_sidebar'
        ),
        array(
            'id'        => 's1-archive',
            'label'     => __( 'Archive (Date Based)', 'themecountry' ),
            'desc'      => __( 'Choose Sidebar to Show On Archive Pages<br/>(Date Based Archive)', 'themecountry' ),
            'type'      => 'sidebar-select',
            'section'   => 'option_sidebar'
        ),
        array(
            'id'        => 's1-archive-category',
            'label'     => __( 'Archive Category', 'themecountry' ),
            'desc'      => __( 'Choose Sidebar to Show On Category Pages.', 'themecountry' ),
            'type'      => 'sidebar-select',
            'section'   => 'option_sidebar'
        ),
        array(
            'id'        => 's1-search',
            'label'     => __( 'Search', 'themecountry' ),
            'desc'      => __( 'Choose Sidebar to Show On Search Page', 'themecountry' ),
            'type'      => 'sidebar-select',
            'section'   => 'option_sidebar'
        ),
        array(
            'id'        => 's1-404',
            'label'     => __( 'Error 404', 'themecountry' ),
            'desc'      => __( 'Choose Sidebar to Show On (Error 404, Page Not Found)', 'themecountry' ),
            'type'      => 'sidebar-select',
            'section'   => 'option_sidebar'
        ),
        array(
            'id'        => 's1-page',
            'label'     => __( 'Page', 'themecountry' ),
            'desc'      => __( 'Choose Sidebar to Show On "Pages".', 'themecountry' ),
            'type'      => 'sidebar-select',
            'section'   => 'option_sidebar'
        ),
	);
}