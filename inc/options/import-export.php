<?php
function tc_import_export_theme_option() {
	return array (
		 array(
            'id'          => 'text_desscription_import_export',
            'desc'        => sprintf(__( '<b>Click link to <a href="%s">Import/Export</a></b>', 'themecountry' ), admin_url('themes.php?page=tc-theme-backup')),
            'std'         => '',
            'type'        => 'textblock',
            'section'     => 'option_import_export',
        )
	);
}