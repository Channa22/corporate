<?php
function tc_footer_theme_option() {
    return array(
        array(
            'id'          =>        'section_contact_on_off',
            'label'       =>        __( 'Socail Button and Contact Infomation','themecountry'),
            'desc'        =>        __( 'Enable or Disable','themecountry'),
            'std'         =>        'on',
            'type'        =>        'on-off',
            'section'     =>        'option_footer'
        ),
        array(
            'id'          =>        'contact_social_links',
            'label'       =>        'Social Button Setup',
            'desc'        =>        'Create Social Media',
            'type'        =>        'list-item',
            'section'     =>        'option_footer',
            'settings'    =>
            array(
                  array(
                  'id'      =>      'home_social_icon',
                  'label'   =>      'Social Icon',
                  'desc'    =>      'Font Awesome icon names [<a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank"><strong>View all</strong>]</a>',
                  'type'    =>      'select',
                  'choices' =>
                  array(
                        array(
                            'value'     =>   '',
                            'label'     =>   __( '-- Choose One --', 'themecountry'),
                        ),
                        array(
                            'value'     =>    'fa fa-facebook',
                            'label'     =>     __( 'Facebook','themecountry'),
                        ),
                        array(
                            'value'     =>     'fa fa-twitter',
                            'label'     =>     __( 'Twitter','themecountry'),
                        ),
                        array(
                            'value'     =>    'fa fa-google-plus',
                            'label'     =>    __( 'Google+', 'themecountry' ),
                        ),
                        array(
                        'value'       =>    'fa fa-linkedin',
                        'label'       =>    __( 'Linkedin', 'themecountry' ),
                        ),
                        array(
                          'value'     =>    'fa fa-pinterest-p',
                          'label'     =>     __( 'Pinterest', 'themecountry' ),
                        ),
                        array(
                          'value'     =>    'fa fa-youtube',
                          'label'     =>    __( 'Youtube', 'themecountry' ),
                        ),
                        array(
                          'value'     =>    'fa fa-github',
                          'label'     =>    __( 'Github', 'themecountry' ),
                        ),
                        array(
                          'value'     =>    'fa fa-dribbble',
                          'label'     =>    __( 'Dribbble', 'themecountry' ),
                        ),
                        array(
                          'value'     =>    'fa fa-reddit',
                          'label'     =>    __( 'Reddit', 'themecountry' ),
                        ),
                        array(
                          'value'     =>    'fa fa-vimeo-square',
                          'label'     =>    __( 'Vimeo', 'themecountry' ),
                        ),
                        array(
                          'value'     =>    'fa fa-wordpress',
                          'label'     =>    __( 'WordPress', 'themecountry' ),
                        ),
                        array(
                          'value'     =>    'fa fa-tumblr',
                          'label'     =>    __( 'Tumblr', 'themecountry' ),
                        ),
                        array(
                          'value'     =>    'fa fa-vine',
                          'label'     =>    __( 'Vine', 'themecountry' ),
                        ),
                        array(
                          'value'     =>    'fa fa-slideshare',
                          'label'     =>    __( 'Slideshare', 'themecountry' ),
                        ),  
                        array(
                          'value'     =>    'fa fa-instagram',
                          'label'     =>     __( 'Instagram','themecountry'),
                          )
                      )
                  ),
                  array(
                      'id'        =>  'social_button_links_url',
                      'label'     =>  'Links Url',
                      'desc'      =>  __( 'Social Link Url'),
                      'std'       =>  'http://',
                      'type'      =>  'text'
                  ),
              )
          ),
          array(
              'id'        =>      'copyright',
              'label'     =>      __( 'Footer Copyright Text', 'themecountry' ),
              'desc'      =>      __( 'Add Text you want to show in the Footer.', 'themecountry' ),
              'type'      =>      'textarea-simple',
              'std'       =>      '<a href="http://wordpress.org/">Proudly powered by WordPress</a><span class="sep"> | </span>Theme: startbiz by <a rel="designer" href="http://themecountry.com/">themecountry.com</a>.',
              'rows'      =>      '3',
              'section'   =>      'option_footer'
          ),        
       
      );
  }