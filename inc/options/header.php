<?php
function tc_header_theme_option() {
	return array (
		// General: Favicon
        array(
            'id'        => 'logo',
            'label'     => __( 'Custom Logo Image', 'themecountry' ),
            'desc'      => __( 'Upload your custom logo image here.<br/>Recommended logo (height: 70px, width: 260px)', 'themecountry' ),
            'type'      => 'upload',
            'section'   => 'option_header'
        ),
		array(
			'id'        => 'fixed_menu',
			'label'     => __( 'Sticky Menu', 'themecountry' ),
			'desc'      => __( 'Enable Sticky Menu', 'themecountry' ),
			'std'       => 'off',
			'type'      => 'on-off',
			'section'   => 'option_header'
		),
		array(
			'id'		=>	'btn_sign_up_on_off',
			'label'		=>	__( 'Subscribe Button','themecountry'),
			'desc'		=>	__( 'Enable or Disable','themecountry'),
			'std'		=>	'on',
			'type'		=>	'on-off',
			'section'	=>	'option_header'	
		),
		array(
			'id'		=>	'btn_sign_up_title',
			'label'		=>	__( '','themecountry'),
			'desc'		=>	__( 'Text Button','themecountry'),
			'std'		=>	'Sign Up Now',
			'type'		=>	'text',
			'section'	=>	'option_header'
		),
		array(
			'id'		=>	'url_link_sign_up',
			'label'		=>	__( '','themecountry'),
			'desc'		=>	__('Link Url','themecountry'),
			'std'		=>	'http://',
			'type'		=>	'text',
			'section'	=>	'option_header'
		),
	);
}