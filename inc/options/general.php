<?php

function tc_general_theme_option () {

	return array(
       // General: Favicon
        array(
            'id'        => 'favicon',
            'label'     => __( 'Favicon', 'themecountry' ),
            'desc'      => __( 'Upload a 16x16px Png/Gif image that will be your favicon', 'themecountry' ),
            'type'      => 'upload',
            'section'   => 'option_general'
        ),
        // Layout
        array(
            'id'        => 'layout_width_sidebar',
            'label'     => __( 'Layout', 'themecountry' ),
            'desc'      => __( 'Choose To Show Sidebar On Left Or Right or No-Sidebar', 'themecountry' ),
            'std'       => 'right',
            'type'      => 'radio-image',
            'section'   => 'option_general',
            'class'     => '',
            'choices'   => array(
                array(
                    'value'     => 'left',
                    'label'     => __( 'Sidebare Left', 'themecountry' ),
                    'src'       => get_template_directory_uri() . '/images/layout/col-2cr.png'
                ),
                array(
                    'value'     => 'right',
                    'label'     => __( 'Sidebar Right', 'themecountry' ),
                    'src'       => get_template_directory_uri() . '/images/layout/col-2cl.png'
                ),
                array(
                    'value'     => 'no-sidebar',
                    'label'     => __( 'No Sidebar', 'themecountry' ),
                    'src'       => get_template_directory_uri() . '/images/layout/col-1cl.png'
                )
            )
        ),

        // General Header Code
        array(
            'id'        => 'header_code',
            'label'     => __( 'Header Code', 'themecountry' ),
            'desc'      => __( 'If you have any code you want to add inside and before , Pastes it here. For example: Google Web Master Cool Code or Pinterest Verify Code.', 'themecountry' ),
            'type'      => 'textarea-simple',
            'section'   => 'option_general',
            'rows'      => '3'
        ),

        // General: Tracking Code
        array(
            'id'        => 'tracking_code',
            'label'     => __( 'Footer Code (Tracking, JavaScript Code)', 'themecountry' ),
            'desc'      => __( 'If you have tracking code (Google Analytic or other ), Pastes Your Code here which be inserted before the closing body tag of your theme.', 'themecountry' ),
            'type'      => 'textarea-simple',
            'section'   => 'option_general',
            'rows'      => '3'
        ),

        // General: Comments
        array(
            'id'        => 'page_comments',
            'label'     => __( 'Comment on Page', 'themecountry' ),
            'desc'      => __( 'Use this option to to enable or completely Hide Comment and Comment form on Page', 'themecountry' ),
            'std'       => 'off',
            'type'      => 'on-off',
            'section'   => 'option_general'
        ),

      	// Back To Top
        array(
            'id'        => 'back_to_top',
            'label'     => __( 'Back To Top Button', 'themecountry' ),
            'desc'      => __( 'Use this option to hide or show "Back To Top" button.', 'themecountry' ),
            'std'       => 'on',
            'type'      => 'on-off',
            'section'   => 'option_general'
        ),

        // Contact Information
        array(
            'id'        =>  'phone_number_contact',
            'label'     =>  __( 'Header Information Contact','themecountry'),
            'desc'      =>  __( 'Phone Number','themecountry'),
            'std'       =>  '(+855)77-588-728',
            'type'      =>  'text',
            'section'   =>  'option_general'         
        ),
        array(
            'id'        =>  'email_address_contact',
            'label'     =>  __( '','themecountry'),
            'desc'      =>  __( 'Email Address','themecountry'),
            'std'       =>  'info@themecountry.com',
            'type'      =>  'text',
            'section'   =>  'option_general'     
        )
    );

}