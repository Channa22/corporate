<?php
function tc_blog_theme_option() {
	return array (
			array(
                'id'        => 'post_text',
                'label'     => __( 'Home Page Entry Text', 'themecountry' ),
                'desc'      => __( 'Use this option to show Expert or Full Content for each posts in Homepage, Blog Posts and Archive Pages.', 'themecountry' ),
                'std'       => 'excerpt',
                'type'      => 'radio',
                'section'   => 'option_blog',
                'choices'   => array(
                    array( 
                        'value' => 'excerpt',
                        'label' => __('Show Excerpt', 'themecountry')
                    ),
                    array( 
                        'value' => 'content',
                        'label' => __('Show Content', 'themecountry')
                    )
                )
            ),
            array(
                'id'            => 'excerpt_length',
                'label'         => 'Excerpt Length',
                'desc'          => __('Number of words as Expert Length to be Shown in Homepage, Blog Posts and Archive Page when you choose to show entry text as Excerpt.', 'themecountry' ),
                'std'           => 46,
                'type'          => 'numeric-slider',
                'section'       => 'option_blog',
                'min_max_step'  => '0,100,1'
            ),
            array(
                'id'            => 'excerpt_more',
                'label'         => __('Excerpt Ending Text', 'themecountry'),
                'std'           => ' ...',
                'type'          => 'text',
                'section'       => 'option_blog',
            ),
            array(
                'id'            => 'blog_more_blocked_title',
                'label'         => __('Read More', 'themecountry'),
                'std'           => 'on',
                'type'          => 'textblock-titled',
                'section'       => 'option_blog',
            ),
            array(
                'id'            => 'enable_more_button',
                'desc'          => __('Enable or Disable', 'themecountry'),
                'std'           => 'on',
                'type'          => 'on-off',
                'section'       => 'option_blog',
            ),
            array(
                'id'          => 'pagination',
                'label'       => __( 'Pagination', 'themecountry', 'themecountry' ),
                'desc'        => __( 'Choose your Preferred Pagination Style.', 'themecountry' ),
                'std'         => 'default',
                'type'        => 'radio',
                'section'     => 'option_blog',        
                'choices'     => array( 
                    array(
                    'value'       => 'default',
                    'label'       => __( 'Default (Older Posts/Newer Posts)', 'themecountry' ),
                    ),
                    array(
                    'value'       => 'numberal',
                    'label'       => __( 'Numberal (1 2 3 ...)', 'themecountry' ),
                    ),
                    array(
                    'value'       => 'loading',
                    'label'       => __( 'Load More...', 'themecountry' ),
                    ),
                    array(
                    'value'     => 'infinite',
                    'label'     => __( 'Auto Infinite Scroll', 'themecountry')
                    )
                )
            ),
            array(
                'id'        => 'blog_enable_post_meta_info',
                'label'     => __( 'Enable Post Meta Info', 'themecountry' ),
                'desc'      => __( 'Enable or Disable Post Meta Info<br/>(Author name, Date etc.).', 'themecountry' ),
                'std'       => 'on',
                'type'      => 'on-off',
                'section'   => 'option_blog'
            ),
            array(
                'id'          => 'blog_post_meta_info',
                'label'       => __( 'What To Hide in Meta Info', 'themecountry' ),
                'desc'        => __( 'Choose Meta Info to Hide', 'themecountry' ),
                'type'        => 'checkbox',
                'section'     => 'option_blog',        
                'choices'     => array( 
                    array(
                    'value'       => 'autho_name',
                    'label'       => __( 'Author Name', 'themecountry' ),
                    ),
                    array(
                    'value'       => 'date',
                    'label'       => __( 'Date', 'themecountry' ),
                    ),
                    array(
                    'value'       => 'category',
                    'label'       => __( 'Category', 'themecountry' ),
                    ),
                    array(
                    'value'       => 'tag',
                    'label'       => __( 'Tag', 'themecountry' ),
                    ),
                    array(
                    'value'       => 'comment_count',
                    'label'       => __( 'Comment Count', 'themecountry' ),
                    )
                )
            ),
		);
    }