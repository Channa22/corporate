<?php
// Include banner 125x125 Widget
require get_template_directory() . '/inc/widget/banner125.php';

// Include banner 300x250 Widget
require get_template_directory() . '/inc/widget/banner300.php';

// Include Facebook Like Box Widget
require get_template_directory() . '/inc/widget/facebook-like-box.php';

// Include Google+ Widget
require get_template_directory() . '/inc/widget/googleplus.php';

// Include Google+ Widget
require get_template_directory() . '/inc/widget/tweets.php';

// Include Pinterest Profile Widget
require get_template_directory() . '/inc/widget/pinterest.php';

// Include Recent Posts Widget
require get_template_directory() . '/inc/widget/recent-posts.php';

// Include Related Posts Widget
require get_template_directory() . '/inc/widget/related-posts.php';

// Include Author's Posts Widget
require get_template_directory() . '/inc/widget/author-posts.php';

// Include Category's Posts Widget
require get_template_directory() . '/inc/widget/category-posts.php';

// Include Popular Posts Widget
require get_template_directory() . '/inc/widget/popular.php';

// Include Static Company Info Widget
require get_template_directory() . '/inc/widget/company-info.php';


