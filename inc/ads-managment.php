<?php
/**
 * TC Theme Ads Functions
 *
 * @package StartBiz
 */


if (!function_exists('tc_ad_managment')) :

	/**
	|------------------------------------------------------------------------------
	| Ads Managment
	|------------------------------------------------------------------------------
	| 
	| Three ads postion on single post
	| 
	| 1. After post title (Postions: left, center or right)
	| 2. Middle post content (Position: left, center or right)
	| 3. Below post content (Position: left, center or right)
	| 
	| @return void
	|
	*/

	function tc_ad_managment($content) {
		global $post;


		if (!is_single()) return $content;


		$today = date_create(date('Y-m-d'));
		$published = date_create(get_the_date('Y-m-d', $post->ID));
		$interval = date_diff($today, $published);
		$age = $interval->format('%a');

		if (  $age >= tc_get_option('duration_ads_top') ) {

			if (tc_get_option('ad_after_post_title')) {
				$content = '<div class="ads-banner-block top-single-ads '. tc_get_option('top_ads_position') .'">' . tc_get_option('ad_after_post_title') . '</div>' . $content;
			}

			if (tc_get_option('ad_middle_post')) {
				$content =  tc_ad_middle_content($content, '<div class="ads-banner-block middle-single-ads '. tc_get_option('middle_ads_position') .'">' . tc_get_option('ad_middle_post') . '</div>');
			}
			
			if (tc_get_option('ad_below_post')) {
				$content = $content . '<div class="ads-banner-block below-single-ads '. tc_get_option('below_ads_position') .'">' . tc_get_option('ad_below_post') . '</div>';
			}
		}

		return $content;

	}

	add_filter('the_content', 'tc_ad_managment');

endif;

/**
|------------------------------------------------------------------------------
| Render ads middle post content
|------------------------------------------------------------------------------
| 
| @return string
|
*/
 
function tc_ad_middle_content( $content, $middle_ad ) {

	$content = explode("</p>", $content);
    $new_content = '';
    $paragraphAfter = round(count($content)/2); //Enter number of paragraphs to display ad after.

    for ($i = 0; $i < count($content); $i++) {

        if ($i == $paragraphAfter) {
          $new_content .= $middle_ad;
        }

        $new_content.= $content[$i] . "</p>";
    }

    return $new_content;
}