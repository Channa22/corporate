<?php
/*
* TC Portfolio Script
*/

function tc_portfolio_register() {
 
	register_post_type(
		'tc_portfolio',
		array(
			'labels'        => array(
				'name'               => __('Portfolio', 'startbiz'),
				'singular_name'      => __('Portfolio', 'startbiz'),
				'menu_name'          => __('Portfolio', 'startbiz'),
				'name_admin_bar'     => __('Portfolio Item', 'startbiz'),
				'all_items'          => __('All Items', 'startbiz'),
				'add_new'            => _x('Add New', 'tc_portfolio', 'startbiz'),
				'add_new_item'       => __('Add New Item', 'startbiz'),
				'edit_item'          => __('Edit Item', 'startbiz'),
				'new_item'           => __('New Item', 'startbiz'),
				'view_item'          => __('View Item', 'startbiz'),
				'search_items'       => __('Search Items', 'startbiz'),
				'not_found'          => __('No items found.', 'startbiz'),
				'not_found_in_trash' => __('No items found in Trash.', 'startbiz'),
				'parent_item_colon'  => __('Parent Items:', 'startbiz'),
			),
			'public'        => true,
			'menu_position' => 5,
			'supports'      => array(
				'title',
				'editor',
				'thumbnail',
				'excerpt',
				'custom-fields',
			),
			'taxonomies'    => array(
				'tc_portfolio_categories',
			),
			'has_archive'   => true,
			'rewrite'       => array(
				'slug' => 'portfolio',
			),
		)
	);
}
add_action('init', 'tc_portfolio_register');

/**
 |------------------------------------------------------------------------------
 | Sets the custom update messages for a custom post type.
 |------------------------------------------------------------------------------
 | @param   array  $messages
 | @return  array
 | @link    http://codex.wordpress.org/Function_Reference/register_post_type
 */
function tc_post_updated_messages($messages)
{
	$post             = get_post();
	$post_type        = get_post_type($post);
	$post_type_object = get_post_type_object($post_type);

	$messages['tc_portfolio'] = array(
		0  => '',
		1  => __('Item updated.', 'startbiz'),
		2  => __('Custom field updated.', 'startbiz'),
		3  => __('Custom field deleted.', 'startbiz'),
		4  => __('Item updated.', 'startbiz'),
		5  => isset($_GET['revision']) ? sprintf(__('Item restored to revision from %s', 'startbiz'), wp_post_revision_title( (int) $_GET['revision'], false)) : false,
		6  => __('Item published.', 'startbiz'),
		7  => __('Item saved.', 'startbiz'),
		8  => __('Item submitted.', 'startbiz'),
		9  => sprintf(__('Item scheduled for: <strong>%1$s</strong>.', 'startbiz'), date_i18n(__('M j, Y @ G:i', 'startbiz'), strtotime($post->post_date))),
		10 => __('Item draft updated.', 'startbiz'),
	);

	if ($post_type_object->publicly_queryable)
	{
		$permalink         = get_permalink($post->ID);
		$preview_permalink = add_query_arg('preview', 'true', $permalink);

		switch ($post_type)
		{
			case 'tc_portfolio':
				$view_link    = sprintf(' <a href="%s">%s</a>', esc_url($permalink), __('View item', 'startbiz'));
				$preview_link = sprintf(' <a href="%s" target="_blank">%s</a>', esc_url($preview_permalink), __('Preview item', 'startbiz'));
			break;
		}

		if (isset($view_link))
		{
			$messages[$post_type][1] .= $view_link;
			$messages[$post_type][6] .= $view_link;
			$messages[$post_type][9] .= $view_link;
		}

		if (isset($preview_link))
		{
			$messages[$post_type][8]  .= $preview_link;
			$messages[$post_type][10] .= $preview_link;
		}
	}

	return $messages;
}

add_filter('post_updated_messages', 'tc_post_updated_messages');

/**
 |------------------------------------------------------------------------------
 | Registers a custom taxonomy.
 |------------------------------------------------------------------------------
 |
 | @link http://codex.wordpress.org/Function_Reference/register_taxonomy
 |
 */
function tc_register_taxonomy()
{
	register_taxonomy(
		'tc_portfolio_categories',
		array(
			'tc_portfolio',
		),
		array(
			'labels'            => array(
				'name'              => _x('Categories', 'tc_portfolio', 'startbiz'),
				'singular_name'     => _x('Category', 'tc_portfolio', 'startbiz'),
				'menu_name'         => __('Categories', 'startbiz'),
				'all_items'         => __('All Categories', 'startbiz'),
				'edit_item'         => __('Edit Category', 'startbiz'),
				'view_item'         => __('View Category', 'startbiz'),
				'update_item'       => __('Update Category', 'startbiz'),
				'add_new_item'      => __('Add New Category', 'startbiz'),
				'new_item_name'     => __('New Category Name', 'startbiz'),
				'parent_item'       => __('Parent Category', 'startbiz'),
				'parent_item_colon' => __('Parent Category:', 'startbiz'),
				'search_items'      => __('Search Categories', 'startbiz'),
			),
			'show_admin_column' => true,
			'hierarchical'      => true,
			'rewrite'           => array(
				'slug' => 'portfolio/category',
			),
		)
	);
}

add_action('init', 'tc_register_taxonomy', 0);

/**
 |------------------------------------------------------------------------------
 | Flushes the rewrite rules.
 |------------------------------------------------------------------------------
 |
 | @link http://codex.wordpress.org/Function_Reference/register_post_type
 |
 */
function tc_flush_rewrite_rules()
{
	flush_rewrite_rules();
}

add_action('after_switch_theme', 'tc_flush_rewrite_rules');