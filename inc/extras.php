<?php

/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features
 *
 * @package StartBiz
 */
if ( ! function_exists( 'tcc_favicon' ) ) :

	/**
	|------------------------------------------------------------------------------
	| Customize favicon
	|------------------------------------------------------------------------------
	| 
	| @return void
	|
	*/

	function tcc_favicon () {

		if (ot_get_option('favicon')):
		?>
		<link rel="shortcut icon" href="<?php echo esc_url(ot_get_option('favicon')); ?>"/>
		<?php
		endif;
	}

	add_action('wp_head','tcc_favicon');

endif;

if (! function_exists('tcc_header_code')):
	
	/**
	|------------------------------------------------------------------------------
	| Header Code Theme Option
	|------------------------------------------------------------------------------
	| 
	| @return void
	|
	*/

	function tcc_header_code() {
		echo ot_get_option('header_code');
	}
	add_action('wp_head','tcc_header_code');
endif;

if (! function_exists('tcc_tracking_code')):

	/*
	|------------------------------------------------------------------------------
	| Tracking Code Theme Option
	|------------------------------------------------------------------------------
	| 
	| @return void
	|
	*/

	function tcc_tracking_code() {
		echo ot_get_option('tracking_code');
	}

	add_action('wp_footer','tcc_tracking_code');

endif;
/**
 |------------------------------------------------------------------------------
 | Adds custom classes to the array of body classes.
 |------------------------------------------------------------------------------
 | @param array $classes Classes for the body element.
 | @return array
 |
 */
function startbiz_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	return $classes;
}
add_filter( 'body_class', 'startbiz_body_classes' );

if ( version_compare( $GLOBALS['wp_version'], '4.1', '<' ) ) :
	/**
	 |------------------------------------------------------------------------------
	 | Filters wp_title to print a neat <title> tag based on what is being viewed.
	 |------------------------------------------------------------------------------
	 | @param string $title Default title text for current view.
	 | @param string $sep Optional separator.
	 | @return string The filtered title.
	 */
function startbiz_wp_title( $title, $sep ) {
	if ( is_feed() ) {
		return $title;
	}

	global $page, $paged;

	// Add the blog name
	$title .= get_bloginfo( 'name', 'display' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title .= " $sep $site_description";
	}

	// Add a page number if necessary:
	if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
		$title .= " $sep " . sprintf( __( 'Page %s', 'startbiz' ), max( $paged, $page ) );
	}

	return $title;
}
add_filter( 'wp_title', 'startbiz_wp_title', 10, 2 );

/**
	 |------------------------------------------------------------------------------
	 | Title shim for sites older than WordPress 4.1.
	 |------------------------------------------------------------------------------
	 |
	 | @link https://make.wordpress.org/core/2014/10/29/title-tags-in-4-1/
	 | @todo Remove this function when WordPress 4.3 is released.
	 */
	function startbiz_render_title() {
		?>
		<title><?php wp_title( '|', true, 'right' ); ?></title>
		<?php
	}
	add_action( 'wp_head', 'startbiz_render_title' );
endif;

/**
|------------------------------------------------------------------------------
| Banner Background on Home Page
|------------------------------------------------------------------------------
*/
function tcc_bg_banner_home () {
	require get_template_directory() . '/inc/Mobile-Detect/Mobile_Detect.php';

	$detect = new Mobile_Detect;

	if ( ($detect->isMobile() || $detect->isTablet()) && tc_get_option('home_first_banner_img_mobile')  ) {
		$src_url = tc_get_option('home_first_banner_img_mobile');
	} else {
		$src_url = tc_get_option('home_first_banner_img');; 
	}

	if ( $src_url ) {
		return 'background-image: url(' . $src_url . ');' ;
	}

	return false;
}

/**
|------------------------------------------------------------------------------
| Blog Post In Home
|------------------------------------------------------------------------------
|
*/
function tcc_home_blog_post() {
	$blog_posts = tc_get_option('home_blog_show_limit');	
	$the_query = new WP_Query( array( 'posts_per_page' => $blog_posts ) );

	if ( $the_query->have_posts() ) {
?>
	<div class="container-fluid">
		<div class="row no-gutter">

			<?php
			while ( $the_query->have_posts() ) {
			$the_query->the_post();
			?>
			<div class="col-xs-6 col-sm-4 col-md-3">					
				<div class="latest-blog">					
				    <?php if (has_post_thumbnail()) : ?>
						<?php the_post_thumbnail('post-thumbnails-blog'); ?>
					<?php endif; ?>

					<div class="latest-blog-caption">
						<div class="latest-blog-content mid-align">
							
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
								<h3 class="project-title"><?php the_title(); ?></h3>
							</a>

							<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post) ); ?>

							<div class="c-overlay-content image-overlay">
								<a class="prettyPhoto" href="<?php echo $url; ?>" rel="prettyPhoto" title="<?php the_title(); ?>">	
									<i class="fa fa-search-plus"></i>
								</a>
								<a href="<?php the_permalink(); ?>" title="view this post">
									<i class="fa fa-link"></i>
								</a>
							</div>

						</div>
					</div>
				</div>					
			</div>
			<?php
			}
			?>
		</div>  	
	</div>

    <div class="site-view-more">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<?php
						$query = new WP_Query( array( 'taxonomy' => 'term', 'posts_per_page' => -1 ) );
						$count = $query->post_count;
					if ($blog_posts < $count) :
					?>
	    				<a href="<?php echo tc_get_option('url_view_more_post'); ?>" class="btn btn-default btn-xl"><?php echo tc_get_option('title_btn_view_more_post'); ?></a>
					
					<?php endif ?>
				
				</div>    				
			</div>    			
		</div>
    </div>
    <?php
	} else {
		echo "Sorry we don`t have posts to show!";
	}
}

/**
|----------------------------------------------------------------------------
|# Section Sponsor
|----------------------------------------------------------------------------
*/

function tcc_sponsor_profile() { ?>
	<div class="container-fluid">
        <div class="row">
            <div class="row-logos-carousel">
                <div class="container">
                    <div id="owl-demo" class="owl-carousel owl-theme">
                    <?php
                        $profile_sponsor = tc_get_option('sponsor_logo_list');
                        $i = 1;
                        if($profile_sponsor) :
                        foreach ($profile_sponsor as $pro_spo) :
                    ?>
                        <div class="item">
                        	<?php if( $pro_spo['sponsor_link'] ) : ?>
                          		<a href="<?php echo $pro_spo['sponsor_link']; ?>" title="<?php echo $pro_spo['title']; ?>" target="_blank">
                          			<img src="<?php echo $pro_spo['sponsor_img']; ?>" alt="<?php echo $pro_spo['title']; ?>">
                          		</a>
                        	<?php else: ?>
                        		<img src="<?php echo $pro_spo['sponsor_img']; ?>" alt="<?php echo $pro_spo['title']; ?>">
                        	<?php endif ?>
                        </div>
                          
					<?php
					if($i == 17)
					{
						break;
					}
					   $i++;
					   endforeach;
					   endif;
					?>
                          
                    </div>
                   
                </div>
            </div>
        </div>
    </div>

<?php

}

/**
|-----------------------------------------------------------------------------------
|# Section Contact
|-----------------------------------------------------------------------------------
*/
function tcc_feature_contact() { ?>
	<div class="container">
		<div class="social-links-home">				
			<?php
			$links = tc_get_option('contact_social_links');
			if (count($links) > 0 && is_array($links)) {
				foreach($links as $link):
					$txtlower = strtolower($link['home_social_icon']); ?>
					<a class="social-link <?php echo $txtlower;?>" href="<?php echo $link['social_button_links_url'];?>" title="<?php echo $link['title'];?>">
						<i class="<?php echo $link['home_social_icon']; ?>"></i>
						<i class="<?php echo $link['home_social_icon']; ?>"></i>
					</a>
			<?php
			endforeach;
			}
			?>
		</div>

		<div class="contact-info-home">
			<i class="fa fa-phone"></i>
			<?php echo tc_get_option('phone_number_contact'); ?>
			
			<span class="sep"></span>

			<i class="fa fa-envelope"></i>
			<a href="mailto:<?php echo tc_get_option('email_address_contact'); ?>?Subject=Hello%20again" target="_top"><?php echo tc_get_option('email_address_contact'); ?></a>
		</div>
	</div>
<?php
}

/**
|-------------------------------------------------------------------------------
|# Site Member
|-------------------------------------------------------------------------------
*/
function tcc_feature_team_member() { ?>

	<div class="container">
        <div class="row">

            <div class="col-lg-12 text-center">
                <h2 class="section-title bottom-border"><?php echo tc_get_option('title_team_member'); ?></h2>
                <p class="text-muted"><?php echo tc_get_option('desc_team_member'); ?></p>
            </div>
            
            <?php
                $team_member = tc_get_option('add_member_list');
                $i = 1;
                if ($team_member) :
                $count = count($team_member);
                $grid = 12 / $count;
                
                foreach($team_member as $team_member) :

                $attachment_url = get_attachment_id_from_src( $team_member['img_profile_member'] );
                $image_thumb = wp_get_attachment_image_src($attachment_url, 'post-thumbnails-service');
            ?>

	            <div class="col-lg-<?php echo $grid ?> col-sm-6 col-xs-12 focus-box">
	                <div class="tc-team-member">

	                    <?php if ( $team_member['list_url_member_detail'] != '' && $team_member['list_url_member_detail'] != 'http://' ) : ?>
	                        
	                        <figure class="profile-pic">
	                            <a href="<?php echo $team_member['list_url_member_detail']; ?>">
	                                <img src="<?php echo $image_thumb[0]; ?>" alt="<?php  echo $team_member['title']; ?>" />
	                            </a>
	                        </figure>

	                        <div class="member-detail">
	                        	<a href="<?php echo $team_member['list_url_member_detail']; ?>"><h3 class="member-title"><?php echo $team_member['title']; ?></h3></a>

	                    <?php else : ?>
	                        
	                        <figure class="profile-pic">
	                            <img src="<?php echo $image_thumb[0]; ?>" alt="<?php  echo $team_member['title']; ?>" />
	                        </figure>

	                        <div class="member-detail">
	                        	<h3 class="member-title"><?php echo $team_member['title']; ?></h3>

	                    <?php endif ?>
	                           

                        <?php if (tc_get_option('enable_mem_social_share') != 'off') : ?>                       
                            <div class="member-social-icon">
                                <ul>
                                    
                                    <?php if ($team_member['link_acc_fb'] != '' && $team_member['link_acc_fb'] != "http://" ) : ?>
                                        <li><a href="<?php echo $team_member['link_acc_fb']; ?>" target="_blank" ><i class="fa fa-facebook"></i></a></li>
                                    <?php endif; ?>
                                    <?php if ($team_member['link_acc_tw'] != '' && $team_member['link_acc_tw'] != "http://" ) : ?>
                                        <li><a href="<?php echo $team_member['link_acc_tw']; ?>" target="_blank" ><i class="fa fa-twitter"></i></a></li>
                                    <?php endif; ?>
                                    <?php if ($team_member['link_acc_youtube'] != '' && $team_member['link_acc_youtube'] != "http://" ) : ?>
                                        <li><a href="<?php echo $team_member['link_acc_youtube']; ?>" target="_blank" ><i class="fa fa-youtube"></i></a></li>
                                    <?php endif; ?>
                                    <?php if ($team_member['link_acc_mail'] != '' && $team_member['link_acc_mail'] != "http://" ) : ?>
                                        <li><a href="<?php echo $team_member['link_acc_mail']; ?>" target="_blank" ><i class="fa fa-envelope"></i></a></li>
                                    <?php endif; ?>

                                </ul>
                            </div>
                        <?php endif; ?>
                        <p class="position"><?php echo $team_member['member_description']; ?></p>
                    </div>
                        
                </div>
            </div>
        
	        <?php 
	            if ($i == 4) {
	                break;
	            }
	            $i++;
	        
	            endforeach;
	            endif;
	        ?>

        </div>
    </div>

<?php
}

/**
|------------------------------------------------------------------------------
| Social Links
|------------------------------------------------------------------------------
|
*/
function tc_social_links() {
	$links = tc_get_option('social_links');

	if (count($links) > 0 && is_array($links)) {
	?>
		 <ul class="footer-social-icons">
		 <?php  
		 	foreach($links as $link) :
		 	$target = $link['social-target'] ? $link['social-target'][0] : '_parent';
			$style = sprintf('color:  %s;', $link['social-color']);
		 ?>
		 	<li>
		 		<a href="<?php echo $link['social-link']; ?>" target="<?php echo $target ?>" style="<?php echo $style ?>">
		 			<span class="<?php echo $link['social-icon']; ?>"></span>
		 		</a>
		 	</li>
         <?php endforeach; ?>
        </ul>
	<?php
	}
}

if (! function_exists(' tcc_related_posts') ):
	/**
	|------------------------------------------------------------------------------
	| Related Posts
	|------------------------------------------------------------------------------
	|
	| You can show related posts by Categories or Tags. 
	| It has two options to show related posts
	|
	| 1. Thumbnail related posts (default)
	| 2. List of related posts
	| 
	| @return void
	|
	*/

	function  tcc_related_posts() {
		global $post;

		$taxonomy = ot_get_option('related-posts-taxonomy');
		$numberRelated = ot_get_option('number-related'); 
		$args =  array();

		if ($taxonomy == 'tag') {

			$tags = wp_get_post_tags($post->ID);
			$arr_tags = array();
			foreach($tags as $tag) {
				array_push($arr_tags, $tag->term_id);
			}
			
			if (!empty($arr_tags)) { 
			    $args = array(  
				    'tag__in' => $arr_tags,  
				    'post__not_in' => array($post->ID),  
				    'posts_per_page'=> $numberRelated,
			    ); 
			}

		} else {

			$args = array( 
			 	'category__in' => wp_get_post_categories($post->ID), 
			 	'posts_per_page' => $numberRelated, 
			 	'post__not_in' => array($post->ID) 
			);

		}

		if (! empty($args) ) {
			$posts = get_posts($args);

			if ($posts) {
				if (ot_get_option('display-related-posts') == 'thumbnail') : 
				?>

				<?php
				$post_types = get_post_type($post);
				if ($post_types == 'tc_portfolio') {
				?>
					<h3><?php _e('Related Projects', 'startbiz') ?></h3>
				<?php
				} else {
				?>
					<h3><?php _e('Related Posts', 'startbiz') ?></h3>
				<?php
				}
				?>

				<ul class="related-post-single grid">
				<?php
				foreach ($posts as $p) {
					
					?>

					<li>
						<div class="related-entry">
							<?php if (has_post_thumbnail($p->ID)) : ?>
								<div class="thumbnail-relate">
									<a href="<?php echo get_the_permalink($p->ID) ?>">
										<?php echo get_the_post_thumbnail($p->ID, 'related-post-singlethumbnail'); ?>
									</a>
								</div>
							<?php endif; ?>

							<header>
								<h2 class="related-title">
									<a href="<?php echo get_the_permalink($p->ID) ?>"><?php echo get_the_title($p->ID) ?></a>
								</h2>
								<?php if (tc_get_option('blog_enable_post_meta_info') != 'off' ) : ?>
									<div class="entry-meta">
										<?php startbiz_related_posts_meta_info(); ?>					
									</div><!-- .entry-meta -->
								<?php endif; ?>
							</header>
						</div>
					</li>
					<?php
				}
				?>
				</ul>

				<?php else : ?>
				
				<?php
				$post_types = get_post_type($post);
				if ($post_types == 'tc_portfolio') {
				?>
					<h3><?php _e('Related Projects', 'startbiz') ?></h3>
				<?php
				} else {
				?>
					<h3><?php _e('Related Posts', 'startbiz') ?></h3>
				<?php
				}
				?>

				<ul class="related list">
				<?php
				foreach ($posts as $p) :
					?>
					<li>
						<a href="<?php echo get_the_permalink($p->ID) ?>"><?php echo get_the_title($p->ID) ?></a>
					</li>
					<?php
				endforeach;
				?>
				</ul>
				<?php
			endif; // display related posts thumbnail
			}
		}
	}
endif;
/**
|------------------------------------------------------------------------------
| Default Theme Breadcrumb
|------------------------------------------------------------------------------
| 
| @return void
|
*/



function tcc_breadcrumb() {
	$showOnHome = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
	$delimiter = '&raquo;'; // delimiter between crumbs
	$home = 'Home'; // text for the 'Home' link
	$showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
	$before = '<span class="current">'; // tag before the current crumb
	$after = '</span>'; // tag after the current crumb
 
  global $post;
  $homeLink = get_bloginfo('url');
 
  if (is_home() || is_front_page()) {
 
    if ($showOnHome == 1) echo '<div id="crumbs"><a href="' . $homeLink . '">' . $home . '</a></div>';
 
  } else {
 
    echo '<div id="crumbs"><a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . ' ';
 
    if ( is_category() ) {
      $thisCat = get_category(get_query_var('cat'), false);
      if ($thisCat->parent != 0) echo get_category_parents($thisCat->parent, TRUE, ' ' . $delimiter . ' ');
      echo $before . __('Archive by category "', 'startbiz') . single_cat_title('', false) . '"' . $after;
 
    } elseif ( is_search() ) {
      echo $before . __('Search results for ', 'startbiz') . get_search_query() . '"' . $after;
 
    } elseif ( is_day() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('d') . $after;
 
    } elseif ( is_month() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('F') . $after;
 
    } elseif ( is_year() ) {
      echo $before . get_the_time('Y') . $after;
 
    } elseif ( is_single() && !is_attachment() ) {
      if ( get_post_type() != 'post' ) {
        $post_type = get_post_type_object(get_post_type());
        $slug = $post_type->rewrite;
        echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a>';
        if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
      } else {
        $cat = get_the_category(); $cat = $cat[0];
        $cats = get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
        if ($showCurrent == 0) $cats = preg_replace("#^(.+)\s$delimiter\s$#", "$1", $cats);
        echo $cats;
        if ($showCurrent == 1) echo $before . get_the_title() . $after;
      }
 
    } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
      $post_type = get_post_type_object(get_post_type());
      echo $before . $post_type->labels->singular_name . $after;
 
    } elseif ( is_attachment() ) {
      $parent = get_post($post->post_parent);
      $cat = get_the_category($parent->ID); $cat = $cat[0];
      echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
      echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a>';
      if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
 
    } elseif ( is_page() && !$post->post_parent ) {
      if ($showCurrent == 1) echo $before . get_the_title() . $after;
 
    } elseif ( is_page() && $post->post_parent ) {
      $parent_id  = $post->post_parent;
      $breadcrumbs = array();
      while ($parent_id) {
        $page = get_page($parent_id);
        $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
        $parent_id  = $page->post_parent;
      }
      $breadcrumbs = array_reverse($breadcrumbs);
      for ($i = 0; $i < count($breadcrumbs); $i++) {
        echo $breadcrumbs[$i];
        if ($i != count($breadcrumbs)-1) echo ' ' . $delimiter . ' ';
      }
      if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
 
    } elseif ( is_tag() ) {
      echo $before . __('Posts tagged "', 'startbiz') . single_tag_title('', false) . '"' . $after;
 
    } elseif ( is_author() ) {
       global $author;
      $userdata = get_userdata($author);
      echo $before . __('Articles posted by ', 'startbiz') . $userdata->display_name . $after;
 
    } elseif ( is_404() ) {
      echo $before . __('Error 404', 'startbiz') . $after;
    }
 
    if ( get_query_var('paged') ) {
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
      echo __('Page') . ' ' . get_query_var('paged');
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
    }
 
    echo '</div>';
 
	} 
}

/**
|------------------------------------------------------------------------------
| Load Google Typography Library
|------------------------------------------------------------------------------
*/

require get_template_directory() . '/inc/google-typography/google-typography.php';

/**
|------------------------------------------------------------------------------
| Initial Google Font UI
|------------------------------------------------------------------------------
| 
| @return void
|
*/
function ot_type_typography_google () {
	?>
	<div class="gooogle-typography">
	<p class="gt-desc"><?php _e('From here, you can control the fonts used on your site. You can choose from 17 standard font sets, or from the Google Fonts Library containing 600+ fonts.', 'startbiz') ?></p>
	<?php
		$typography = new GoogleTypography();
	    $typography->options_ui();
    ?>
    </div>
    <?php

}

/**
| Contact Us
*/
function tc_contact_send() {

	$name = esc_html($_POST['name']);
	$email = esc_html($_POST['email']);
	$message = "From: $name \r\n". "Email: $email \r\n". esc_textarea($_POST['message']);

	$to = tc_get_option('contact-email') ? tc_get_option('contact-email') : get_option( 'admin_email' );

	$headers = "From: $name <$email>" . "\r\n";
	wp_mail( $to, 'Client Contact', $message, $headers );

	 _e('Your massage has been sent.', 'startbiz');
	 exit;
}

add_action('wp_ajax_tc_contact_send', 'tc_contact_send');        // for logged in user
add_action('wp_ajax_nopriv_tc_contact_send', 'tc_contact_send');