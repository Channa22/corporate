<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package StartBiz
 */


if (tc_get_option('layout_width_sidebar') != 'no-sidebar') :

$sidebar = tc_sidebar_primary();

?>
<div class="col-lg-4 col-md-4 col-sm-12 sidebar-right">
	<div id="secondary" class="widget-area sidebar" role="complementary">
		<?php dynamic_sidebar( $sidebar ); ?>
	</div><!-- #secondary -->
</div>
<!-- End colome-lg-4 -->

<?php endif; ?>
</div>
<!-- end row2 -->
</div>
<!-- end .container -->