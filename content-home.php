    <?php
    $projects = tc_get_option('feather_projects');
    ?>

    <?php  if (tc_get_option('first_banner') != 'off') : ?>
        <section id="intro" class="intro-banner site-banner" style="<?php echo tcc_bg_banner_home() ?>">
            <div class="header-content">
                <div class="header-content-inner">
                    <h2 class="tc-title"><?php echo tc_get_option('home_first_banner_title') ?></h2>
                    <p class="tc-intro"><?php echo tc_get_option('home_first_banner_short_desc') ?></p>       
                    <?php if ( tc_get_option('home_first_banner_link') ): ?>
                        <div class="tc-buttons">
                            <a href="<?php echo esc_url(tc_get_option('home_first_banner_link')) ?>" class="btn btn-primary btn-xl page-scrolls">
                                <?php _e('Learn More', 'startbiz') ?>
                            </a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </section>
    <?php endif; ?>

    <?php  if ( tc_get_option('featured_services') != 'off' && !empty($projects) ) : ?>
        <section id="welcome-business" class="welcome-business">
            <div class="container">  
                <div class="row">
                    <div class="col-lg-12 text-center ">
                        <h2 class="section-title bottom-border">
                            <?php echo tc_get_option('featured_services_section_title'); ?>
                        </h2>
                        <p class="intro-dis"><?php echo tc_get_option('home_modules_section_short_desc'); ?></p>
                    </div>

                    <div class="wrap-row">
                        <ul class="clean-list text-center wrap-box-list clearboth">
                           <?php
                                foreach($projects as $projects) :

                                $attachment_url = get_attachment_id_from_src( $projects['feather_projects_img'] );
                                $image_thumb = wp_get_attachment_image_src($attachment_url, 'post-thumbnails-service');
                            ?>
                            <li class="col-md-3 col-sm-4">
                                <?php if ( $projects['feather_projects_url'] != '' && $projects['feather_projects_url'] != 'http://' ) : ?>
                                    <div class="facility-item">
                                        <div class="shape-square lightblue">
                                            <figure>
                                                <a href="<?php echo $projects['feather_projects_url']; ?>">
                                                    <img src="<?php echo $image_thumb[0]; ?>" alt="<?php  echo $projects['title']; ?>" />
                                                </a>
                                                <a href="<?php echo $projects['feather_projects_url']; ?>" class="more-link-wrapper">
                                                    <span class="more-info-link">
                                                        <span>+</span>
                                                    </span>
                                                </a>
                                            </figure>
                                        </div>
                                        <span class="facility-title"><?php echo $projects['title']; ?></span>
                                    </div>
                                <?php else : ?>
                                    <div class="facility-item">
                                        <div class="shape-square lightblue">
                                            <figure>
                                                <img src="<?php echo $image_thumb[0]; ?>" alt="<?php  echo $projects['title']; ?>" />
                                                <a href="<?php echo $projects['feather_projects_url']; ?>" class="more-link-wrapper"><span class="more-info-link"><span>+</span></span></a>
                                            </figure>
                                        </div>
                                        <span class="facility-title"><?php  echo $projects['title']; ?></span>
                                    </div>
                                <?php endif; ?>
                            </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>

                </div>
            </div>
        </section>
    <?php endif; ?>

    <?php if (tc_get_option('featured_apps') != 'off') : ?>
        <section id="about" class="featured-apps">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2 text-center">
                        <h2 class="section-title bottom-border white"><?php echo tc_get_option('featured_apps_title'); ?></h2>
                        <p class="text-faded"><?php echo tc_get_option('featured_apps_description'); ?></p>
                        <div class="downloads">
                        <?php 
                            $img_button = tc_get_option('img_btn_download_app_list');
                            $i = 1;
                            if($img_button) : 
                            foreach($img_button as $img_btn) : 
                        ?>
                            <a href="<?php echo  $img_btn['url_img_button_download'] ?>" title="<?php echo $img_btn['title']; ?>" target="_blank"><img src="<?php echo $img_btn['img_button_download']; ?>" alt="<?php echo $img_btn['title']; ?>"></a>
                            <?php 
                            if($i == 4) {
                                break;
                            }
                            $i++;
                            endforeach;
                            endif;
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endif; ?>

    <?php  if (tc_get_option('featured_works') != 'off') : ?>
        <section id="features" class="features-section section-content">
            <div class="container">   
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2 class="section-title bottom-border"><?php echo tc_get_option('featured_works_title'); ?></h2>
                        <p class="intro-dis"><?php echo tc_get_option('home_featured_works_short_desc'); ?></p>
                    </div>
                </div>
                <div class="row">
                    <div class="wrapper-service-slide">
                        <div id="service-slide" class="owl-carousel owl-theme">
                            <?php
                                $blocks = tc_get_option('feature_action_4_col_list');
                                $i = 1;
                                if ($blocks) :
                                foreach($blocks as $bl) :
                            ?>
                            <div class="item feature-service-box">
                                <div class="service-box-shape">
                                   <a href="<?php echo $bl['block_link']; ?>">
                                       <i class="<?php echo $bl['feature_action_awesome_font_icons']; ?>"></i>
                                   </a>
                                </div>
                                <h3><a href="<?php echo $bl['block_link']; ?>"><?php echo $bl['title']; ?></a></h3>
                                <p><?php echo $bl['block_short_desc']; ?></p>                           
                                <a href="<?php echo $bl['block_link']; ?>" class="rm-slide-services">Continue Reading</a>
                            </div>
                            <?php
                                if ($i == 8) {
                                    break;
                                }
                                 $i++;
                                endforeach; 
                                endif;
                            ?>
                       </div>
                       <div class="customNavigation">
                            <div class="owl-prev prev">
                                <i class="fa fa-angle-left"></i>
                            </div>
                            <div class="owl-next next">
                                <i class="fa fa-angle-right"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endif; ?>    

    <?php if ( tc_get_option('Our_blog_section') != 'off' ) : ?>
        <section id="our-blogs" class="latest-blogs-section no-padding">           
            <?php tcc_home_blog_post(); ?>
        </section>
    <?php endif; ?>    

    <?php if (tc_get_option('section_team_member') != 'off') : ?>
        <section id="site-member" class="site-member">
             <?php tcc_feature_team_member();  ?>
        </section>
    <?php endif; ?>
    
    <?php if(tc_get_option('enable_sponsor') != 'off') : ?>
        <section id="sponsor" class="site-sponsor sponsor">
           <?php tcc_sponsor_profile(); ?>
        </section>
    <?php endif; ?>