<?php
/**
 * Default post render
 *
 * @package StartBiz
 */
?>

	<article id="post-<?php the_ID(); ?>" <?php post_class('entry-content post-item clearfix'); ?> >
		<header class="entry-header clearboth">
				<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
				<?php if (tc_get_option('blog_enable_post_meta_info') != 'off' ) : ?>
					<div class="entry-meta">
						<?php startbiz_posted_on(); ?>
					</div><!-- .entry-meta -->
				<?php endif; ?>				
		</header><!-- .entry-header -->

		<div class="entry-blog clearboth">
			<?php if ( has_post_thumbnail() && ot_get_option('post_text') != 'content') : ?>
					<div class="post-thumbnail">
						<a href="<?php the_permalink() ?>" rel="bookmark"><?php the_post_thumbnail('post-thumbnails-list'); ?></a>
					</div>
			<?php endif; ?>
			
			<div class="post-excerpt <?php if ( !has_post_thumbnail() || ot_get_option('post_text') == 'content') : ?> full-width-content <?php endif; ?>">
				<?php 
					if ( ot_get_option('post_text') == 'content' ) :
						the_content(); 
					else :
						the_excerpt();
					endif;
				?>
				<?php if (tc_get_option('enable_more_button') != 'off') : ?>
					<a href="<?php the_permalink() ?>" class="more-link" >Read more</a>
				<?php endif; ?>
			</div><!-- .entry-excerpt -->
		</div>
		
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'startbiz' ),
				'after'  => '</div>',
			) );
		?>

	</article><!-- #post-## -->