<?php
/**
 * The template for displaying all single posts.
 *
 * @package StartBiz
 */

get_header(); ?>
<div class="container">
	<div class="row padding-bottom30">
		<div class="col-lg-8 col-md-8 col-sm-12 content-left">	
			<div id="primary" class="content-area">
				<main id="main" class="site-main clearfix" role="main">

					<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', 'single' ); ?>


					<?php if ( ( tc_get_option( 'author-bio' ) != 'off' ) && get_the_author_meta( 'description' ) ): ?>
					<div class="author-bio">
						<div class="bio-avatar"><?php echo get_avatar(get_the_author_meta('user_email'),'128'); ?></div>
						<p class="bio-name"><?php _e('About Author:', 'bizcorp' ); ?><?php the_author_posts_link(); ?></p>
						<p class="bio-desc"><?php the_author_meta('description'); ?></p>
						<div class="clear"></div>
					</div>
					<?php endif; ?>

					<?php if ( tc_get_option( 'post-nav' ) != 'off'){ tcc_the_post_navigation(); } ?>

					<?php
					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
					?>

					<?php endwhile; // end of the loop. ?>

				</main><!-- #main -->
			</div><!-- #primary -->
		</div>
	<!-- end colume 8 -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>
