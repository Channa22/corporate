(function($) {
	var themecountry = {
		initAll: function() {
			this.selectExportData();
			this.showHideOptionSocialButtons();
			this.showHideOptionSlideshow();
			this.showHideTypeSlideshow();
			this.showHideShowExcerpt();
			this.showHideShowMetaInfo();
			this.showHideRelatedPosts();
			this.saveTypoGrapography();
			this.preventNumberofSlide();
			this.showHideFirstBanner();
			this.showHideAfterBanner();
			this.showHideFeaturedWorks();
			this.showHidePortfolio();
			this.showHideSecondBanner();
			this.showHideOurBlog();
			this.showHideHomeSponsor();
			this.showHideSocialLink();
			this.showHideSignUp();
			this.optionBannerTitle();

		},
		optionBannerTitle: function() {
			$('ul.option-tree-sortable li select').change(function() {
				var $this = $(this);
				$this.each(function(){
					var $values = $this.find('option:selected').text();
					$this.parents('ul li').find('.ui-sortable-handle').text($values);
				});
			});

			$('ul.option-tree-sortable li select').each(function(){
				var $this = $(this);
				var $values = $this.find('option:selected').text();
				$this.parents('ul li').find('.ui-sortable-handle').text($values);
			});
		},

		showHideSignUp: function() {
			
			if ( $('#setting_btn_sign_up_on_off .on-off-switch input[type="radio"]:first-child').is(':checked') ) {
				$('#setting_btn_sign_up_title, #setting_url_link_sign_up').show();
			} else {
				$('#setting_btn_sign_up_title, #setting_url_link_sign_up').hide();
			}			
			
			$('#setting_btn_sign_up_on_off .on-off-switch label').click(function() {
				var labelText = $(this).text();
				if(labelText == 'On') {
					$('#setting_btn_sign_up_title, #setting_url_link_sign_up').show();
				} else if (labelText == 'Off') {
					$('#setting_btn_sign_up_title, #setting_url_link_sign_up').hide();
				}
			});
			
		},

		showHideSocialLink: function() {

			if ( $('#setting_social_link_enable .on-off-switch input[type="radio"]:first-child').is(':checked') ) {
				$('#setting_social_links').show();
			} else {
				$('#setting_social_links').hide();
			}

			$('#setting_social_link_enable .on-off-switch label').click(function() {
				var labelText = $(this).text();
				if(labelText == 'On') {
					$('#setting_social_links').show();
				} else if (labelText == 'Off') {
					$('#setting_social_links').hide();
				}
			});

		},

		showHideFirstBanner: function() {
			
			if ( $('#setting_first_banner .on-off-switch input[type="radio"]:first-child').is(':checked') ) {
				$('#setting_home_first_banner_title, #setting_home_first_banner_color, #setting_home_first_banner_link, #setting_home_first_banner_img, #setting_home_first_banner_img_mobile, #setting_home_first_banner_short_desc').show();
			} else {
				$('#setting_home_first_banner_title, #setting_home_first_banner_color, #setting_home_first_banner_link, #setting_home_first_banner_img, #setting_home_first_banner_img_mobile, #setting_home_first_banner_short_desc').hide();
			}

			$('#setting_first_banner .on-off-switch label').click(function() {
				var labelText = $(this).text();
				if(labelText == 'On') {
					$('#setting_home_first_banner_title, #setting_home_first_banner_color, #setting_home_first_banner_link, #setting_home_first_banner_img, #setting_home_first_banner_img_mobile, #setting_home_first_banner_short_desc').show();
				} else if (labelText == 'Off') {
					$('#setting_home_first_banner_title, #setting_home_first_banner_color, #setting_home_first_banner_link, #setting_home_first_banner_img, #setting_home_first_banner_img_mobile, #setting_home_first_banner_short_desc').hide();
				}
			});
			
		},

		showHideAfterBanner: function() {
			
			if ( $('#setting_featured_services .on-off-switch input[type="radio"]:first-child').is(':checked') ) {
				$('#setting_featured_services_section_title, #setting_home_modules_section_short_desc').show();
			} else {
				$('#setting_featured_services_section_title, #setting_home_modules_section_short_desc').hide();
			}

			$('#setting_featured_services .on-off-switch label').click(function() {
				var labelText = $(this).text();
				if(labelText == 'On') {
					$('#setting_featured_services_section_title, #setting_home_modules_section_short_desc').show();
				} else if (labelText == 'Off') {
					$('#setting_featured_services_section_title, #setting_home_modules_section_short_desc').hide();
				}
			});
			
		},

		showHideFeaturedWorks: function() {
			
			if ( $('#setting_featured_works .on-off-switch input[type="radio"]:first-child').is(':checked') ) {
				$('#setting_home_featured_works_title, #setting_home_featured_works_short_desc, #setting_home_3_col_list').show();
			} else {
				$('#setting_home_featured_works_title, #setting_home_featured_works_short_desc, #setting_home_3_col_list').hide();
			}

			$('#setting_featured_works .on-off-switch label').click(function() {
				var labelText = $(this).text();
				if(labelText == 'On') {
					$('#setting_home_featured_works_title, #setting_home_featured_works_short_desc, #setting_home_3_col_list').show();
				} else if (labelText == 'Off') {
					$('#setting_home_featured_works_title, #setting_home_featured_works_short_desc, #setting_home_3_col_list').hide();
				}
			});
			
		},

		showHidePortfolio: function() {
			
			if ( $('#setting_portfolio_section .on-off-switch input[type="radio"]:first-child').is(':checked') ) {
				$('#setting_home_portfolio_title, #setting_home_portfolio_show_limit, #setting_home_portfolio_bg_color, #setting_home_portfolio_link_color, #setting_home_portfolio_border_title_color').show();
			} else {
				$('#setting_home_portfolio_title, #setting_home_portfolio_show_limit, #setting_home_portfolio_bg_color, #setting_home_portfolio_link_color, #setting_home_portfolio_border_title_color').hide();
			}

			$('#setting_portfolio_section .on-off-switch label').click(function() {
				var labelText = $(this).text();
				if(labelText == 'On') {
					$('#setting_home_portfolio_title, #setting_home_portfolio_show_limit, #setting_home_portfolio_bg_color, #setting_home_portfolio_link_color, #setting_home_portfolio_border_title_color, #setting_home_portfolio_more_link, #setting_home_portfolio_more_link, #setting_home_portfolio_more_link_text').show();

				} else if (labelText == 'Off') {
					$('#setting_home_portfolio_title, #setting_home_portfolio_show_limit, #setting_home_portfolio_bg_color, #setting_home_portfolio_link_color, #setting_home_portfolio_border_title_color, #setting_portfolio_section_more_link, #setting_home_portfolio_more_link, #setting_portfolio_section_more_link, #setting_home_portfolio_more_link, #setting_home_portfolio_more_link_text').hide();
				}
			});
		},

		showHideSecondBanner: function() {
			
			if ( $('#setting_second_banner .on-off-switch input[type="radio"]:first-child').is(':checked') ) {
				$('#setting_home_second_banner_title, #setting_home_second_banner_color, #setting_home_second_bg_color, #setting_home_second_banner_link, #setting_home_second_banner_img, #setting_home_second_banner_short_desc').show();
			} else {
				$('#setting_home_second_banner_title, #setting_home_second_banner_color, #setting_home_second_bg_color, #setting_home_second_banner_link, #setting_home_second_banner_img, #setting_home_second_banner_short_desc').hide();
			}

			$('#setting_second_banner .on-off-switch label').click(function() {
				var labelText = $(this).text();
				if(labelText == 'On') {
					$('#setting_home_second_banner_title, #setting_home_second_banner_color, #setting_home_second_bg_color, #setting_home_second_banner_link, #setting_home_second_banner_img, #setting_home_second_banner_short_desc').show();
				} else if (labelText == 'Off') {
					$('#setting_home_second_banner_title, #setting_home_second_banner_color, #setting_home_second_bg_color, #setting_home_second_banner_link, #setting_home_second_banner_img, #setting_home_second_banner_short_desc').hide();
				}
			});
			
		},

		showHideOurBlog: function() {
			
			if ( $('#setting_Our_blog_section .on-off-switch input[type="radio"]:first-child').is(':checked') ) {
				$('#setting_home_blog_show_limit, #setting_home_blog_bg_color, #setting_bg_color_in_blog').show();
			} else {
				$('#setting_home_blog_show_limit, #setting_home_blog_bg_color, #setting_bg_color_in_blog').hide();
			}

			$('#setting_Our_blog_section .on-off-switch label').click(function() {
				var labelText = $(this).text();
				if(labelText == 'On') {
					$('#setting_home_blog_show_limit, #setting_home_blog_bg_color, #setting_home_blog_more_link_text, #setting_home_blog_more_link, #setting_bg_color_in_blog').show();
				} else if (labelText == 'Off') {
					$('#setting_home_blog_show_limit, #setting_home_blog_bg_color, #setting_Our_blog_section_more_link, #setting_home_blog_more_link_text, #setting_home_blog_more_link, #setting_bg_color_in_blog').hide();
				}
			});
		},

		showHideHomeSponsor: function() {
			
			
			
		},

		preventNumberofSlide: function() {
			$('#section_option_home_slider #number-of-post').blur(function() {
				if( $(this).val() == '0' ) {
					alert('Not allow 0 value!');
					$(this).val('5');
				}
			});
		},

		showHideRelatedPosts: function() {
			if ( $('#post-related-0').is(':checked') ) {

				$('#setting_related-posts-taxonomy, #setting_number-related, #setting_display-related-posts').show();
			} else {

				$('#setting_related-posts-taxonomy, #setting_number-related, #setting_display-related-posts').hide();
			}
		
			$('#setting_post-related .on-off-switch label').click(function() {
				var labelText = $(this).text();
				if(labelText == 'On') {
					$('#setting_related-posts-taxonomy, #setting_number-related, #setting_display-related-posts').show();
				} else if (labelText == 'Off') {
					$('#setting_related-posts-taxonomy, #setting_number-related, #setting_display-related-posts').hide();
				}
			});
		},

		showHideShowMetaInfo: function() {
			themecountry.customizeShowHideShowMetaInfo();

			$('#setting_enable-post-meta-info .radio').click(function() {

				themecountry.customizeShowHideShowMetaInfo();
			});
		},
		customizeShowHideShowMetaInfo: function() {
			$('#setting_enable-post-meta-info').find('input[type="radio"]').each(function(){
				if ( $(this).is(':checked') && $(this).attr('id') == 'enable-post-meta-info-0' ) {

					$('#setting_post-meta-info').show();
				} else if ( $(this).is(':checked') && $(this).attr('id') == 'enable-post-meta-info-1' ) {

					$('#setting_post-meta-info').hide();
				}
			});
		},

		showHideShowExcerpt: function() {
			themecountry.customizeShowHideShowExcerpt();

			$('#setting_post_text .radio').click(function() {

				themecountry.customizeShowHideShowExcerpt();
			});
		},
		customizeShowHideShowExcerpt: function() {
			$('#setting_post_text').find('input[type="radio"]').each(function(){
				if ( $(this).is(':checked') && $(this).attr('id') == 'post_text-0' ) {

					$('#setting_excerpt_length, #setting_excerpt_more, #setting_home-post-read-more').show();
				} else if ( $(this).is(':checked') && $(this).attr('id') == 'post_text-1' ) {

					$('#setting_excerpt_length, #setting_excerpt_more, #setting_home-post-read-more').hide();
				}
			});
		},
		showHideTypeSlideshow: function() {
			if ( $('#type-of-slider-0').is(':checked') ) {

				$('#setting_customize_slider_option').hide();
			} else {

				$('#setting_category-slide-option').hide();
			}
		
			$('#setting_type-of-slider .on-off-switch label').click(function() {
				var labelText = $(this).text();
				if(labelText == 'On') {
					$('#setting_customize_slider_option').hide();
					$('#setting_category-slide-option').show();
				} else if (labelText == 'Off') {
					$('#setting_category-slide-option').hide();
					$('#setting_customize_slider_option').show();
				}
			});
		},
		showHideOptionSlideshow: function() {
			if ( $('#enable-home-slider-0').is(':checked') ) {
				$('#setting_number-of-post, #setting_type-of-slider, #setting_category-slide-option, #setting_customize_slider_option').show();
			} else {
				$('#setting_number-of-post, #setting_type-of-slider, #setting_category-slide-option, #setting_customize_slider_option').hide();
			}
		
			$('#setting_enable-home-slider .on-off-switch label').click(function() {
				var labelText = $(this).text();
				if(labelText == 'On') {
					$('#setting_number-of-post, #setting_type-of-slider, #setting_category-slide-option').show();
					$('#type-of-slider-0').prop('checked', true);
				} else if (labelText == 'Off') {
					$('#setting_number-of-post, #setting_type-of-slider, #setting_category-slide-option,#setting_customize_slider_option').hide();
				}
			});
		},
		showHideOptionSocialButtons: function() {
			if ( $('#social-button-enable-0').is(':checked') ) {
				$('#setting_twitter-username, #setting_social-sharing-pos, #setting_social-twitter, #setting_social-twitter, #setting_social-facebook, #setting_social-google-plus, #setting_social-google-plus, #setting_social-linkedin, #setting_social-pinterest').show();
			} else {
				$('#setting_twitter-username, #setting_social-sharing-pos, #setting_social-twitter, #setting_social-twitter, #setting_social-facebook, #setting_social-google-plus, #setting_social-google-plus, #setting_social-linkedin, #setting_social-pinterest').hide();
			}
		
			$('#setting_social-button-enable .on-off-switch label').click(function() {
				var labelText = $(this).text();
					
				if(labelText == 'On') {
					$('#setting_social-sharing-pos p:first-child input[type="checkbox"]').prop('checked', true);
					$('#setting_twitter-username, #setting_social-sharing-pos, #setting_social-twitter, #setting_social-twitter, #setting_social-facebook, #setting_social-google-plus, #setting_social-google-plus, #setting_social-linkedin, #setting_social-pinterest').show();
				} else if (labelText == 'Off') {
					$('#setting_twitter-username, #setting_social-sharing-pos, #setting_social-twitter, #setting_social-twitter, #setting_social-facebook, #setting_social-google-plus, #setting_social-google-plus, #setting_social-linkedin, #setting_social-pinterest').hide();
				}
			});
		},
		
		selectExportData: function() {
			$("#tab_option_import_export").on('click', function(){
				$('.tc-export-data').focus().select();
			});

			$('.tc-export-data').on('focus', function(){
				$(this).select()
			});
		},
		
		saveTypoGrapography: function() {
			
			$("#option-tree-sub-header > button, #option-tree-settings-api > .option-tree-ui-buttons > button").on('click', function(e) {
				e.preventDefault();

				var collectionData = new Array();
			i=0;
      
			$("#google_typography").find(".collections .collection").each(function() {
        
				/*if(showLoading != false) {
					collection.find(".save_collection").addClass("saving").html("Saving...");
				}*/

				previewText  = $(this).find(".preview_text").val();
				previewColor = $(this).find(".preview_color li.current a").attr("class");
				fontFamily   = $(this).find(".font_family").val();
				fontVariant  = $(this).find(".font_variant").val();
				fontSize     = $(this).find(".font_size").val();
				fontColor    = $(this).find(".font_color").val();
				cssSelectors = $(this).find(".css_selectors").val();
				isDefault    = $(this).attr("data-default");
        
				collectionData[i] = {
					uid           : i+1,
					preview_text  : previewText,
					preview_color : previewColor,
					font_family   : fontFamily,
					font_variant  : fontVariant, 
					font_size     : fontSize,
					font_color    : fontColor,
					css_selectors : cssSelectors,
					default       : isDefault
				};
  
				i++;
        
			});

			$.ajax({
	        	url: ajaxurl,
		        type:'POST',
		        data: {  'action' : 'save_user_fonts',  'collections' : collectionData },
		        beforeSend : function() {
		        	$('.option-tree-ui-buttons .spinner, #option-tree-sub-header .spinner').show();
		        }
		        
		    }).done(function(html) {
		    	setTimeout(function() {
		    		$('.option-tree-ui-buttons .spinner, #option-tree-sub-header .spinner').hide();
		    	}, 4000);
		    	$("#option-tree-settings-api").submit();
		    });
			//saveCollections(collection, container);
					
			});
		}
	}

	$(document).ready(function() {
        themecountry.initAll();
    });
})(jQuery);