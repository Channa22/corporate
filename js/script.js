(function ($) {
	"use strict";
	var Testjquery = {
		init: function(){
			this.backtoTop();
			this.dropdownMenu();
			this.slideshow();
			this.mobileMenu();
			this.prettyPhoto();
			this.stickyMenu();
			this.shareSocailLink();
			this.serviceSlide();
			this.verticalAlign();	
		},
		customVerticalAlign: function() {
			var screenSize = $( document ).width();
			if ( $('.mid-align').length) {
				$('.mid-align').each(function(){
					var boxHeight = ( $(this).innerHeight() );
					$(this).css({'position': 'absolute', 'top': '50%'});
					$(this).css( { "margin-top" : -boxHeight/2 } );
				});
			}
		},
		verticalAlign: function() {
			Testjquery.customVerticalAlign();
			$(window).resize(function() {
				Testjquery.customVerticalAlign();
			});
		},

		serviceSlide: function() {			
			var owl = $("#service-slide");			
			owl.owlCarousel({
				items : 3, //10 items above 1000px browser width									      
				itemsDesktop : [1000,3], //5 items between 1000px and 901px
				itemsDesktopSmall : [900,2], // betweem 900px and 601px
				itemsTablet: [600,2], //2 items between 600 and 0
				itemsMobile : [600,1] // itemsMobile disabled - inherit from itemsTablet option
			});					 
			// Custom Navigation Events
			$(".next").click(function(){
				owl.trigger('owl.next');
			})
			$(".prev").click(function(){
				owl.trigger('owl.prev');
			})
			$(".play").click(function(){
				owl.trigger('owl.play',5000);
			})
			$(".stop").click(function(){
				owl.trigger('owl.stop');
			})
		},
		slideshow : function() {
			var owl = $("#owl-demo");
			owl.owlCarousel({
				items:8,
				itemsDesktop : [1000,5], //5 items between 1000px and 901px
				itemsDesktopSmall : [900,3], // betweem 900px and 601px
				itemsTablet: [600,2], //2 items between 600 and 0
				itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
			});
			owl.trigger('owl.play',2000);
			// Custom Navigation Events
			$(".next").click(function(){
				owl.trigger('owl.next');
			})
			$(".prev").click(function(){
				owl.trigger('owl.prev');
			})
			$(".play").click(function(){
				owl.trigger('owl.play',1000);
			})
			$(".stop").click(function(){
				owl.trigger('owl.stop');
			})
		},
		shareSocailLink: function() {
			var scrollDes = 'html,body';
			if(navigator.userAgent.match(/opera/i)) {
				scrollDes = 'html';
			}
			//Show, Hide
			$(window).scroll(function() { 
				if($(this).scrollTop() > 130 ) {
					$('.sharing-top-float').fadeIn();
				} else {
					$('.sharing-top-float').fadeOut();
				}
			});			
		},
		stickyMenu: function() {
			var self = this;

			var catcher = $('#catcher'),
				sticky  = $('#sticky'),
				bodyTop = $('body').offset().top;
			if ( sticky.length ) {
				$(window).scroll(function() {
					self.stickThatMenu(sticky,catcher,bodyTop);
				});
				$(window).resize(function() {
					self.stickThatMenu(sticky,catcher,bodyTop);
				});
			}
		},
		isScrolledTo: function(elem,top) {
			var docViewTop = $(window).scrollTop();
			var docViewBottom = docViewTop + $(window).height();

			var elemTop = $(elem).offset().top - top;
			var elemBottom = elemTop + $(elem).height();

			return ((elemTop <= docViewTop));
		},
		stickThatMenu: function(sticky,catcher,top) {
			var screenWidth = $(window).width();	
			if(screenWidth > 1025) {	 	
				var self = this;
				//var navOffset = $(".wrapper").offset().top;
				//var scrollPos = $(window).scrollTop();
			
				if(self.isScrolledTo(sticky,top)) {
					sticky.addClass('navbar-fixed-top');
					catcher.height(sticky.height());
				}					
				var stopHeight = catcher.offset().top;
				if ( stopHeight > sticky.offset().top - 1 ) {
					sticky.removeClass('navbar-fixed-top');
					catcher.height(0);
				}
			}
		},
		backtoTop : function() {
			$('.back-to-top').click(function(e){
				e.preventDefault();
				$('html, body').animate({scrollTop : 0}, 800);
				return false;
			});
			$(document).scroll ( function() {
				var topPositionScrollBar = $(document).scrollTop();
				if ( topPositionScrollBar < "400" ) {
					$(".back-to-top").fadeOut();
				} else {
					$(".back-to-top").fadeIn();
				}
			});
		},
		dropdownMenu : function() {
			$('ul.nav li.dropdown').hover(function() {
			    $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
			}, function() {
				$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
			});
		},		
		mobileMenu : function() {
			var screenWidth = $(window).width();
			$( window ).resize(function() {
				var screenResizeWidth = $(window).width();
				if ( screenResizeWidth > 1024 ) {
					$('#masthead .menu-item-has-children > a i').remove();
				}
			  	console.log(screenResizeWidth);
			});
			if(screenWidth < 1025) {
				$('#masthead .menu-item-has-children > a').append('<i class="fa arrow-sub-menu fa-caret-right"></i>');
			}			
			var $top_menu = $('.primary-navigation');
			var $secondary_menu = $('.secondary-navigation');
			var $first_menu = '';
			var $second_menu = '';

			if ($top_menu.length == 0 && $secondary_menu.length == 0) {
				return;
			} else {
				if ($top_menu.length) {
					$first_menu = $top_menu;
					$second_menu = $secondary_menu;
				} else {
					$first_menu = $secondary_menu;
				}
			}

			var menu_wrapper = $first_menu
			.clone().attr('class', 'mobile-menu')
			.wrap('<div id="mobile-menu-wrapper" class="mobile-only"></div>').parent().hide()
			.appendTo('body');
			
			// Add items from the other menu
			if ($second_menu.length) {
				$second_menu.find('ul.menu').clone().appendTo('#mobile-menu-wrapper .mobile-menu');
			}
			
			$('.toggle-mobile-menu').click(function(e) {
				e.preventDefault();
				e.stopPropagation();		
				$('#mobile-menu-wrapper').show(); // only required once
				$('body').toggleClass('mobile-menu-active');
			});

			$('#page').click(function(e) {
				e.stopPropagation();

				if ($('body').hasClass('mobile-menu-active')) {
					$('body').removeClass('mobile-menu-active');
				}
				if($('.menu-item-has-children .arrow-sub-menu').hasClass('fa-caret-down')) {
					$('.menu-item-has-children .arrow-sub-menu').removeClass('fa-caret-down').addClass('fa-caret-right');
					$('.arrow-sub-menu').parent().next().removeAttr('style');
				}

		
			});

			$('.arrow-sub-menu').on('click', function(e) {
				e.preventDefault();
				e.stopPropagation();

				var active = $(this).hasClass('fa-caret-down');
				if(!active) {
					$(this).parent().next().css({'display' : 'block'});
					$(this).removeClass('fa-caret-right').addClass('fa-caret-down');
				} else {
					$(this).parent().next().css({'display' : 'none'});
					$(this).removeClass('fa-caret-down').addClass('fa-caret-right');
				}
				
			});
		},

		// end function mobileMenu
			prettyPhoto : function() {					
				if ($(".image-overlay a.prettyPhoto").length) {
					$(".image-overlay a.prettyPhoto").prettyPhoto();
				}
			}
		
		// end function prettyPhotos

		

	}
	$(document).ready(function(){
		Testjquery.init();
	});
})(jQuery);