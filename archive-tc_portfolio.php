<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Theme Country
 */

get_header(); 
?>
<div class="container">
	<div class="row">
		<div class="col-lg-12 text-center">
			<div class="head-section">
    			<h1 class="portfolio-title section-title bottom-border">Portfolio</h1>
    			<p class="text-muted">Our portfolio describe our action from company have started</p> 			
			</div>
		</div>		
	</div>	
</div>
<div id="portfolio-page" class="portfolio-inner">
	<div class="container">
		<div class="tc-portfolio row">

			<?php if ( have_posts() ) : ?>

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php
						/* Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'content', 'portfolio' );						
						
					?>

				<?php endwhile; ?>

				<?php the_posts_navigation(); ?>

			<?php else : ?>

				<?php get_template_part( 'content', 'none' ); ?>

			<?php endif; ?>

		</div>
	</div>
</div>

<?php get_footer(); ?>