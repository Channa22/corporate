# Copyright (C) 2015 Automattic
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: _s 1.0.0\n"
"Report-Msgid-Bugs-To: http://wordpress.org/tags/_s\n"
"POT-Creation-Date: 2015-01-06 02:24:02+00:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2015-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"

#: 404.php:12
msgid "Oops! That page can&rsquo;t be found."
msgstr ""

#: 404.php:26
msgid "It looks like nothing was found at this location. Maybe try one of the links below or a search?"
msgstr ""

#: comments.php:28
msgctxt "comments title"
msgid "One thought on &ldquo;%2$s&rdquo;"
msgid_plural "%1$s thoughts on &ldquo;%2$s&rdquo;"
msgstr[0] ""
msgstr[1] ""

#: comments.php:38 comments.php:59
msgid "Older Comments"
msgstr ""

#: comments.php:39 comments.php:60
msgid "Newer Comments"
msgstr ""

#: comments.php:72
msgid "Comments are closed."
msgstr ""

#: comments.php:56
msgid "Comment navigation"
msgstr ""

#: content-home.php:10 content-home.php:59
msgid "Learn More"
msgstr ""

#: content-none.php:13
msgid "Nothing Found"
msgstr ""

#: content-none.php:19
msgid "Ready to publish your first post? <a href=\"%1$s\">Get started here</a>."
msgstr ""

#: content-none.php:23
msgid "Sorry, but nothing matched your search terms. Please try again with some different keywords."
msgstr ""

#: content-none.php:28
msgid "It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help."
msgstr ""

#: content-page.php:17 content-portfoliosingle.php:39 content-single.php:35 content.php:35
msgid "Pages:"
msgstr ""

#: content-page.php:24
msgid "Edit"
msgstr ""

#: functions.php:89
msgid "Primary Menu"
msgstr ""

#: functions.php:127
msgid "Sidebar"
msgstr ""

#: functions.php:136
msgid "Footer Sidebar 1"
msgstr ""

#: functions.php:146
msgid "Footer Sidebar 2"
msgstr ""

#: functions.php:156
msgid "Footer Sidebar 3"
msgstr ""

#: functions.php:165
msgid "Footer Sidebar 4"
msgstr ""

#: header.php:22
msgid "Skip to content"
msgstr ""

#: header.php:31
msgid "Main Navigation"
msgstr ""

#: page-contact.php:36
msgid "Name"
msgstr ""

#: page-contact.php:37
msgid "E-Mail Address"
msgstr ""

#: page-contact.php:38
msgid "Type your message"
msgstr ""

#: page-contact.php:39
msgid "Send Message"
msgstr ""

#: search.php:16
msgid "Search Results for: %s"
msgstr ""

#: single.php:21 single-tc_portfolio.php:21
msgid "About Author"
msgstr ""

#: Theme Option

#: inc/extras.php:114
msgid "Page %s"
msgstr ""

#: inc/extras.php:252
msgid "Learn more"
msgstr ""

#: inc/extras.php:309
msgid "View More Posts"
msgstr ""

#: inc/extras.php:439
msgid "Related Projects"
msgstr ""

#: inc/extras.php:439 inc/extras.php:480
msgid "Related Projects"
msgstr ""

#: inc/extras.php:439 inc/extras.php:484
msgid "Related Posts"
msgstr ""

#: inc/extras.php:518
msgid "Home"
msgstr ""

#: inc/extras.php:518
msgid "Home"
msgstr ""

#: inc/extras.php:537
msgid "Archive by category"
msgstr ""

#: inc/extras.php:540
msgid "Search results for"
msgstr ""

#: inc/extras.php:598
msgid "Posts tagged"
msgstr ""

#: inc/extras.php:603
msgid "Articles posted by"
msgstr ""

#: inc/extras.php:606
msgid "Error 404"
msgstr ""

#: inc/extras.php:611
msgid "Page"
msgstr ""

#: inc/extras.php:639
msgid "From here, you can control the fonts used on your site. You can choose from 17 standard font sets, or from the Google Fonts Library containing 600+ fonts."
msgstr ""

#: inc/import-export-theme-option.php:23
msgid "Theme Options Backup/Restore"
msgstr ""

#: inc/import-export-theme-option.php:24
msgid "Options Backup"
msgstr ""

#: inc/import-export-theme-option.php:29
msgid "Options updated."
msgstr ""

#: inc/import-export-theme-option.php:30
msgid "Options reset."
msgstr ""

#: inc/import-export-theme-option.php:31
msgid "Save Changes"
msgstr ""

#: inc/import-export-theme-option.php:38
msgid "Import/Export"
msgstr ""

#: inc/import-export-theme-option.php:44 inc/import-export-theme-option.php:56
msgid "Import Theme Options"
msgstr ""

#: inc/import-export-theme-option.php:45 inc/import-export-theme-option.php:57
msgid "Theme Options"
msgstr ""

#: inc/import-export-theme-option.php:98
msgid "Only after you've imported the Settings should you try and update your Theme Options."
msgstr ""

#: inc/import-export-theme-option.php:100
msgid "To import your Theme Options copy and paste what appears to be a random string of alpha numeric characters into this textarea and press the 'Import Theme Options' button."
msgstr ""

#: inc/import-export-theme-option.php:102
msgid "Import Theme Options"
msgstr ""

#: inc/portfolio.php:12 inc/portfolio.php:13 inc/portfolio.php:14
msgid "Portfolio"
msgstr ""

#: inc/portfolio.php:15
msgid "Portfolio Item"
msgstr ""

#: inc/portfolio.php:16
msgid "All Items"
msgstr ""

#: inc/portfolio.php:17
msgid "Add New"
msgstr ""

#: inc/portfolio.php:18
msgid "Add New Item"
msgstr ""

#: inc/portfolio.php:19
msgid "Edit Item"
msgstr ""

#: inc/portfolio.php:20
msgid "New Item"
msgstr ""

#: inc/portfolio.php:21
msgid "View Item"
msgstr ""

#: inc/portfolio.php:22
msgid "Search Items"
msgstr ""

#: inc/portfolio.php:23
msgid "No items found."
msgstr ""

#: inc/portfolio.php:24
msgid "No items found in Trash."
msgstr ""

#: inc/portfolio.php:25
msgid "Parent Items"
msgstr ""

#: inc/portfolio.php:64
msgid "Item updated."
msgstr ""

#: inc/portfolio.php:65
msgid "Custom field updated."
msgstr ""

#: inc/portfolio.php:66
msgid "Custom field deleted."
msgstr ""

#: inc/portfolio.php:67
msgid "Item updated."
msgstr ""

#: inc/portfolio.php:68
msgid "Item restored to revision from %s"
msgstr ""

#: inc/portfolio.php:69
msgid "Item published."
msgstr ""

#: inc/portfolio.php:70
msgid "Item saved."
msgstr ""

#: inc/portfolio.php:71
msgid "Item submitted."
msgstr ""

#: inc/portfolio.php:72
msgid "Item scheduled for"
msgstr ""

#: inc/portfolio.php:72
msgid "%1$s"
msgstr ""

#: inc/portfolio.php:73
msgid "Item draft updated."
msgstr ""

#: inc/portfolio.php:84
msgid "View item"
msgstr ""

#: inc/portfolio.php:85
msgid "Preview item"
msgstr ""

#: inc/portfolio.php:125
msgid "Categories"
msgstr ""

#: inc/portfolio.php:126
msgid "Category"
msgstr ""

#: inc/portfolio.php:127
msgid "Categories"
msgstr ""

#: inc/portfolio.php:128
msgid "All Categories"
msgstr ""

#: inc/portfolio.php:129
msgid "Edit Category"
msgstr ""

#: inc/portfolio.php:130
msgid "View Category"
msgstr ""

#: inc/portfolio.php:131
msgid "Update Category"
msgstr ""

#: inc/portfolio.php:132
msgid "Add New Category"
msgstr ""

#: inc/portfolio.php:133
msgid "New Category Name"
msgstr ""

#: inc/portfolio.php:134
msgid "Parent Category"
msgstr ""

#: inc/portfolio.php:135
msgid "Parent Category:"
msgstr ""

#: inc/portfolio.php:136
msgid "Search Categories"
msgstr ""

#: inc/template-tags.php:80
msgid "Previous page"
msgstr ""

#: inc/template-tags.php:81
msgid "Next page"
msgstr ""

#: inc/template-tags.php:82
msgid "Page"
msgstr ""

#: inc/template-tags.php:90
msgid "Posts navigation"
msgstr ""

#: inc/template-tags.php:94
msgid "Older posts"
msgstr ""

#: inc/template-tags.php:98
msgid "Newer posts"
msgstr ""

#: inc/template-tags.php:127
msgid "Loading..."
msgstr ""

#: inc/template-tags.php:201
msgid "Post navigation"
msgstr ""

#: inc/template-tags.php:251
msgid "In %1$s"
msgstr ""

#: inc/template-tags.php:257
msgid "Tagged %1$s"
msgstr ""

#: inc/template-tags.php:294
msgid "Leave a comment"
msgstr ""

#: inc/template-tags.php:294
msgid "1 Comment"
msgstr ""

#: inc/template-tags.php:294
msgid "% Comments"
msgstr ""

#: inc/template-tags.php:299
msgid "Edit"
msgstr ""

#: inc/template-tags.php:321
msgid "Category: %s"
msgstr ""

#: inc/template-tags.php:323
msgid "Tag: %s"
msgstr ""

#: inc/template-tags.php:325
msgid "Author: %s"
msgstr ""

#: inc/template-tags.php:327
msgid "Year: %s"
msgstr ""

#: inc/template-tags.php:327
msgid "yearly archives date format"
msgstr ""

#: inc/template-tags.php:329
msgid "Month: %s"
msgstr ""

#: inc/template-tags.php:329
msgid "monthly archives date format"
msgstr ""

#: inc/template-tags.php:331
msgid "Day: %s"
msgstr ""

#: inc/template-tags.php:331
msgid "daily archives date format"
msgstr ""

#: inc/template-tags.php:334
msgid "Asides"
msgstr ""

#: inc/template-tags.php:334
msgid "Asides"
msgstr "post format archive title"

#: inc/template-tags.php:336
msgid "Galleries"
msgstr ""

#: inc/template-tags.php:336
msgid "post format archive title"
msgstr ""

#: inc/template-tags.php:338
msgid "Images"
msgstr ""

#: inc/template-tags.php:338
msgid "post format archive title"
msgstr ""

#: inc/template-tags.php:340
msgid "Videos"
msgstr ""

#: inc/template-tags.php:340
msgid "post format archive title"
msgstr ""

#: inc/template-tags.php:342
msgid "Quotes"
msgstr ""

#: inc/template-tags.php:342
msgid "post format archive title"
msgstr ""

#: inc/template-tags.php:344
msgid "Links"
msgstr ""

#: inc/template-tags.php:344
msgid "post format archive title"
msgstr ""

#: inc/template-tags.php:346
msgid "Statuses"
msgstr ""

#: inc/template-tags.php:346
msgid "post format archive title"
msgstr ""

#: inc/template-tags.php:348
msgid "Audio"
msgstr ""

#: inc/template-tags.php:348
msgid "post format archive title"
msgstr ""

#: inc/template-tags.php:350
msgid "Chats"
msgstr ""

#: inc/template-tags.php:350
msgid "post format archive title"
msgstr ""

#: inc/template-tags.php:353
msgid "Archives: %s"
msgstr ""

#: inc/template-tags.php:357
msgid "%1$s: %2$s"
msgstr ""

#: inc/template-tags.php:359
msgid "Archives"
msgstr ""

#: inc/theme-options.php:38
msgid "Option Types"
msgstr ""

#: inc/theme-options.php:39
msgid "Help content goes here!"
msgstr ""

#: inc/theme-options.php:42
msgid "Sidebar content goes here!"
msgstr ""

#: inc/theme-options.php:47
msgid "General"
msgstr ""

#: inc/theme-options.php:51
msgid "Home"
msgstr ""

#: inc/theme-options.php:55
msgid "Contact"
msgstr ""

#: inc/theme-options.php:59
msgid "Styles"
msgstr ""

#: inc/theme-options.php:63
msgid "Header"
msgstr ""

#: inc/theme-options.php:67
msgid "Footer"
msgstr ""

#: inc/theme-options.php:71
msgid "Blog"
msgstr ""

#: inc/theme-options.php:75
msgid "Single Post"
msgstr ""

#: inc/theme-options.php:79
msgid "Sidebar"
msgstr ""

#: inc/theme-options.php:83
msgid "Social Buttons"
msgstr ""

#: inc/theme-options.php:87
msgid "Social Footer Menu"
msgstr ""

#: inc/theme-options.php:92
msgid "Typography"
msgstr ""

#: inc/theme-options.php:96
msgid "Ad Managment"
msgstr ""

#: inc/theme-options.php:100
msgid "Import/Export"
msgstr ""


#: Start All Widget

#: inc/widget/author-posts.php:30 inc/widget/author-posts.php:37
msgid "ThemeCountry Author\'s Posts."
msgstr ""

#: inc/widget/author-posts.php:110
msgid "Author\'s Posts"
msgstr ""

#: inc/widget/author-posts.php:121
msgid "Title:"
msgstr ""

#: inc/widget/author-posts.php:126
msgid "Number of Posts to show"
msgstr ""

#: inc/widget/author-posts.php:133
msgid "Show Thumbnails"
msgstr ""

#: inc/widget/author-posts.php:140
msgid "Show post date"
msgstr ""

#: inc/widget/author-posts.php:147
msgid "Show number of comments"
msgstr ""

#: inc/widget/author-posts.php:154
msgid "Show excerpt"
msgstr ""

#: inc/widget/author-posts.php:159
msgid "Excerpt Length:"
msgstr ""

#: inc/widget/author-posts.php:213
msgid "No Comment"
msgstr ""

#: inc/widget/author-posts.php:213
msgid "One Comment"
msgstr ""

#: inc/widget/author-posts.php:213
msgid "Comments"
msgstr ""

#: inc/widget/banner125.php:32
msgid "A widget for 125 x 125 banner unit (max 6 ads)"
msgstr ""

#: inc/widget/banner125.php:43
msgid "ThemeCountry: 125x125 Banner"
msgstr ""

#: inc/widget/banner125.php:210
msgid "Our Sponsors"
msgstr ""

#: inc/widget/banner300.php:49
msgid "A widget for 300 x 250 banner (Single banner)"
msgstr ""

#: inc/widget/banner300.php:60
msgid "ThemeCountry: 300x250 Banner"
msgstr ""

#: inc/widget/banner300.php:143
msgid "Title:"
msgstr ""

#: inc/widget/banner300.php:149
msgid "Banner image url:"
msgstr ""

#: inc/widget/banner300.php:154
msgid "Open New Window:"
msgstr ""

#: inc/widget/category-posts.php:30
msgid "ThemeCountry Category Posts."
msgstr ""

#: inc/widget/category-posts.php:37
msgid "ThemeCountry: Category Posts"
msgstr ""

#: inc/widget/category-posts.php:111
msgid "Category Posts"
msgstr ""

#: inc/widget/category-posts.php:122
msgid "Title:"
msgstr ""

#: inc/widget/category-posts.php:126
msgid "Category:"
msgstr ""

#: inc/widget/category-posts.php:142
msgid "Number of Posts to show"
msgstr ""

#: inc/widget/category-posts.php:149
msgid "Show Thumbnails"
msgstr ""

#: inc/widget/category-posts.php:156
msgid "Show post date"
msgstr ""

#: inc/widget/category-posts.php:163
msgid "Show number of comments"
msgstr ""

#: inc/widget/category-posts.php:170
msgid "Show excerpt"
msgstr ""

#: inc/widget/category-posts.php:175
msgid "Excerpt Length:"
msgstr ""

#: inc/widget/category-posts.php:225
msgid "No Comment"
msgstr ""

#: inc/widget/category-posts.php:225
msgid "One Comment"
msgstr ""

#: inc/widget/category-posts.php:225
msgid "Comments"
msgstr ""

#: inc/widget/company-info.php:49
msgid "A widget to display static company information"
msgstr ""

#: inc/widget/company-info.php:59
msgid "ThemeCountry: Company Info"
msgstr ""

#: inc/widget/company-info.php:169
msgid "Title:"
msgstr ""

#: inc/widget/company-info.php:175
msgid "Address:"
msgstr ""

#: inc/widget/company-info.php:180
msgid "Phone:"
msgstr ""

#: inc/widget/company-info.php:186
msgid "Fax:"
msgstr ""

#: inc/widget/company-info.php:191
msgid "Email:"
msgstr ""

#: inc/widget/company-info.php:196
msgid "Website:"
msgstr ""

#: inc/widget/company-info.php:201
msgid "Description:"
msgstr ""

#: inc/widget/facebook-like-box.php:49
msgid "Add Facebook Like Box."
msgstr ""

#: inc/widget/facebook-like-box.php:54
msgid "ThemeCountry: FB Like Box"
msgstr ""

#: inc/widget/facebook-like-box.php:131
msgid "Find us on Facebook"
msgstr ""

#: inc/widget/facebook-like-box.php:140
msgid "Facebook Page URL"
msgstr ""

#: inc/widget/facebook-like-box.php:150
msgid "Color Scheme"
msgstr ""

#: inc/widget/facebook-like-box.php:159
msgid "Show Faces"
msgstr ""

#: inc/widget/facebook-like-box.php:169
msgid "Show Stream"
msgstr ""

#: inc/widget/facebook-like-box.php:169
msgid "Show Facebook Header"
msgstr ""

#: inc/widget/facebook-like-box.php:173
msgid "Show Border"
msgstr ""

#: inc/widget/googleplus.php:30
msgid "Add Google Plus Badge Box."
msgstr ""

#: inc/widget/googleplus.php:37
msgid "ThemeCountry: Google Plus Badge Box"
msgstr ""

#: inc/widget/googleplus.php:117
msgid "Find us on Google Plus"
msgstr ""

#: inc/widget/googleplus.php:121
msgid "Page type"
msgstr ""

#: inc/widget/googleplus.php:129
msgid "Title"
msgstr ""

#: inc/widget/googleplus.php:134
msgid "Google+ Page URL"
msgstr ""

#: inc/widget/googleplus.php:139
msgid "Width"
msgstr ""

#: inc/widget/googleplus.php:144
msgid "Color Scheme"
msgstr ""

#: inc/widget/googleplus.php:152
msgid "Layout"
msgstr ""

#: inc/widget/googleplus.php:165
msgid "Cover Photo"
msgstr ""

#: inc/widget/googleplus.php:170
msgid "Tagline"
msgstr ""

#: inc/widget/pinterest.php:30 inc/widget/pinterest.php:37
msgid "ThemeCountry Pinterest Profile Widget Box."
msgstr ""

#: inc/widget/pinterest.php:108
msgid "Find us on Pinterest"
msgstr ""

#: inc/widget/pinterest.php:117
msgid "Title"
msgstr ""

#: inc/widget/pinterest.php:122
msgid "Pinterest User URL:"
msgstr ""

#: inc/widget/pinterest.php:127
msgid "Image Width"
msgstr ""

#: inc/widget/pinterest.php:132
msgid "Board Width"
msgstr ""

#: inc/widget/pinterest.php:136
msgid "Board Height"
msgstr ""

#: inc/widget/popular.php:30
msgid "ThemeCountry Popular Posts."
msgstr ""

#: inc/widget/popular.php:37
msgid "ThemeCountry: Popular Posts"
msgstr ""

#: inc/widget/popular.php:110
msgid "Popular Posts"
msgstr ""

#: inc/widget/popular.php:123
msgid "Title:"
msgstr ""

#: inc/widget/popular.php:127
msgid "Popular limit (days):"
msgstr ""

#: inc/widget/popular.php:132
msgid "Number of Posts to show"
msgstr ""

#: inc/widget/popular.php:139
msgid "Show Thumbnails"
msgstr ""

#: inc/widget/popular.php:146
msgid "Show post date"
msgstr ""

#: inc/widget/popular.php:153
msgid "Show number of comments"
msgstr ""

#: inc/widget/popular.php:160
msgid "Show excerpt"
msgstr ""

#: inc/widget/popular.php:165
msgid "Excerpt Length:"
msgstr ""

#: inc/widget/popular.php:226
msgid "No Comment"
msgstr ""

#: inc/widget/popular.php:226
msgid "One Comment"
msgstr ""

#: inc/widget/popular.php:226
msgid "Comments"
msgstr ""

#: inc/widget/recent-posts.php:30
msgid "ThemeCountry Recent Posts."
msgstr ""

#: inc/widget/recent-posts.php:37
msgid "ThemeCountry: Recent Posts"
msgstr ""

#: inc/widget/recent-posts.php:108
msgid "Recent Posts"
msgstr ""

#: inc/widget/recent-posts.php:119
msgid "Title:"
msgstr ""

#: inc/widget/recent-posts.php:124
msgid "Number of Posts to show"
msgstr ""

#: inc/widget/recent-posts.php:131
msgid "Show Thumbnails"
msgstr ""

#: inc/widget/recent-posts.php:138
msgid "Show post date"
msgstr ""

#: inc/widget/recent-posts.php:145
msgid "Show number of comments"
msgstr ""

#: inc/widget/recent-posts.php:152
msgid "Show excerpt"
msgstr ""

#: inc/widget/recent-posts.php:157
msgid "Excerpt Length:"
msgstr ""

#: inc/widget/recent-posts.php:202
msgid "No Comment"
msgstr ""

#: inc/widget/recent-posts.php:202
msgid "One Comment"
msgstr ""

#: inc/widget/recent-posts.php:202
msgid "Comments"
msgstr ""

#: inc/widget/related-posts.php:30
msgid "ThemeCountry Related Posts."
msgstr ""

#: inc/widget/related-posts.php:37
msgid "ThemeCountry: Related Posts"
msgstr ""

#: inc/widget/related-posts.php:110
msgid "Related Posts"
msgstr ""

#: inc/widget/related-posts.php:121
msgid "Title:"
msgstr ""

#: inc/widget/related-posts.php:126
msgid "Number of Posts to show"
msgstr ""

#: inc/widget/related-posts.php:133
msgid "Show Thumbnails"
msgstr ""

#: inc/widget/related-posts.php:140
msgid "Show post date"
msgstr ""

#: inc/widget/related-posts.php:147
msgid "Show number of comments"
msgstr ""

#: inc/widget/related-posts.php:154
msgid "Show excerpt"
msgstr ""

#: inc/widget/related-posts.php:159
msgid "Excerpt Length:"
msgstr ""

#: inc/widget/related-posts.php:217
msgid "No Comment"
msgstr ""

#: inc/widget/related-posts.php:217
msgid "One Comment"
msgstr ""

#: inc/widget/related-posts.php:217
msgid "Comments"
msgstr ""

#: inc/widget/tweets.php:22
msgid "ThemeCountry Recent Tweets Widget."
msgstr ""

#: inc/widget/tweets.php:29
msgid "ThemeCountry: Recent Tweets"
msgstr ""