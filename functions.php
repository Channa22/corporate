<?php
/**
 * startbiz functions and definitions
 *
 * @package StartBiz
 */
/**
|------------------------------------------------------------------------------
| OptionTree framework integration: Use in theme mode
|------------------------------------------------------------------------------
*/	
	add_filter( 'ot_show_pages', '__return_false' );
	add_filter( 'ot_show_new_layout', '__return_false' );
	add_filter( 'ot_theme_mode', '__return_true' );
	load_template( get_template_directory() . '/option-tree/ot-loader.php' );

/** 
|------------------------------------------------------------------------------
| Set the content width based on the theme's design and stylesheet.
|------------------------------------------------------------------------------
*/
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

/**
|------------------------------------------------------------------------------
| Get Theme Options
|------------------------------------------------------------------------------
| @return string 
|
*/
function tc_get_option($option_key) {
	
	if ( ! function_exists( 'ot_get_option' ) ) {
		return false;
	}

	return ot_get_option($option_key);
}


if ( ! function_exists( 'startbiz_setup' ) ) :
/** 
|------------------------------------------------------------------------------
| Sets up theme defaults and registers support for various WordPress features.
|------------------------------------------------------------------------------
|
| Note that this function is hooked into the after_setup_theme hook, which
| runs before the init hook. The init hook is too late for some features, such
| as indicating support for post thumbnails.
|
*/

function startbiz_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on startbiz, use a find and replace
	 * to change 'startbiz' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'startbiz', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	set_post_thumbnail_size( 300 );
	add_image_size( 'post-thumbnails-blog', 340 , 250, true );
	add_image_size( 'post-thumbnails-service', 260, 260, true);
	add_image_size( 'post-thumbnails-list', 300, 162, true);
	add_image_size( 'widget-thumbnail', 110, 65, true);
	add_image_size( 'related-post-singlethumbnail', 170, 100, true);
	

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'startbiz' ),
		'secondary' => __( 'Secondary Menu', 'startbiz' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'startbiz_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // startbiz_setup
add_action( 'after_setup_theme', 'startbiz_setup' );


/**
|------------------------------------------------------------------------------
| Get Attachment id From src
|------------------------------------------------------------------------------
*/
function get_attachment_id_from_src ($image_src) {

    global $wpdb;
    $query = "SELECT ID FROM {$wpdb->posts} WHERE guid='$image_src'";
    $id = $wpdb->get_var($query);
    return $id;

}

/** 
|------------------------------------------------------------------------------
| Register widget area.
|------------------------------------------------------------------------------
|
| @link http://codex.wordpress.org/Function_Reference/register_sidebar
|
*/
function startbiz_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'startbiz' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));
	register_sidebar( array(
		'name'          => __( 'Footer Sidebar 1', 'themecountry' ),
		'id'            => 'footer-sidebar-1',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer Sidebar 2', 'themecountry' ),
		'id'            => 'footer-sidebar-2',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer Sidebar 3', 'themecountry' ),
		'id'            => 'footer-sidebar-3',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Sidebar 4', 'themecountry' ),
		'id'            => 'footer-sidebar-4',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'startbiz_widgets_init' );

/**
|------------------------------------------------------------------------------
| Enqueue scripts and styles.
|------------------------------------------------------------------------------
*/
function startbiz_scripts() {
	/**
	* Engueue Style
	*/
	wp_enqueue_style( 'startbiz-admin-style', get_template_directory_uri() .  '/css/admin-style.css');
	wp_enqueue_style( 'startbiz-font-awesome-style', get_template_directory_uri() .  '/css/font-awesome.min.css');
	wp_enqueue_style( 'startbiz-prettyPhoto-style', get_template_directory_uri() .  '/css/prettyPhoto.css');
	wp_enqueue_style( 'startbiz-bootstrap.min', get_template_directory_uri() . '/css/bootstrap.min.css' );
	wp_enqueue_style( 'startbiz-style', get_stylesheet_uri() );
	wp_enqueue_style( 'startbiz-responsive-style', get_template_directory_uri() .  '/css/responsive.css');
	wp_enqueue_style( 'startbiz-owlcarousel', get_template_directory_uri() .  '/css/owl.carousel.css');	
	wp_enqueue_style( 'startbiz-owltheme', get_template_directory_uri() .  '/css/owl.theme.css');
	/**
	* Engueue Script
	*/
	if (is_page_template('page-contact.php')) {
		wp_enqueue_script( 'startbiz-jquery-prettyPhoto', '//maps.google.com/maps/api/js?sensor=false', array('jquery'), '20150423', false );
	}
	wp_enqueue_script( 'startbiz-jquery-prettyPhoto', get_template_directory_uri() . '/js/jquery.prettyPhoto.js', array('jquery'), '20150423', true );
	wp_enqueue_script( 'startbiz-jquery-owlcarousel', get_template_directory_uri() . '/js/owl.carousel.js', array('jquery'), '20150423', true );
	wp_enqueue_script( 'startbiz-script', get_template_directory_uri() . '/js/script.js', array('startbiz-jquery-prettyPhoto'), '20150423', true );
	wp_localize_script( 'startbiz-script', 'AdminAjaxURL', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
	wp_enqueue_script( 'startbiz-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	//wp_enqueue_script( 'startbiz-flexslider', get_template_directory_uri() . '/js/jquery.flexslider.js', array('jquery'), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'startbiz_scripts' );

/**
|------------------------------------------------------------------------------
| Enqueue Admin scripts and styles.
|------------------------------------------------------------------------------
*/

function startbiz_admin_scripts() {
        wp_enqueue_style( 'startbiz-admin-css', get_template_directory_uri() . '/css/admin-style.css', false, '1.0.0' );
        wp_enqueue_script( 'ZeroClipboard-admin-script', get_template_directory_uri() . '/js/ZeroClipboard.min.js', array('jquery'), '20150226', true );
        wp_enqueue_script( 'startbiz-admin-script', get_template_directory_uri() . '/js/admin-script.js', array('jquery'), '20150226', true );
        wp_enqueue_script( 'startbiz-admin-script', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '20150226', true );

}
add_action( 'admin_enqueue_scripts', 'startbiz_admin_scripts' );

/**
|------------------------------------------------------------------------------
|  Dynamic sidebar primary
|------------------------------------------------------------------------------
*/

if ( ! function_exists( 'tc_sidebar_primary' ) ) {

	function tc_sidebar_primary() {
		// Default sidebar
		$sidebar = 'sidebar-1';

		// Set sidebar based on page
		if ( is_home() && tc_get_option('s1-home') ) $sidebar = tc_get_option('s1-home');
		if ( is_single() && tc_get_option('s1-single') ) $sidebar = tc_get_option('s1-single');
		if ( is_archive() && tc_get_option('s1-archive') ) $sidebar = tc_get_option('s1-archive');
		if ( is_category() && tc_get_option('s1-archive-category') ) $sidebar = tc_get_option('s1-archive-category');
		if ( is_search() && tc_get_option('s1-search') ) $sidebar = tc_get_option('s1-search');
		if ( is_404() && tc_get_option('s1-404') ) $sidebar = tc_get_option('s1-404');
		if ( is_page() && tc_get_option('s1-page') ) $sidebar = tc_get_option('s1-page');


		// Check for page/post specific sidebar
		if ( is_page() || is_single() ) {
			// Reset post data
			wp_reset_postdata();
			global $post;
			// Get meta
			$meta = get_post_meta($post->ID,'_sidebar_primary',true);
			if ( $meta ) { $sidebar = $meta; }
		}

		// Return sidebar
		return $sidebar;
	}
	
}

if ( ! function_exists( 'startbiz_custom_sidebars' ) ) {

	/**  
	|------------------------------------------------------------------------------
	| Register custom sidebars
	|------------------------------------------------------------------------------
	*/

	function tc_custom_sidebars() {
		if ( !tc_get_option('sidebar-areas') =='' ) {
				
			$sidebars = tc_get_option('sidebar-areas', array());
				
			if ( !empty( $sidebars ) ) {
				foreach( $sidebars as $sidebar ) {
					if ( isset($sidebar['title']) && !empty($sidebar['title']) && isset($sidebar['id']) && !empty($sidebar['id']) && ($sidebar['id'] !='sidebar-') ) {
						register_sidebar(array('name' => ''.$sidebar['title'].'','id' => ''.strtolower($sidebar['id']).'','before_widget' => '<aside id="%1$s" class="widget %2$s">','after_widget' => '</aside>','before_title' => '<h2 class="widget-title">','after_title' => '</h2>'));
					}
				}
			}
		}
	}
		
}

add_action( 'widgets_init', 'tc_custom_sidebars' );

/**
|------------------------------------------------------------------------------
| Truncate string to x letters/words
|------------------------------------------------------------------------------
*/

function excerpt($limit) {
	 $excerpt = explode(' ', get_the_excerpt(), $limit);
	 if (count($excerpt)>=$limit) {
	 array_pop($excerpt);
	 $excerpt = implode(" ",$excerpt).'...';
	 } else {
	 $excerpt = implode(" ",$excerpt);
	 }
	 $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
	 return $excerpt;
}

function content($limit) {
	 $content = explode(' ', get_the_content(), $limit);
	 if (count($content)>=$limit) {
	 array_pop($content);
	 $content = implode(" ",$content).'...';
	 } else {
	 $content = implode(" ",$content);
	 }
	 $content = preg_replace('/[.+]/','', $content);
	 $content = apply_filters('the_content', $content);
	 $content = str_replace(']]>', ']]&gt;', $content);
	 return $content;
}

function tc_truncate( $str, $length = 40, $units = 'letters', $ellipsis = '&nbsp;&hellip;' ) {
    if ( $units == 'letters' ) {
        if ( mb_strlen( $str ) > $length ) {
            return mb_substr( $str, 0, $length ) . $ellipsis;
        } else {
            return $str;
        }
    } else {
        $words = explode( ' ', $str );
        if ( count( $words ) > $length ) {
            return implode( " ", array_slice( $words, 0, $length ) ) . $ellipsis;
        } else {
            return $str;
        }
    }
}

if ( ! function_exists( 'tc_excerpt' ) ) {
    function tc_excerpt( $limit = 40 ) {
      return tc_truncate( get_the_excerpt(), $limit, 'words' );
    }
}
if ( ! function_exists('tc_breadcrumb')) {

	/**
	|------------------------------------------------------------------------------
	| TC Breadcrumb 
	|------------------------------------------------------------------------------
	|
	*/
	function tc_breadcrumb() {
		$breadcrumb = is_array(ot_get_option('yoast_breadcrumb')) ? array_flip(ot_get_option('yoast_breadcrumb')) : array();
		
		if ( ot_get_option( 'breadcrumb' ) != 'off') : ?>
			<div class="breadcrumb">
				 <?php

				 	if ( isset($breadcrumb['yoast_breadcrumb_yes']) ) {
				 		if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('<p id="breadcrumbs">','</p>');
						}
				 	} else {
				 		tcc_breadcrumb();
				 	}
				 ?>
			</div>
		<?php endif; 
	}
}



function set_tag_cloud_sizes($args) {
	$args['smallest'] = 12;
	$args['largest'] = 12;
	$args['unit'] = 'px';
return $args; }

add_filter('widget_tag_cloud_args','set_tag_cloud_sizes');


/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
* Ads Functions
*/
require get_template_directory() . '/inc/ads-managment.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Widgets
 */
require get_template_directory() . '/inc/widget.php';


/**
* Backup Theme Option
*/
load_template( get_template_directory() . '/inc/import-export-theme-option.php' );

/**
|------------------------------------------------------------------------------
| Hide backup theme option from admin menu
|------------------------------------------------------------------------------
*/

function tc_hide_wp_menu() {
  $page = remove_submenu_page( 'themes.php', 'tc-theme-backup' );
}
add_action( 'admin_menu', 'tc_hide_wp_menu', 999 );

/** 
|------------------------------------------------------------------------------
|  Remove comment box
|
*/
function startbiz_comments_form_defaults($default) {
        $default['comment_notes_after'] = '';
        return $default;
}

add_filter('comment_form_defaults','startbiz_comments_form_defaults');

/**
* Theme Options
*/
load_template( get_template_directory() . '/inc/theme-options.php' );

/**
* Custom Style 
*/
load_template( get_template_directory() . '/inc/custom-style.php' );

/**
* Portfolio
*/
load_template( get_template_directory() . '/inc/portfolio.php' );
